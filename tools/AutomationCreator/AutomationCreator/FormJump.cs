﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Biendich.Utility;

namespace AutomationCreator
{
    public partial class FormJump : Form, IFieldItemForm
    {

        private static FormJump form_jump = new FormJump();
        public static FormJump GetForm()
        {
            return form_jump;
        }

        private bool cmd;
        public FormJump()
        {
            InitializeComponent();
        }

        public Pair<bool, Object[]> ShowAsDialog(params Object[] param)
        {
            cmd = false;

            cmbType.Items.Clear();
            cmbParam.Items.Clear();

            string 
                type = param[0].ToString(),
                name = param[1].ToString(),
                parametrs = param[2].ToString();

            FieldData 
                data = (FieldData)param[3];

            foreach (string item in data.Jumps())
            {
                cmbParam.Items.Add(item);
            }

            cmbType.Items.Add("Jump");
            cmbType.SelectedIndex = 0;
            txtName.Text = name;
            cmbParam.Text = parametrs;


            this.ShowDialog();

            Object[] arr = new string[] { 
                cmbType.Text, txtName.Text, cmbParam.Text };

            return Pair.Create(cmd, arr);
        }

        private void JumpOptions_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                cmd = true;
                this.Close();
            }
            else if (e.KeyChar == (char)Keys.Escape)
            {
                this.Close();
            }
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            cmd = true;
            this.Close();
        }
    }
}
