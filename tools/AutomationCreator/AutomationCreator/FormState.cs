﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Biendich.Utility;

namespace AutomationCreator
{
    public partial class StateOptions : Form, IFieldItemForm
    {
        private static StateOptions form_state = new StateOptions();
        public static StateOptions GetForm()
        {
            return form_state;
        }

        private bool cmd;
        private FieldData data;

        public StateOptions()
        {
            InitializeComponent();
        }

        public Pair<bool, Object[]> ShowAsDialog(params Object[] param)
        {
            cmd = false;
            cmbType.Items.Clear();

            string 
                type = param[0].ToString(), 
                name = param[1].ToString(),
                parametrs = param[2].ToString();

            data = (FieldData)param[4];

            foreach (string s in data.States())
            {
                cmbType.Items.Add(s);
            }

            cmbType.Text = param[0].ToString();
            txtName.Text = param[1].ToString();
            txtParams.Text = parametrs;
            chGen.Checked = (bool)param[3];

            this.ShowDialog();

            Object[] arr = new Object[] { 
                cmbType.Text, txtName.Text, txtParams.Text, chGen.Checked };

            return Pair.Create(cmd, arr);
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (cmbType.Text != "" && txtName.Text != "")
            {
                cmd = true;
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void StateOptions_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && 
                cmbType.Text != "" && txtName.Text != "")
                button1_Click(sender, e);
            if (e.KeyChar == (char)Keys.Escape)
                button2_Click(sender, e);
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtName.Text = data.AttributeState(cmbType.Text, "name");
            txtParams.Text = data.AttributeState(cmbType.Text, "params");
        }
    }
}
