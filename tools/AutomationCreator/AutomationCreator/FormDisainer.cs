﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Graphic;
using Biendich.Utility;

namespace AutomationCreator
{
    public partial class FormDesigner : Form
    {
        private string filename;
        private Field field;
        private GraphicField graphic_field;

        private FormOptions form_option;
        private FormItems form_items;

        private Point mousedown;
        private IGraphicItem curr;
        private Options options;
        private TextOption[] text_options;

        private Dictionary<TextOption.TextType, ToolStripMenuItem> menu_items_state;
        private Dictionary<TextOption.TextType, ToolStripMenuItem> menu_items_jump;

        private Dictionary<string, TextOption.TextType> text_types;
        private string data_path;

        public FormDesigner()
        {
            InitializeComponent();

            graphic_field = new GraphicField(1200, 800);

            text_options = new TextOption[] { 
                new TextOption(Color.White, TextOption.TextType.Name), 
                new TextOption(Color.White, TextOption.TextType.Parametrs) };

            if ((options = Options.Load("options.xml")) != null)
            {
                GraphicStateItem.Color = options.StateColor;
                text_options[0].Color = options.StateNameColor;
                GraphicJump.Color = options.JumpColor;
                GraphicJump.PointColor = options.JumpPointColor;
                text_options[1].Color = options.JumpNameColor;
                graphic_field.Fon = options.BackgroundColor;
                text_options[0].Type = options.StateCaption;
                text_options[1].Type = options.JumpCaption;
            }
            else
            {
                options = new Options();

                options.StateColor = GraphicStateItem.Color;
                options.StateNameColor = text_options[0].Color;
                options.JumpColor = GraphicJump.Color;
                options.JumpNameColor = GraphicJump.PointColor;
                options.JumpPointColor = text_options[1].Color;
                options.BackgroundColor = graphic_field.Fon;
            }

            menu_items_state = new Dictionary<TextOption.TextType, ToolStripMenuItem>();
            menu_items_state.Add(TextOption.TextType.Name, menuStateName);
            menu_items_state.Add(TextOption.TextType.Type, menuStateType);
            menu_items_state.Add(TextOption.TextType.Parametrs, menuStatePara);
            menu_items_state.Add(TextOption.TextType.Invisible, menuStateInvi);

            menu_items_jump = new Dictionary<TextOption.TextType, ToolStripMenuItem>();
            menu_items_jump.Add(TextOption.TextType.Name, menuJumpName);
            menu_items_jump.Add(TextOption.TextType.Type, menuJumpType);
            menu_items_jump.Add(TextOption.TextType.Parametrs, menuJumpPara);
            menu_items_jump.Add(TextOption.TextType.Invisible, menuJumpInvi);

            menu_items_state[options.StateCaption].Checked = true;
            menu_items_jump[options.JumpCaption].Checked = true;

            text_types = new Dictionary<string, TextOption.TextType>();
            text_types.Add("Name", TextOption.TextType.Name);
            text_types.Add("Type", TextOption.TextType.Type);
            text_types.Add("Para", TextOption.TextType.Parametrs);
            text_types.Add("Invi", TextOption.TextType.Invisible);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            filename = "";
            data_path = "data.xml";

            pictureBox1.Image = graphic_field.Image;
            pictureBox1.Refresh();

            field = new Field(graphic_field, text_options);
            field.LoadData(data_path);

            form_option = new FormOptions();
            form_items = new FormItems();
        }


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            mousedown = new Point(e.X, e.Y);

            curr = graphic_field.GetItem(new Point(
                e.X + graphic_field.Position.X, e.Y + graphic_field.Position.Y));
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != System.Windows.Forms.MouseButtons.Left) return;

            Point p1 = new Point(
                mousedown.X + graphic_field.Position.X, mousedown.Y + graphic_field.Position.Y);
            Point p2 = new Point(
                e.X + graphic_field.Position.X, e.Y + graphic_field.Position.Y);

            if (chestate.Checked)
            {
                if (graphic_field.Free(p2))
                {
                    string type = field.Data.States()[0];
                    Pair<bool, Object[]> pair = StateOptions.GetForm().ShowAsDialog(
                        type, field.Data.AttributeState(type, "name"),
                        field.Data.AttributeState(type, "params"), false, field.Data);

                    Object[] state = pair.Second;
                    if (pair.First && !field.ContainState(state[1].ToString()))
                    {
                        State s = new State( 
                            state[0].ToString(), state[1].ToString(),
                            state[2].ToString(), (bool)state[3], field, 
                            graphic_field, p2, text_options[0]);
                        field.AddState(s);
                    }
                }
            }
            else if (chejump.Checked)
            {
                IReflection s1 = (IReflection)graphic_field.GetItem(p1);
                IReflection s2 = (IReflection)graphic_field.GetItem(p2);

                if (s1 is GraphicStateItem && s2 is GraphicStateItem)
                {
                    Pair<bool, Object[]> pair = FormJump.GetForm().ShowAsDialog(
                        "Jump", field.GetJumpName(), field.Data.Jumps()[0], field.Data);
                    Object[] jump = pair.Second;
                    if (pair.First && !field.ContainJump(jump[1].ToString()))
                    {
                        Jump j = new Jump(
                            jump[0].ToString(), jump[1].ToString(), jump[2].ToString(), 
                            (State)s1.Objects[0], (State)s2.Objects[0], 
                            field, graphic_field, text_options[1]);
                        field.AddJump(j);
                    }
                }
            }
            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (cheselect.Checked)
                {
                    Point p2 = new Point(e.X, e.Y);
                    if (curr != null)
                    {
                        curr.Move(mousedown, p2);
                        mousedown = p2;
                    }
                    else
                    {
                        graphic_field.MovePosition(mousedown.X - e.X, mousedown.Y - e.Y);
                        mousedown.X = e.X;
                        mousedown.Y = e.Y;
                    }
                }
                graphic_field.DrawOnLayers();
                pictureBox1.Refresh();
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                Point p2 = new Point(e.X, e.Y);
                if (curr != null)
                {
                    curr.Move(mousedown, p2);
                    mousedown = p2;
                }
                graphic_field.DrawOnLayers();
                pictureBox1.Refresh();
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Middle)
            {
                graphic_field.MovePosition(mousedown.X - e.X, mousedown.Y - e.Y);
                mousedown.X = e.X;
                mousedown.Y = e.Y;
                graphic_field.DrawOnLayers();
                pictureBox1.Refresh();
            }
        }


        private void chestate_CheckedChanged(object sender, EventArgs e)
        {
            if (chestate.Checked)
            {
                chejump.Checked = false;
                cheselect.Checked = false;
            }
        }

        private void chejump_CheckedChanged(object sender, EventArgs e)
        {
            if (chejump.Checked)
            {
                chestate.Checked = false;
                cheselect.Checked = false;
            }
        }

        private void cheselect_CheckedChanged(object sender, EventArgs e)
        {
            if (cheselect.Checked)
            {
                chestate.Checked = false;
                chejump.Checked = false;
            }
        }


        private void menuSaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = "auto.xml";
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                field.Save(saveFileDialog1.FileName);
                filename = saveFileDialog1.FileName;
                this.Text = "Дизайнер - " + filename;
            }
        }

        private void menuOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                GraphicField gfield = new GraphicField(1200, 800);
                Field field_open = new Field(gfield, text_options);
                field_open.LoadData(data_path);
                if (field_open.Load(openFileDialog1.FileName))
                {
                    gfield.Fon = graphic_field.Fon;
                    graphic_field = gfield;
                    field = field_open;
                    pictureBox1.Image = graphic_field.Image;
                    pictureBox1.Refresh();
                    filename = openFileDialog1.FileName;
                }

                this.Text = "Дизайнер - " + filename;
            }
        }

        private void menuSave_Click(object sender, EventArgs e)
        {
            if (filename == "") menuSaveAs_Click(sender, e);
            else field.Save(filename);
        }
        
        private void menuOptions_Click(object sender, EventArgs e)
        {
            form_option.ShowAsDialog(options);

            GraphicStateItem.Color = options.StateColor;
            text_options[0].Color = options.StateNameColor;
            GraphicJump.Color = options.JumpColor;
            GraphicJump.PointColor = options.JumpPointColor;
            text_options[1].Color = options.JumpNameColor;
            graphic_field.Fon = options.BackgroundColor;


            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void menuStates_Click(object sender, EventArgs e)
        {
            form_items.ShowAsDialog(field, "s");
            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void menuJumps_Click(object sender, EventArgs e)
        {
            form_items.ShowAsDialog(field, "j");
            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void menuClose_Click(object sender, EventArgs e)
        {
            field.Save("exit");
            this.Close();
        }

        private void menuStateCaption_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem curr = (ToolStripMenuItem)sender;
            foreach (ToolStripMenuItem item in menu_items_state.Values)
            {
                if (curr.Name == item.Name)
                {
                    item.Checked = true;
                    text_options[0].Type = text_types[item.Name.Substring(item.Name.Length - 4)];
                    options.StateCaption = text_options[0].Type;
                }
                else item.Checked = false;
            }

            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void menuJumpCaption_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem curr = (ToolStripMenuItem)sender;
            foreach (ToolStripMenuItem item in menu_items_jump.Values)
            {
                if (curr.Name == item.Name)
                {
                    item.Checked = true;
                    text_options[1].Type = text_types[item.Name.Substring(item.Name.Length - 4)];
                    options.JumpCaption = text_options[1].Type;
                }
                else item.Checked = false;
            }

            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void menuCreate_Click(object sender, EventArgs e)
        {
            field.Save("create");

            field = new Field(graphic_field, text_options);
            field.LoadData(data_path);
            filename = "";

            this.Text = "Дизайнер";

            graphic_field.Clear();
            graphic_field.DrawOnLayers();
            pictureBox1.Refresh();
        }

        private void menuCSharp_Click(object sender, EventArgs e)
        {
            File.WriteAllText(filename + ".cs", field.GenerateCSharp());
        }


        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (curr != null)
            {
                State curr_s = (State)((IReflection)curr).Objects[0];
                curr_s.Remove();
                graphic_field.DrawOnLayers();
                pictureBox1.Refresh();
            }
        }

        private void cmdEdit_Click(object sender, EventArgs e)
        {
            if (curr != null)
            {
                IFieldItem curr_s = (State)((IReflection)curr).Objects[0];

                if (curr_s.Edit())
                {
                    graphic_field.DrawOnLayers();
                    pictureBox1.Refresh();
                }
            }
        }

        private void Form1_Closing(object sender, FormClosingEventArgs e)
        {
            options.Save("options.xml");
        }


        private void Automation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                cmdDel_Click(sender, e);
            }
        }

        private void menuXML_Click(object sender, EventArgs e)
        {
            field.GenerateXML().Save(filename + ".code.xml");
        }
    }
}
