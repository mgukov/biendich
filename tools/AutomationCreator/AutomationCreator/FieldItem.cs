﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Graphic;
using Biendich.Utility;

namespace AutomationCreator
{
    public interface IFieldItemForm
    {
        Pair<bool, Object[]> ShowAsDialog(params Object[] param);
    }

    public interface IFieldItem : IReflectable
    {
        void Remove();
        bool Edit();
        string[] Description();
        string[] DescriptionCols();
        string Name { get; }
        string Type { get; }
        string Parametrs { get; }
        IFieldItemForm GetForm();
    }
}
