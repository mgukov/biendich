﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutomationCreator
{
    public partial class FormOptions : Form
    {
        private Options opti;
        public FormOptions()
        {
            InitializeComponent();
        }
        public Options ShowAsDialog(Options options)
        {
            opti = options;

            cmdBackground.BackColor = options.BackgroundColor;
            cmdState.BackColor = options.StateColor;
            cmdJump.BackColor = options.JumpColor;
            cmdStateName.BackColor = options.StateNameColor;
            cmdJumpName.BackColor = options.JumpNameColor;
            cmdPoint.BackColor = options.JumpPointColor;

            this.ShowDialog();

            return opti;
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdBackground_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = cmdBackground.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cmdBackground.BackColor = colorDialog1.Color;
                opti.BackgroundColor = colorDialog1.Color;
            }
        }

        private void cmdState_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = cmdState.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cmdState.BackColor = colorDialog1.Color;
                opti.StateColor = colorDialog1.Color;
            }
        }

        private void cmdJump_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = cmdJump.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cmdJump.BackColor = colorDialog1.Color;
                opti.JumpColor = colorDialog1.Color;
            }
        }

        private void cmdStateName_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = cmdStateName.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cmdStateName.BackColor = colorDialog1.Color;
                opti.StateNameColor = colorDialog1.Color;
            }
        }

        private void cmdJumpName_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = cmdJumpName.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cmdJumpName.BackColor = colorDialog1.Color;
                opti.JumpNameColor = colorDialog1.Color;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = cmdPoint.BackColor;
            if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cmdPoint.BackColor = colorDialog1.Color;
                opti.JumpPointColor = colorDialog1.Color;
            }
        }
    }
}
