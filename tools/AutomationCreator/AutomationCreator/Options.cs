﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml;
using System.Windows.Forms;
using Graphic;

namespace AutomationCreator
{

    public class Options
    {
        private TextOption.TextType state_caption;
        private TextOption.TextType jump_caption;

        private Color color_background;
        private Color color_state;
        private Color color_state_name;
        private Color color_jump;
        private Color color_jump_name;
        private Color color_jump_point;

        public Options()
        {
            jump_caption = TextOption.TextType.Parametrs;
            state_caption = TextOption.TextType.Name;

        }

        public TextOption.TextType StateCaption
        {
            get { return state_caption; }
            set { state_caption = value; }
        }

        public TextOption.TextType JumpCaption
        {
            get { return jump_caption; }
            set { jump_caption = value; }
        }

        public Color StateColor
        {
            get { return color_state; }
            set { color_state = value; }
        }

        public Color JumpColor
        {
            get { return color_jump; }
            set { color_jump = value; }
        }

        public Color StateNameColor
        {
            get { return color_state_name; }
            set { color_state_name = value; }
        }

        public Color JumpNameColor
        {
            get { return color_jump_name; }
            set { color_jump_name = value; }
        }

        public Color BackgroundColor
        {
            get { return color_background; }
            set { color_background = value; }
        }

        public Color JumpPointColor
        {
            get { return color_jump_point; }
            set { color_jump_point = value; }
        }

        public static Options Load(string path)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch (Exception)
            {
                MessageBox.Show("Файл настроек не найден или имеет неверный формат",
                    "Options", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
            XmlNode root = doc["options"];
            Options options = new Options();

            options.BackgroundColor =
                Color.FromArgb(Convert.ToInt32(root["background_color"].InnerText));
            options.StateColor =
                Color.FromArgb(Convert.ToInt32(root["state_color"].InnerText));
            options.JumpColor =
                Color.FromArgb(Convert.ToInt32(root["jump_color"].InnerText));
            options.StateNameColor =
                Color.FromArgb(Convert.ToInt32(root["state_name_color"].InnerText));
            options.JumpNameColor =
                Color.FromArgb(Convert.ToInt32(root["jump_name_color"].InnerText));
            options.JumpPointColor =
                Color.FromArgb(Convert.ToInt32(root["jump_point_color"].InnerText));
            options.JumpCaption =
                (TextOption.TextType)Convert.ToInt32(root["jump_caption"].InnerText);
            options.StateCaption =
                (TextOption.TextType)Convert.ToInt32(root["state_caption"].InnerText);
            return options;
        }

        public void Save(string path)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("options");
            doc.AppendChild(root);

            XmlElement node = doc.CreateElement("background_color");
            node.InnerText = this.BackgroundColor.ToArgb().ToString();
            root.AppendChild(node);

            node = doc.CreateElement("state_color");
            node.InnerText = this.StateColor.ToArgb().ToString();
            root.AppendChild(node);

            node = doc.CreateElement("jump_color");
            node.InnerText = this.JumpColor.ToArgb().ToString();
            root.AppendChild(node);

            node = doc.CreateElement("jump_name_color");
            node.InnerText = this.JumpNameColor.ToArgb().ToString();
            root.AppendChild(node);

            node = doc.CreateElement("state_name_color");
            node.InnerText = this.StateNameColor.ToArgb().ToString();
            root.AppendChild(node);

            node = doc.CreateElement("jump_point_color");
            node.InnerText = this.JumpPointColor.ToArgb().ToString();
            root.AppendChild(node);

            node = doc.CreateElement("jump_caption");
            node.InnerText = ((int)this.JumpCaption).ToString();
            root.AppendChild(node);

            node = doc.CreateElement("state_caption");
            node.InnerText = ((int)this.StateCaption).ToString();
            root.AppendChild(node);

            doc.Save("options.xml");

        }
    }
}