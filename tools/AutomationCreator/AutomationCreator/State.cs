﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Graphic;
using Biendich.Utility;

namespace AutomationCreator
{
    public class State : IFieldItem
    {
        private static int counter = 0;
        private static int getid() { return counter++; }

        private int id;
        private string name;
        private string type;
        private string parametrs;
        private bool gen;

        private GraphicField gfield;
        private Field field;

        private List<Jump> jamps_in;
        private List<Jump> jamps_out;
        private List<IReflection> reflections;

        private bool del_semaphore;

        public State(string type, string name, string parametrs, bool gen)
        {
            id = getid();

            this.type = type;
            this.name = name;
            this.parametrs = parametrs;
            this.gen = gen;

            jamps_in = new List<Jump>();
            jamps_out = new List<Jump>();
            reflections = new List<IReflection>();

            del_semaphore = false;
        }

        public State(string type, string name, string parametrs, bool gen,
            Field field ,GraphicField gfield, Point p, TextOption text_option)
            : this(type, name, parametrs, gen)
        {
            this.gfield = gfield;
            this.field = field;

            GraphicStateItem gstate = new GraphicStateItem(this, p, 20);
            GraphicStateCaption gsname = new GraphicStateCaption(this, gstate, text_option);
            this.AddReflection(gstate);
            this.AddReflection(gsname);
            gfield.AddItem(gstate);
            gfield.AddItem(gsname);
        }

        public int Id
        {
            get { return id; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value;  }
        }

        public string Parametrs
        {
            get { return parametrs; }
            set { parametrs = value; }
        }

        public bool Generation
        {
            get { return gen; }
            set { gen = value; }
        }

        public IReflection[] Reflections
        {
            get { return reflections.ToArray(); }
        }

        public void AddReflection(IReflection reflection)
        {
            reflections.Add(reflection);
        }

        public void Remove()
        {
            del_semaphore = true;

            foreach (IReflection item in reflections)
            {
                item.Remove();
            }
            foreach (Jump j in jamps_in)
            {
                j.Remove();
            }
            foreach (Jump j in jamps_out)
            {
                j.Remove();
            }
            field.DelState(this);
        }

        public void AddJumpIn(Jump j)
        {
            jamps_in.Add(j);
        }
        public void AddJumpOut(Jump j)
        {
            jamps_out.Add(j);
        }

        public void DelJumpIn(Jump j)
        {
            if (!del_semaphore) jamps_in.Remove(j);
        }
        public void DelJumpOut(Jump j)
        {
            if (!del_semaphore) jamps_out.Remove(j);
        }

        public Jump[] JumpIn()
        {
            return jamps_in.ToArray();
        }
        public Jump[] JumpOut()
        {
            return jamps_out.ToArray();
        }

        public bool Edit()
        {
            Pair<bool, Object[]> p = GetForm().ShowAsDialog(
                this.type, this.name, this.parametrs, this.gen, field.Data);

            if (!p.First) return false;

            string name = this.name;
            this.name = p.Second[1].ToString();

            if (field.StateEdit(name, this))
            {
                this.type = p.Second[0].ToString();
                this.parametrs = p.Second[2].ToString();
                this.gen = (bool)p.Second[3];
                return true;
            }

            this.name = name;
            return false;
        }

        public string[] Description()
        {
            string[] desc = new string[] {
                this.Type, this.Name, this.Parametrs };
            return desc;
        }
        public string[] DescriptionCols()
        {
            string[] desc = new string[] { "Тип", "Имя", "Параметры" };
            return desc;
        }

        public IFieldItemForm GetForm()
        {
            return StateOptions.GetForm();
        }
    }

    public class GraphicStateItem : AGraphicItem, IReflection
    {

        private static Color color = Color.YellowGreen;
        public static Color Color
        {
            get { return color; }
            set { color = value; }
        }

        private static int count = 0;
        private static string curr() { return count++.ToString(); }

        private State state;
        private Point point;
        private int radius;

        public IReflectable[] Objects
        {
            get { return new IReflectable[] { state }; }
        }

        public override Point Point
        {
            get { return point; }
        }

        public int Radius
        {
            get { return radius; }
        }

        public GraphicStateItem(State state, Point point, int radius)
            : base(1, "gs" + curr())
        {
            this.state = state;
            this.radius = radius;
            this.point = point;

            state.AddReflection(this);
        }

        public override void Draw(Graphics graphics, Point p)
        {
            if (p.X > point.X + radius || p.Y > point.Y + radius ||
                p.X + Field.Width < point.X - radius || p.Y + Field.Width < point.Y - radius)
                return;

            graphics.DrawEllipse(new Pen(color, 2),
                point.X - p.X - radius, point.Y - p.Y - radius, 
                radius * 2, radius * 2);
        }

        public override bool InSide(Point p)
        {
            return ((point.X - p.X) * (point.X - p.X) +
                (point.Y - p.Y) * (point.Y - p.Y) <= 
                radius * radius);
        }

        public void Remove()
        {
            Field.DelItem(this);
        }

        public override void Clear()
        {
        }

        public override void Move(Point start, Point finish)
        {
            Move(finish.X - start.X, finish.Y - start.Y);
        }

        public override void Move(int dx, int dy)
        {
            point.X += dx;
            point.Y += dy;
        }

        public override void Move(Point center)
        {
            point.X = center.X;
            point.Y = center.Y;
        }

        public override System.Xml.XmlElement ToXml(System.Xml.XmlDocument xmldoc)
        {
            throw new NotImplementedException();
        }
    }

    public class GraphicStateCaption : GraphicText
    {
        public GraphicStateCaption(IFieldItem item, IGraphicItem gitem, TextOption option)
            : base(item, gitem, "gsc", 3, new Point(-20, -38), option)
        {
        }
    }
}
