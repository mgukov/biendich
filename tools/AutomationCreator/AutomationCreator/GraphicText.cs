﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Graphic;

namespace AutomationCreator
{
    public class TextOption
    {
        public TextOption(Color color, TextType type)
        {
            this.Color = color;
            this.Type = type;
        }
        public enum TextType : int
        {
            Name, Type, Parametrs, Invisible
        };
        public Color Color;
        public TextType Type;
    }

    public class GraphicText : AGraphicItem, IReflection
    {
        private static int count = 0;
        private static string curr() { return count++.ToString(); }

        private IFieldItem item;
        private IGraphicItem gitem;
        private Point vector;
        private TextOption option;

        private string GetText()
        {
            if (option.Type == TextOption.TextType.Name) return item.Name;
            else if (option.Type == TextOption.TextType.Parametrs) return item.Parametrs;
            else return item.Type;
        }


        public IReflectable[] Objects
        {
            get { return new IReflectable[] { item }; }
        }

        public override Point Point
        {
            get { return vector; }
        }

        public GraphicText(IFieldItem item, IGraphicItem gitem, 
            string prefix, int layer, Point vector, TextOption option)
            : base(layer, prefix + curr())
        {
            this.item = item;
            this.gitem = gitem;
            this.vector = vector;
            this.option = option;

            item.AddReflection(this);
        }

        public void Remove()
        {
            Field.DelItem(this);
        }

        public override void Draw(Graphics graphics, Point p)
        {
            if (option.Type == TextOption.TextType.Invisible) return;

            Point point = new Point(
                gitem.Point.X + vector.X, gitem.Point.Y + vector.Y);

            if (p.X > point.X || p.Y > point.Y ||
                p.X + Field.Width < point.X || p.Y + Field.Width < point.Y) 
                return;

            graphics.DrawString(
                this.GetText(), new Font(FontFamily.GenericMonospace, 8),
                new SolidBrush(option.Color), new PointF(point.X - p.X, point.Y - p.Y));
        }

        public override bool InSide(Point p)
        {
            if (option.Type == TextOption.TextType.Invisible) return false;

            Point point = new Point(
                gitem.Point.X + vector.X, gitem.Point.Y + vector.Y);

            SizeF size = Field.Graphics.MeasureString(this.GetText(),
                new Font(FontFamily.GenericMonospace, 8));

            return (p.X - point.X <= size.Width && p.X - point.X >= 0 &&
                p.Y - point.Y <= size.Height && p.Y - point.Y >= 0);
        }

        public override void Move(Point start, Point finish)
        {
            Move(finish.X - start.X, finish.Y - start.Y);
        }

        public override void Move(int dx, int dy)
        {
            vector.X += dx;
            vector.Y += dy;
        }

        public override void Move(Point center)
        {
            vector.X = center.X;
            vector.Y = center.Y;
        }
    }
}
