﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml;
using System.Linq;
using System.Windows.Forms;
using Graphic;
using Biendich.Utility;
using Biendich.Language;

namespace AutomationCreator
{
    public class FieldData
    {
        private Dictionary<string, XmlNode> states;
        private Dictionary<string, XmlNode> conditions;
        private Dictionary<string, XmlNode> jumps;

        public FieldData(XmlNode root)
        {
            states = new Dictionary<string, XmlNode>();
            conditions = new Dictionary<string, XmlNode>();
            jumps = new Dictionary<string, XmlNode>();

            foreach (XmlNode node in root["state_types"])
            {
                states.Add(node.Attributes["type"].Value, node);
            }
            foreach (XmlNode node in root["condition_types"])
            {
                conditions.Add(node.Attributes["type"].Value, node);
            }
            foreach (XmlNode node in root["jump_types"])
            {
                jumps.Add(node.Attributes["lable"].Value, node);
            }
        }

        public string AttributeState(string type, string attr)
        {
            return states[type].Attributes[attr].Value;
        }
        public string AttributeCond(string type, string attr)
        {
            return conditions[type].Attributes[attr].Value;
        }
        public string AttributeJump(string type, string attr)
        {
            return jumps[type].Attributes[attr].Value;
        }

        public string[] States()
        {
            return states.Keys.ToArray();
        }
        public string[] Conditions()
        {
            return conditions.Keys.ToArray();
        }
        public string[] Jumps()
        {
            return jumps.Keys.ToArray();
        }
    }

    public class Field
    {
        private Dictionary<string, State> states;
        private Dictionary<string, Jump> jumps;

        private LinkedList<State> states_sort;
        private LinkedList<Jump> jumps_sort;

        private FieldData data;
        private GraphicField gfield;

        private int jump_id;
        private TextOption[] text_options;

        public Field(GraphicField gfield, TextOption[] text_options)
        {
            this.jump_id = 0;
            this.gfield = gfield;
            this.text_options = text_options;

            states = new Dictionary<string, State>();
            jumps = new Dictionary<string, Jump>();
            data = null;

            states_sort = new LinkedList<State>();
            jumps_sort = new LinkedList<Jump>();
        }

        public int JumpsCount
        {
            get { return jumps.Count; }
        }

        public int StatesCount
        {
            get { return states.Count; }
        }

        public FieldData Data
        {
            get { return data; }
        }

        public void AddState(State s)
        {
            states.Add(s.Name, s);
            states_sort.AddLast(s);
        }

        public void AddJump(Jump j)
        {
            jumps.Add(j.Name, j);
            jumps_sort.AddLast(j);
        }

        public void MoveState(State pos, State s)
        {
            states_sort.Remove(s);
            if (pos != null)
            {
                LinkedListNode<State> p = states_sort.Find(pos);
                states_sort.AddAfter(p, s);
            }
            else states_sort.AddFirst(s);
        }

        public void MoveJump(Jump pos, Jump j)
        {
            jumps_sort.Remove(j);
            if (pos != null)
            {
                LinkedListNode<Jump> p = jumps_sort.Find(pos);
                jumps_sort.AddAfter(p, j);
            }
            else jumps_sort.AddFirst(j);
        }

        public string GetJumpName()
        {
            jump_id = Math.Max(JumpsCount, jump_id + 1);
            while (jumps.ContainsKey("j_" + jump_id.ToString()))
                ++jump_id;
            return "j_" + jump_id.ToString();
        }

        public void DelState(State s)
        {
            states.Remove(s.Name);
            states_sort.Remove(s);
        }

        public void DelJump(Jump j)
        {
            jumps.Remove(j.Name);
            jumps_sort.Remove(j);
        }


        public State GetState(string name)
        {
            return states[name];
        }

        public bool ContainState(string name)
        {
            return states.ContainsKey(name);
        }

        public bool ContainJump(string name)
        {
            return jumps.ContainsKey(name);
        }

        public State[] GetStates()
        {
            int i = 0;
            State[] ss = new State[StatesCount];
            foreach (State s in states_sort)
            {
                ss[i] = s;
                ++i;
            }
            return ss;
        }

        public Jump[] GetJumps()
        {
            int i = 0;
            Jump[] js = new Jump[JumpsCount];
            foreach (Jump s in jumps_sort)
            {
                js[i] = s;
                ++i;
            }
            return js;
        }

        public bool StateEdit(string name, State s)
        {
            if (s.Name != name && ContainState(s.Name))
                return false;
            else if (s.Name == name)
                return true;
            states.Remove(name);
            states.Add(s.Name, s);
            return true;
        }

        public bool JumpEdit(string name, Jump j)
        {
            if (j.Name != name && ContainJump(j.Name))
                return false;
            else if (j.Name == name) 
                return true;
            jumps.Remove(name);
            jumps.Add(j.Name, j);
            return true;
        }


        public void Save(string path)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement
                auto = doc.CreateElement("auto"),
                xmlstates = doc.CreateElement("states"),
                xmljumps = doc.CreateElement("jumps");
            auto.AppendChild(xmlstates);
            auto.AppendChild(xmljumps);
            doc.AppendChild(auto);

            foreach (State s in states_sort)
            {
                XmlElement state = doc.CreateElement("state");

                XmlAttribute attr = doc.CreateAttribute("type");
                attr.Value = s.Type;
                state.Attributes.Append(attr);

                attr = doc.CreateAttribute("name");
                attr.Value = s.Name;
                state.Attributes.Append(attr);

                attr = doc.CreateAttribute("param");
                attr.Value = s.Parametrs;
                state.Attributes.Append(attr);

                attr = doc.CreateAttribute("gen");
                attr.Value = s.Generation.ToString();
                state.Attributes.Append(attr);

                Point p = ((IGraphicItem)s.Reflections[0]).Point;

                attr = doc.CreateAttribute("point_s");
                attr.Value = p.X.ToString() + "," + p.Y.ToString();
                state.Attributes.Append(attr);

                p = ((IGraphicItem)s.Reflections[1]).Point;

                attr = doc.CreateAttribute("point_sn");
                attr.Value = p.X.ToString() + "," + p.Y.ToString();
                state.Attributes.Append(attr);

                xmlstates.AppendChild(state);
            }
            foreach (Jump j in jumps_sort)
            {
                XmlElement jump = doc.CreateElement("jump");

                XmlAttribute attr = doc.CreateAttribute("type");
                attr.Value = j.Type;
                jump.Attributes.Append(attr);

                attr = doc.CreateAttribute("name");
                attr.Value = j.Name;
                jump.Attributes.Append(attr);

                attr = doc.CreateAttribute("param");
                attr.Value = j.Parametrs;
                jump.Attributes.Append(attr);

                attr = doc.CreateAttribute("first");
                attr.Value = j.First.Name;
                jump.Attributes.Append(attr);

                attr = doc.CreateAttribute("second");
                attr.Value = j.Second.Name;
                jump.Attributes.Append(attr);

                Point p = ((IGraphicItem)j.Reflections[1]).Point;

                attr = doc.CreateAttribute("point_jn");
                attr.Value = p.X.ToString() + "," + p.Y.ToString();
                jump.Attributes.Append(attr);

                xmljumps.AppendChild(jump);
            }
            doc.Save(path);
        }

        public void LoadData(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);

            data = new FieldData(doc["data"]);
        }

        public bool Load(string path)
        {
            try 
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);

                XmlNode auto = doc["auto"];
                foreach (XmlNode node in doc["auto"]["states"])
                {
                    string[] xy = node.Attributes["point_s"].Value.Split(',');
                    Point point = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));

                    State s = new State(
                        node.Attributes["type"].Value,
                        node.Attributes["name"].Value, 
                        node.Attributes["param"].Value,
                        bool.Parse(node.Attributes["gen"].Value),
                        this, gfield, point, text_options[0]);

                    xy = node.Attributes["point_sn"].Value.Split(',');
                    point = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));

                    ((GraphicStateCaption)s.Reflections[1]).Move(point);

                    AddState(s);
                }

                foreach (XmlNode node in doc["auto"]["jumps"])
                {
                    Jump j = new Jump(node.Attributes["type"].Value,
                        node.Attributes["name"].Value,
                        node.Attributes["param"].Value,
                        GetState(node.Attributes["first"].Value),
                        GetState(node.Attributes["second"].Value), this, gfield, text_options[1]);

                    string[] xy = node.Attributes["point_jn"].Value.Split(',');
                    Point p = new Point(Convert.ToInt32(xy[0]), Convert.ToInt32(xy[1]));

                    ((GraphicJumpCaption)j.Reflections[1]).Move(p);

                    AddJump(j);
                }

                return true;
            }
            catch
            {
                MessageBox.Show("Неврный формат файла", "Ошибка загрузки",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public string GenerateCSharp()
        {
            StringBuilder code = new StringBuilder();
            Lang lang = new Lang("C:/test/lang.xml");

            foreach (string j in data.Jumps())
            {
                string 
                    jtype = data.AttributeJump(j, "type"), 
                    jname = data.AttributeJump(j, "lable");
                code.Append(jtype);
                code.Append(" ");
                code.Append(jname);
                code.Append(" = new ");
                code.Append(jtype);
                code.Append("(new Token(");
                code.Append(lang.TokenId(data.AttributeJump(j, "i")));
                code.Append("), new ");
                code.Append(data.AttributeJump(j, "cond") + "(), ");
                code.Append(data.AttributeJump(j, "sh"));
                code.Append(");");
                code.AppendLine();
            }
            code.AppendLine();

            foreach (State s in states_sort)
            {
                if (s.Generation) continue;

                string[] param_val = s.Parametrs.Split(
                    new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
                StringBuilder param = new StringBuilder();

                if (param_val.Length > 0) 
                    param.Append('"' + param_val[0] + '"');
                for (int i = 1; i < param_val.Length; ++i)
                {
                    param.Append(", ");
                    param.Append('"' + param_val[i] + '"');

                }
                code.AppendLine(
                    s.Type + " " + s.Name + " = new " + s.Type + "(" + param.ToString() + ");");
            }
            code.AppendLine();

            foreach (Jump j in jumps_sort)
            {
                code.AppendLine("Jump.Join(" + j.First.Name + ", " +
                    j.Second.Name + ", " + j.Parametrs + ");");
            }
            return code.ToString();
        }

        public XmlDocument GenerateXML()
        {
            StringBuilder code = new StringBuilder();
            Lang lang = new Lang("C:/test/lang.xml");
            XmlDocument xmldoc = new XmlDocument();

            XmlElement
                root = xmldoc.CreateElement("syntax"),
                xml_states = xmldoc.CreateElement("states"),
                xml_jumps = xmldoc.CreateElement("jumps");

            root.AppendChild(xml_states);
            root.AppendChild(xml_jumps);
            xmldoc.AppendChild(root);

            foreach (State s in states_sort)
            {
                if (s.Generation) continue;

                XmlElement state = xmldoc.CreateElement("state");

                XmlAttribute attr = xmldoc.CreateAttribute("id");
                attr.Value = s.Id.ToString();
                state.Attributes.Append(attr);

                attr = xmldoc.CreateAttribute("type");
                attr.Value = data.AttributeState(s.Type, "lable");
                state.Attributes.Append(attr);

                attr = xmldoc.CreateAttribute("params");
                attr.Value = s.Parametrs;
                state.Attributes.Append(attr);
                xml_states.AppendChild(state);
            }

            foreach (Jump j in jumps_sort)
            {
                XmlElement jump = xmldoc.CreateElement("jump");

                XmlAttribute attr = xmldoc.CreateAttribute("id");
                attr.Value = j.Id.ToString();
                jump.Attributes.Append(attr);

                attr = xmldoc.CreateAttribute("s");
                attr.Value = j.First.Id.ToString();
                jump.Attributes.Append(attr);

                attr = xmldoc.CreateAttribute("f");
                attr.Value = j.Second.Id.ToString();
                jump.Attributes.Append(attr);

                attr = xmldoc.CreateAttribute("sh");
                attr.Value = data.AttributeJump(j.Parametrs, "sh");
                jump.Attributes.Append(attr);


                attr = xmldoc.CreateAttribute("c");
                attr.Value = data.AttributeCond(
                    data.AttributeJump(j.Parametrs, "cond"), "lable");
                jump.Attributes.Append(attr);

                attr = xmldoc.CreateAttribute("i");
                attr.Value = lang.TokenId(data.AttributeJump(j.Parametrs, "i")).ToString();
                jump.Attributes.Append(attr);

                xml_jumps.AppendChild(jump);
            }


            return xmldoc;
        }
    }
}