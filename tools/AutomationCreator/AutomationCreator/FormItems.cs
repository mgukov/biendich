﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Biendich.Utility;

namespace AutomationCreator
{
    public partial class FormItems : Form
    {
        private Field field;
        private List<IFieldItem> items;
        private StateOptions form_dialog;
        private string type;
        private Graphics graphics;

        public Object ShowAsDialog(Field field, string type)
        {
            this.field = field;
            this.type = type;

            this.UpdateList();

            this.ShowDialog();
            return null;
        }

        private void UpdateList()
        {
            this.items.Clear();
            this.dataGridView1.Rows.Clear();
            this.dataGridView1.Columns.Clear();

            if (this.type == "s") this.items.AddRange(field.GetStates());
            else this.items.AddRange(field.GetJumps());

            if (items.Count == 0) return;

            string[] cols = items[0].DescriptionCols();
            this.dataGridView1.ColumnCount = cols.Length + 1;
            this.dataGridView1.Columns[0].Name = "Позиция";
            for (int i = 0; i < cols.Length; ++i)
            {
                this.dataGridView1.Columns[i + 1].Name = cols[i];
            }

            for (int i = 0; i < items.Count; ++i)
            {
                List<Object> desc = new List<Object>();
                desc.Add(i);
                desc.AddRange(items[i].Description());
                dataGridView1.Rows.Add(desc.ToArray());
            }       
        }

        public FormItems()
        {
            InitializeComponent();
            form_dialog = new StateOptions();

            graphics = this.CreateGraphics();

            this.items = new List<IFieldItem>();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }

        private void Items_Load(object sender, EventArgs e)
        {
        }

        private void cmdChange_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) return;
            int i = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            IFieldItem item = items[i];

            if (item.Edit())
            {
                this.UpdateList();
                dataGridView1.Rows[i].Selected = true;
            }
        }

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) return;

            int i = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            items[i].Remove();
            items.RemoveAt(i);

            this.UpdateList();
            if (i < items.Count) dataGridView1.Rows[i].Selected = true;
            else if (items.Count > 0) dataGridView1.Rows[i - 1].Selected = true;
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            cmdChange_Click(sender, e);
        }

        private void cmdUp_Click(object sender, EventArgs e)
        {

            int index = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            if (index <= 0) return;

            IFieldItem pos = null;

            if (index > 1)
            {
                pos = items[index - 2];
            }
            if (items[0] is State) field.MoveState((State)pos, (State)items[index]);
            else field.MoveJump((Jump)pos, (Jump)items[index]);

            this.UpdateList();
            if (index > 0) dataGridView1.Rows[index - 1].Selected = true;
        }

        private void cmdDown_Click(object sender, EventArgs e)
        {
            int index = (int)dataGridView1.SelectedRows[0].Cells[0].Value;

            if (index < 0 || index == items.Count - 1) return;

            IFieldItem pos = items[index + 1];

            if (items[0] is State) field.MoveState((State)pos, (State)items[index]);
            else field.MoveJump((Jump)pos, (Jump)items[index]);

            this.UpdateList();
            if (index < items.Count - 1) dataGridView1.Rows[index + 1].Selected = true;
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            cmdChange_Click(sender, e);
        }

    }
}
