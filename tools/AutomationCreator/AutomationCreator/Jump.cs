﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Xml;
using Graphic;
using Biendich.Utility;

namespace AutomationCreator
{
    public class Jump : IFieldItem
    {
        private static int counter = 0;
        private static int getid() { return counter++; }

        private int id;

        private string type;
        private string name;
        private string parametrs;

        private State first;
        private State second;
        private List<IReflection> reflections;
        private Field field;

        public int Id
        {
            get { return id; }
        }
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Parametrs
        {
            get { return parametrs; }
            set { parametrs = value; }
        }

        public State First
        {
            get { return first; }
        }
        public State Second
        {
            get { return second; }
        }

        public Jump(string type, string name, string parametrs,
            State first, State second, Field field, GraphicField gfield, TextOption text_option)
        {
            this.type = type;
            this.name = name;
            this.parametrs = parametrs;
            this.first = first;
            this.second = second;
            this.reflections = new List<IReflection>();
            this.field = field;
            this.id = getid();

            first.AddJumpOut(this);
            second.AddJumpIn(this);

            GraphicJump gjump = 
                new GraphicJump(this, (GraphicStateItem)first.Reflections[0],
                (GraphicStateItem)second.Reflections[0]);
            GraphicJumpCaption gjname =
                new GraphicJumpCaption(this, gjump, text_option);

            this.AddReflection(gjump);
            this.AddReflection(gjname);
            gfield.AddItem(gjump);
            gfield.AddItem(gjname);
        }

        public void Remove()
        {
            first.DelJumpOut(this);
            second.DelJumpIn(this);
            field.DelJump(this);

            foreach (IReflection item in reflections)
            {
                item.Remove();
            }
        }

        public void AddReflection(IReflection reflection)
        {
            reflections.Add(reflection);
        }

        public IReflection[] Reflections
        {
            get { return reflections.ToArray(); }
        }

        public bool Edit()
        {
            Pair<bool, Object[]> p = GetForm().ShowAsDialog(
                this.type, this.name, this.parametrs, field.Data, false);

            if (!p.First) return false;

            string name = this.name;
            this.name = p.Second[1].ToString();

            if (field.JumpEdit(name, this))
            {
                this.type = p.Second[0].ToString();
                this.parametrs = p.Second[2].ToString();
                return true;
            }
            this.name = name;
            return false;
        }

        public string[] Description()
        {
            string[] desc = new string[] {
                 this.Type, this.Name, this.Parametrs,
                 this.First.Name, this.second.Name };
            return desc;
        }
        public string[] DescriptionCols()
        {
            string[] desc = new string[] { 
                "Тип", "Имя", "Параметры", "Начало", "Финиш" };
            return desc;
        }

        public IFieldItemForm GetForm()
        {
            return FormJump.GetForm();
        }
    }

    public class GraphicJump : AGraphicItem, IReflection
    {

        private static Color color = Color.Lime;
        public static Color Color
        {
            get { return color; }
            set { color = value; }
        }

        private static Color point_color = Color.Yellow;
        public static Color PointColor
        {
            get { return point_color; }
            set { point_color = value; }
        }

        private static int count = 0;
        private static string curr() { return count++.ToString(); }

        private Jump jump;
        private GraphicStateItem s1;
        private GraphicStateItem s2;
        private Point point;

        public IReflectable[] Objects
        {
            get { return new IReflectable[] { jump }; }
        }

        public override Point Point 
        {
            get { return point; }
        }

        public GraphicJump(Jump jump, GraphicStateItem s1, 
            GraphicStateItem s2)
            : base(2, "gj" + curr())
        {
            this.jump = jump;
            this.s1 = s1;
            this.s2 = s2;
            this.point = new Point();

            jump.AddReflection(this);
        }

        public override void Draw(Graphics graphics, Point p)
        {

            float
                x1 = s1.Point.X,
                y1 = s1.Point.Y,

                x2 = s2.Point.X,
                y2 = s2.Point.Y;

            point.X = (int)((x1 + x2) / 2);
            point.Y = (int)((y1 + y2) / 2);

            float
                m = (float)Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)),
                sina = (y2 - y1) / m,
                cosa = (x2 - x1) / m,
                dx = s1.Radius * cosa,
                dy = s1.Radius * sina;

            if (p.X > Math.Max(x1, x2) || p.Y > Math.Max(y1, y2) ||
                p.X + Field.Width < Math.Min(x1, x2) || p.Y + Field.Width < Math.Min(y1, y2))
                return;


            if (x1 == x2 && y1 == y2)
            {
                graphics.FillEllipse(new SolidBrush(point_color),
                    x2 - p.X - 2, y2 - p.Y - 2, 4, 4);
            }
            else
            {
                graphics.DrawLine(new Pen(color, 1),
                    x1 - p.X + dx, y1 - p.Y + dy, x2 - p.X - dx, y2 - p.Y - dy);
                graphics.FillEllipse(new SolidBrush(point_color),
                    x2 - p.X - dx - 2, y2 - p.Y - dy - 2, 4, 4);
            }
        }

        public override bool InSide(Point p)
        {
            return false;
        }

        public void Remove()
        {
            Field.DelItem(this);
        }

        public override void Move(Point start, Point finish)
        {
        }

        public override void Move(int dx, int dy)
        {
        }

        public override void Move(Point center)
        {
        }

        public override XmlElement ToXml(XmlDocument xmldoc)
        {
            throw new NotImplementedException();
        }
    }

    public class GraphicJumpCaption : GraphicText 
    {
        public GraphicJumpCaption(IFieldItem item, IGraphicItem gitem, TextOption option)
            : base(item, gitem, "gjc", 4, new Point(-10, -20), option)
        {
        }
    }
}
