﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Biendich.Utility;

namespace AutomationCreator
{
    public partial class AddItem : Form
    {
        private Field field;

        public AddItem()
        {
            InitializeComponent();
        }

        public void ShowAsDialog(Field field)
        {
            this.field = field;
            UpdateList();
            this.ShowDialog();
        }

        private void UpdateList()
        {
            listBox1.Items.Clear();

            foreach (Pair<string, string> item in field.StateTypes())
            {
                listBox1.Items.Add(item.First + " : " + item.Second);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Поля тип и имя должны быть заполнены", "Ошибка", 
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            this.field.AddStateType(textBox1.Text, textBox2.Text);
            textBox1.Text = ""; 
            textBox2.Text = "";
            UpdateList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                field.DelStateType(listBox1.SelectedIndex);
                UpdateList();
            }
        }
    }
}
