﻿namespace AutomationCreator
{
    partial class FormOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.cmdJump = new System.Windows.Forms.Button();
            this.cmdStateName = new System.Windows.Forms.Button();
            this.cmdState = new System.Windows.Forms.Button();
            this.cmdBackground = new System.Windows.Forms.Button();
            this.cmdJumpName = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdPoint = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmdJump
            // 
            this.cmdJump.Location = new System.Drawing.Point(198, 16);
            this.cmdJump.Name = "cmdJump";
            this.cmdJump.Size = new System.Drawing.Size(85, 41);
            this.cmdJump.TabIndex = 0;
            this.cmdJump.Text = "Прыжки";
            this.cmdJump.UseVisualStyleBackColor = true;
            this.cmdJump.Click += new System.EventHandler(this.cmdJump_Click);
            // 
            // cmdStateName
            // 
            this.cmdStateName.Location = new System.Drawing.Point(16, 63);
            this.cmdStateName.Name = "cmdStateName";
            this.cmdStateName.Size = new System.Drawing.Size(85, 41);
            this.cmdStateName.TabIndex = 1;
            this.cmdStateName.Text = "Названия состояний";
            this.cmdStateName.UseVisualStyleBackColor = true;
            this.cmdStateName.Click += new System.EventHandler(this.cmdStateName_Click);
            // 
            // cmdState
            // 
            this.cmdState.Location = new System.Drawing.Point(107, 16);
            this.cmdState.Name = "cmdState";
            this.cmdState.Size = new System.Drawing.Size(85, 41);
            this.cmdState.TabIndex = 2;
            this.cmdState.Text = "Состояния";
            this.cmdState.UseVisualStyleBackColor = true;
            this.cmdState.Click += new System.EventHandler(this.cmdState_Click);
            // 
            // cmdBackground
            // 
            this.cmdBackground.Location = new System.Drawing.Point(16, 16);
            this.cmdBackground.Name = "cmdBackground";
            this.cmdBackground.Size = new System.Drawing.Size(85, 41);
            this.cmdBackground.TabIndex = 3;
            this.cmdBackground.Text = "Фон";
            this.cmdBackground.UseVisualStyleBackColor = true;
            this.cmdBackground.Click += new System.EventHandler(this.cmdBackground_Click);
            // 
            // cmdJumpName
            // 
            this.cmdJumpName.Location = new System.Drawing.Point(107, 63);
            this.cmdJumpName.Name = "cmdJumpName";
            this.cmdJumpName.Size = new System.Drawing.Size(85, 41);
            this.cmdJumpName.TabIndex = 4;
            this.cmdJumpName.Text = "Названия прыжков";
            this.cmdJumpName.UseVisualStyleBackColor = true;
            this.cmdJumpName.Click += new System.EventHandler(this.cmdJumpName_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Location = new System.Drawing.Point(198, 128);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(85, 29);
            this.cmdOK.TabIndex = 5;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdPoint
            // 
            this.cmdPoint.Location = new System.Drawing.Point(198, 63);
            this.cmdPoint.Name = "cmdPoint";
            this.cmdPoint.Size = new System.Drawing.Size(85, 41);
            this.cmdPoint.TabIndex = 6;
            this.cmdPoint.Text = "Точка соединения";
            this.cmdPoint.UseVisualStyleBackColor = true;
            this.cmdPoint.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 171);
            this.Controls.Add(this.cmdPoint);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdJumpName);
            this.Controls.Add(this.cmdBackground);
            this.Controls.Add(this.cmdState);
            this.Controls.Add(this.cmdStateName);
            this.Controls.Add(this.cmdJump);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormOptions";
            this.Text = "Опции";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button cmdJump;
        private System.Windows.Forms.Button cmdStateName;
        private System.Windows.Forms.Button cmdState;
        private System.Windows.Forms.Button cmdBackground;
        private System.Windows.Forms.Button cmdJumpName;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.Button cmdPoint;
    }
}