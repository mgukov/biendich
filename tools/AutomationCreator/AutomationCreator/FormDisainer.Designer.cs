﻿namespace AutomationCreator
{
    partial class FormDesigner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cheselect = new System.Windows.Forms.CheckBox();
            this.chestate = new System.Windows.Forms.CheckBox();
            this.chejump = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStateCaption = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStateInvi = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStateName = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStateType = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStatePara = new System.Windows.Forms.ToolStripMenuItem();
            this.menuJumpCaption = new System.Windows.Forms.ToolStripMenuItem();
            this.menuJumpInvi = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuJumpName = new System.Windows.Forms.ToolStripMenuItem();
            this.menuJumpType = new System.Windows.Forms.ToolStripMenuItem();
            this.menuJumpPara = new System.Windows.Forms.ToolStripMenuItem();
            this.элементыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStates = new System.Windows.Forms.ToolStripMenuItem();
            this.menuJumps = new System.Windows.Forms.ToolStripMenuItem();
            this.генерироватьКодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCSharp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuXML = new System.Windows.Forms.ToolStripMenuItem();
            this.опцииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.cheselect);
            this.groupBox1.Controls.Add(this.chestate);
            this.groupBox1.Controls.Add(this.chejump);
            this.groupBox1.Location = new System.Drawing.Point(554, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(116, 407);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Инструменты";
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button4.Location = new System.Drawing.Point(14, 370);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 31);
            this.button4.TabIndex = 9;
            this.button4.Text = "Изменить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.cmdEdit_Click);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button3.Location = new System.Drawing.Point(14, 333);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(85, 31);
            this.button3.TabIndex = 6;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.cmdDel_Click);
            // 
            // cheselect
            // 
            this.cheselect.Appearance = System.Windows.Forms.Appearance.Button;
            this.cheselect.Location = new System.Drawing.Point(14, 95);
            this.cheselect.Name = "cheselect";
            this.cheselect.Size = new System.Drawing.Size(85, 31);
            this.cheselect.TabIndex = 5;
            this.cheselect.Text = "Выделение";
            this.cheselect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cheselect.UseVisualStyleBackColor = true;
            this.cheselect.CheckedChanged += new System.EventHandler(this.cheselect_CheckedChanged);
            // 
            // chestate
            // 
            this.chestate.Appearance = System.Windows.Forms.Appearance.Button;
            this.chestate.Location = new System.Drawing.Point(14, 21);
            this.chestate.Name = "chestate";
            this.chestate.Size = new System.Drawing.Size(85, 31);
            this.chestate.TabIndex = 0;
            this.chestate.Text = "Состояние";
            this.chestate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chestate.UseVisualStyleBackColor = true;
            this.chestate.CheckedChanged += new System.EventHandler(this.chestate_CheckedChanged);
            // 
            // chejump
            // 
            this.chejump.Appearance = System.Windows.Forms.Appearance.Button;
            this.chejump.Location = new System.Drawing.Point(14, 58);
            this.chejump.Name = "chejump";
            this.chejump.Size = new System.Drawing.Size(85, 31);
            this.chejump.TabIndex = 1;
            this.chejump.Text = "Прыжок";
            this.chejump.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chejump.UseVisualStyleBackColor = true;
            this.chejump.CheckedChanged += new System.EventHandler(this.chejump_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.видToolStripMenuItem,
            this.элементыToolStripMenuItem,
            this.генерироватьКодToolStripMenuItem,
            this.опцииToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(670, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCreate,
            this.menuOpen,
            this.сохранитьToolStripMenuItem,
            this.menuSave,
            this.toolStripMenuItem1,
            this.закрытьToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // menuCreate
            // 
            this.menuCreate.Name = "menuCreate";
            this.menuCreate.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuCreate.Size = new System.Drawing.Size(173, 22);
            this.menuCreate.Text = "Создать";
            this.menuCreate.Click += new System.EventHandler(this.menuCreate_Click);
            // 
            // menuOpen
            // 
            this.menuOpen.Name = "menuOpen";
            this.menuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuOpen.Size = new System.Drawing.Size(173, 22);
            this.menuOpen.Text = "Открыть";
            this.menuOpen.Click += new System.EventHandler(this.menuOpen_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.menuSave_Click);
            // 
            // menuSave
            // 
            this.menuSave.Name = "menuSave";
            this.menuSave.Size = new System.Drawing.Size(173, 22);
            this.menuSave.Text = "Сохрнить как";
            this.menuSave.Click += new System.EventHandler(this.menuSaveAs_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(170, 6);
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.закрытьToolStripMenuItem.Text = "Закрыть";
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.menuClose_Click);
            // 
            // видToolStripMenuItem
            // 
            this.видToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStateCaption,
            this.menuJumpCaption});
            this.видToolStripMenuItem.Name = "видToolStripMenuItem";
            this.видToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.видToolStripMenuItem.Text = "Вид";
            // 
            // menuStateCaption
            // 
            this.menuStateCaption.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStateInvi,
            this.toolStripMenuItem2,
            this.menuStateName,
            this.menuStateType,
            this.menuStatePara});
            this.menuStateCaption.Name = "menuStateCaption";
            this.menuStateCaption.Size = new System.Drawing.Size(186, 22);
            this.menuStateCaption.Text = "Подпись состояний";
            // 
            // menuStateInvi
            // 
            this.menuStateInvi.CheckOnClick = true;
            this.menuStateInvi.Name = "menuStateInvi";
            this.menuStateInvi.ShortcutKeyDisplayString = "";
            this.menuStateInvi.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.X)));
            this.menuStateInvi.ShowShortcutKeys = false;
            this.menuStateInvi.Size = new System.Drawing.Size(149, 22);
            this.menuStateInvi.Text = "Не показывать";
            this.menuStateInvi.Click += new System.EventHandler(this.menuStateCaption_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(146, 6);
            // 
            // menuStateName
            // 
            this.menuStateName.CheckOnClick = true;
            this.menuStateName.Name = "menuStateName";
            this.menuStateName.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.N)));
            this.menuStateName.ShowShortcutKeys = false;
            this.menuStateName.Size = new System.Drawing.Size(149, 22);
            this.menuStateName.Text = "Имя";
            this.menuStateName.Click += new System.EventHandler(this.menuStateCaption_Click);
            // 
            // menuStateType
            // 
            this.menuStateType.CheckOnClick = true;
            this.menuStateType.Name = "menuStateType";
            this.menuStateType.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.T)));
            this.menuStateType.ShowShortcutKeys = false;
            this.menuStateType.Size = new System.Drawing.Size(149, 22);
            this.menuStateType.Text = "Тип";
            this.menuStateType.Click += new System.EventHandler(this.menuStateCaption_Click);
            // 
            // menuStatePara
            // 
            this.menuStatePara.CheckOnClick = true;
            this.menuStatePara.Name = "menuStatePara";
            this.menuStatePara.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.P)));
            this.menuStatePara.ShowShortcutKeys = false;
            this.menuStatePara.Size = new System.Drawing.Size(149, 22);
            this.menuStatePara.Text = "Параметры";
            this.menuStatePara.Click += new System.EventHandler(this.menuStateCaption_Click);
            // 
            // menuJumpCaption
            // 
            this.menuJumpCaption.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuJumpInvi,
            this.toolStripMenuItem3,
            this.menuJumpName,
            this.menuJumpType,
            this.menuJumpPara});
            this.menuJumpCaption.Name = "menuJumpCaption";
            this.menuJumpCaption.Size = new System.Drawing.Size(186, 22);
            this.menuJumpCaption.Text = "Подпись прыжков";
            // 
            // menuJumpInvi
            // 
            this.menuJumpInvi.Name = "menuJumpInvi";
            this.menuJumpInvi.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.X)));
            this.menuJumpInvi.ShowShortcutKeys = false;
            this.menuJumpInvi.Size = new System.Drawing.Size(149, 22);
            this.menuJumpInvi.Text = "Не показывать";
            this.menuJumpInvi.Click += new System.EventHandler(this.menuJumpCaption_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(146, 6);
            // 
            // menuJumpName
            // 
            this.menuJumpName.Name = "menuJumpName";
            this.menuJumpName.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.N)));
            this.menuJumpName.ShowShortcutKeys = false;
            this.menuJumpName.Size = new System.Drawing.Size(149, 22);
            this.menuJumpName.Text = "Имя";
            this.menuJumpName.Click += new System.EventHandler(this.menuJumpCaption_Click);
            // 
            // menuJumpType
            // 
            this.menuJumpType.Name = "menuJumpType";
            this.menuJumpType.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.T)));
            this.menuJumpType.ShowShortcutKeys = false;
            this.menuJumpType.Size = new System.Drawing.Size(149, 22);
            this.menuJumpType.Text = "Тип";
            this.menuJumpType.Click += new System.EventHandler(this.menuJumpCaption_Click);
            // 
            // menuJumpPara
            // 
            this.menuJumpPara.Name = "menuJumpPara";
            this.menuJumpPara.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.P)));
            this.menuJumpPara.ShowShortcutKeys = false;
            this.menuJumpPara.Size = new System.Drawing.Size(149, 22);
            this.menuJumpPara.Text = "Параметры";
            this.menuJumpPara.Click += new System.EventHandler(this.menuJumpCaption_Click);
            // 
            // элементыToolStripMenuItem
            // 
            this.элементыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuStates,
            this.menuJumps});
            this.элементыToolStripMenuItem.Name = "элементыToolStripMenuItem";
            this.элементыToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.элементыToolStripMenuItem.Text = "Элементы";
            // 
            // menuStates
            // 
            this.menuStates.Name = "menuStates";
            this.menuStates.Size = new System.Drawing.Size(135, 22);
            this.menuStates.Text = "Состояния";
            this.menuStates.Click += new System.EventHandler(this.menuStates_Click);
            // 
            // menuJumps
            // 
            this.menuJumps.Name = "menuJumps";
            this.menuJumps.Size = new System.Drawing.Size(135, 22);
            this.menuJumps.Text = "Прыжки";
            this.menuJumps.Click += new System.EventHandler(this.menuJumps_Click);
            // 
            // генерироватьКодToolStripMenuItem
            // 
            this.генерироватьКодToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCSharp,
            this.menuXML});
            this.генерироватьКодToolStripMenuItem.Name = "генерироватьКодToolStripMenuItem";
            this.генерироватьКодToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.генерироватьКодToolStripMenuItem.ShowShortcutKeys = false;
            this.генерироватьКодToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.генерироватьКодToolStripMenuItem.Text = "Генерация";
            // 
            // menuCSharp
            // 
            this.menuCSharp.Name = "menuCSharp";
            this.menuCSharp.Size = new System.Drawing.Size(152, 22);
            this.menuCSharp.Text = "C#";
            this.menuCSharp.Click += new System.EventHandler(this.menuCSharp_Click);
            // 
            // menuXML
            // 
            this.menuXML.Name = "menuXML";
            this.menuXML.Size = new System.Drawing.Size(152, 22);
            this.menuXML.Text = "XML";
            this.menuXML.Click += new System.EventHandler(this.menuXML_Click);
            // 
            // опцииToolStripMenuItem
            // 
            this.опцииToolStripMenuItem.Name = "опцииToolStripMenuItem";
            this.опцииToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.опцииToolStripMenuItem.Text = "Опции";
            this.опцииToolStripMenuItem.Click += new System.EventHandler(this.menuOptions_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(0, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(552, 407);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 437);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(670, 22);
            this.statusStrip1.TabIndex = 6;
            // 
            // FormDesigner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 459);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 450);
            this.Name = "FormDesigner";
            this.Text = "Дизайнер ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Closing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Automation_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chejump;
        private System.Windows.Forms.CheckBox chestate;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuOpen;
        private System.Windows.Forms.ToolStripMenuItem menuSave;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem генерироватьКодToolStripMenuItem;
        private System.Windows.Forms.CheckBox cheselect;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem опцииToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ToolStripMenuItem элементыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuStates;
        private System.Windows.Forms.ToolStripMenuItem menuJumps;
        private System.Windows.Forms.ToolStripMenuItem видToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuStateCaption;
        private System.Windows.Forms.ToolStripMenuItem menuStateInvi;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem menuStateName;
        private System.Windows.Forms.ToolStripMenuItem menuStateType;
        private System.Windows.Forms.ToolStripMenuItem menuStatePara;
        private System.Windows.Forms.ToolStripMenuItem menuJumpCaption;
        private System.Windows.Forms.ToolStripMenuItem menuJumpInvi;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem menuJumpName;
        private System.Windows.Forms.ToolStripMenuItem menuJumpType;
        private System.Windows.Forms.ToolStripMenuItem menuJumpPara;
        private System.Windows.Forms.ToolStripMenuItem menuCreate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuCSharp;
        private System.Windows.Forms.ToolStripMenuItem menuXML;
    }
}

