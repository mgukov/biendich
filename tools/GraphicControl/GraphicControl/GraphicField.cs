﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml;

namespace Graphic
{
    public interface IGraphicItem
    {
        GraphicField Field { get; set; }
        string Name { get; }
        int Layer { get; }
        Point Point { get; }

        void Draw(Graphics graphics, Point p);
        void Move(Point start, Point finish);
        void Move(Point center);
        void Move(int dx, int dy);
        void Clear();
        bool InSide(Point p);
        XmlElement ToXml(XmlDocument xmldoc);
    }

    public interface IReflection
    {
        //void Synchronize();
        //void AddObject(IReflectable obj);
        void Remove();

        IReflectable[] Objects { get; }
    }

    public interface IReflectable
    {
        void AddReflection(IReflection reflection);
        IReflection[] Reflections { get; }
    }

    public interface IGraphicItemReflection : IGraphicItem, IReflection
    {
    }

    public abstract class AGraphicItem : IGraphicItem
    {
        private int layer;
        private string name;
        private GraphicField field;

        public string Name
        {
            get { return name; }
        }

        public GraphicField Field
        {
            get { return field; }
            set { field = value; }
        }

        public int Layer
        {
            get { return layer; }
        }

        public abstract Point Point { get; }

        public AGraphicItem(int layer, string name)
        {
            this.layer = layer;
            this.name = name;
            this.field = null;
        }

        public virtual void Clear() { }

        public abstract void Move(Point start, Point finish);

        public abstract void Move(Point center);

        public abstract void Move(int dx, int dy);

        public abstract void Draw(Graphics graphics, Point p);

        public abstract bool InSide(Point p);

        public virtual XmlElement ToXml(XmlDocument xmldoc) { return null; }

    }

    public class GraphicField
    {
        private Bitmap bitmap;
        private Graphics graphics;
        private int width;
        private int height;
        private Color fon;
        private Point position;

        private Dictionary<string, IGraphicItem> items;
        private List<IGraphicItem> list_items;
        private SortedDictionary<int, List<IGraphicItem>> layer_items;

        public GraphicField(int width, int height)
        {
            this.width = width;
            this.height = height;
            this.position = new Point(0, 0);

            items = new Dictionary<string, IGraphicItem>();
            list_items = new List<IGraphicItem>();
            layer_items = new SortedDictionary<int, List<IGraphicItem>>();
            bitmap = new Bitmap(width, height);
            graphics = Graphics.FromImage(bitmap);

            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            fon = Color.White;
        }

        public Bitmap Image
        {
            get { return bitmap; }
        }

        public Graphics Graphics
        {
            get { return graphics; }
        }

        public Color Fon
        {
            get { return fon; }
            set { fon = value; }
        }
        
        public int ItemsCount
        {
            get { return list_items.Count; }
        }

        public Point Position
        {
            get { return position; }
            set { position = value; }
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public void SetPosition(int x, int y)
        {
            position.X = x;
            position.Y = y;
        }

        public void MovePosition(int dx, int dy)
        {
            position.X += dx;
            position.Y += dy;
        }

        public bool Free(Point p)
        {
            return (this.GetItem(p) == null);
        }

        public void AddItem(IGraphicItem item)
        {
            items.Add(item.Name, item);
            list_items.Add(item);
            List<IGraphicItem> list;

            if (!layer_items.TryGetValue(item.Layer, out list))
            {
                list = new List<IGraphicItem>();
                layer_items.Add(item.Layer, list);
            }
            list.Add(item);
            item.Field = this;
        }

        public IGraphicItem GetItem(string name)
        {
            return items[name];
        }

        public IGraphicItem GetItem(Point p)
        {
            List<IGraphicItem>[] list = layer_items.Values.ToArray();
            for (int i = list.Length - 1; i >= 0; --i)
            {
                foreach (IGraphicItem item in list[i])
                {
                    if (item.InSide(p)) return item;
                }
            }
            return null;
        }

        public void Clear()
        {
            items.Clear();
            list_items.Clear();
            layer_items.Clear();
        }

        public void DelItem(IGraphicItem item)
        {
            items.Remove(item.Name);
            list_items.Remove(item);
            layer_items[item.Layer].Remove(item);
        }

        public void Draw()
        {
            graphics.Clear(fon);
            foreach (IGraphicItem item in list_items)
            {
                item.Draw(graphics, position);
            }
        }

        public void DrawOnLayers()
        {
            graphics.Clear(fon);
            foreach (List<IGraphicItem> list in layer_items.Values)
            {
                foreach (IGraphicItem item in list)
                {
                    item.Draw(graphics, position);
                }
            }
        }

        public void DrawLayers(int[] layers)
        {
            graphics.Clear(fon);
            foreach (KeyValuePair<int, List<IGraphicItem>> pair in layer_items)
            {
                if (layers.Contains(pair.Key))
                {
                    List<IGraphicItem> list = pair.Value;
                    foreach (IGraphicItem item in list)
                    {
                        item.Draw(graphics, position);
                    }
                }
            }
        }

        public IGraphicItem[] Items()
        {
            return list_items.ToArray();
        }

        public virtual void Save(string path)
        {
            //XmlDocument xmldoc = new XmlDocument();
        }

        public virtual void Load(string path)
        {
        }
    }
}