﻿using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Automation;
using Biendich.Lexical;

namespace Biendich.Syntax
{
    public class TokChain : IChain
    {
        private int cursor;
        private List<Token> instructions;
        private Scanner scann;
        private IChain chain;

        public TokChain(Scanner scann, IChain chain)
        {
            cursor = 0;
            instructions = new List<Token>();
            this.scann = scann;
            this.chain = chain;
            Reserve(1);
        }

        public IInstruction Current
        {
            get { return !EOF() ? instructions[cursor] : new Token(-1, cursor, "EOF"); }
        }

        public int Cursor
        {
            get { return cursor; }
            set { this.Move(value - cursor); }
        }

        public int Reserved
        {
            get { return instructions.Count; }
        }

        public IInstruction Deviation(int sh)
        {
            if (sh > 0) Reserve(sh + cursor);
            else if (sh == 0) return Current;
            return (cursor + sh >= 0 &&
                cursor + sh < Reserved) ? instructions[cursor + sh] : null;
        }

        public bool EOF()
        {
            return (cursor >= Reserved && ReserveMore(1) == 0);
        }

        public void Move(int sh)
        {
            Reserve(sh + cursor);
            cursor += sh;
        }

        public bool TryMove(int sh)
        {
            if (sh + cursor == Reserve(sh + cursor))
            {
                cursor += sh;
                return true;
            }
            return false;
        }

        public int ReserveMore(int sh)
        {
            if (sh <= 0) return 0;

            Token tok = (Token)scann.Run(chain);
            int i = 0;

            while(tok != null)
            {
                ++i;
                instructions.Add(tok);
                if (i >= sh) break;
                tok = scann.Run(chain);
            }
            return i;
        }

        public int Reserve(int len)
        {
            ReserveMore(len - Reserved);
            return Reserved;
        }

        public void Close()
        {
        }

    }
}
