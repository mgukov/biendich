﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Lexical;
using Biendich.Automation;

namespace Biendich.Syntax
{
    public class Operator : Expression
    {
        private int priority;
        private int parametrs;
        private char associativity;

        public Operator(int id, IInstruction node, int priority, char associativity, int parametrs)
            : base("operator", id, node)
        {
            this.priority = priority;
            this.associativity = associativity;
            this.parametrs = parametrs;
        }

        public int Priority
        {
            get { return priority; }
        }

        public int Parametrs
        {
            get { return parametrs; }
        }

        public char Associativity
        {
            get { return associativity; }
        }

        public void AddParametr()
        {
            ++parametrs;
        }

        public override void Fill(LinkedListNode<Expression> exp)
        {
            base.Fill(null);
            if (Parametrs == 0) return;
            LinkedListNode<Expression> curr = exp.Previous;
            Stack<Expression> stack = new Stack<Expression>();
            for (int i = 0; i < Parametrs; ++i)
            {
                stack.Push(curr.Value);
                if (i < Parametrs - 1)
                {
                    curr = curr.Previous;
                    curr.List.Remove(curr.Next);
                }
            }
            curr.List.Remove(curr);
            while (stack.Count > 0)
            {
                this.AddStatement(stack.Pop());
            }
        }
        public override XmlNode ToXml(XmlDocument doc)
        {
            XmlNode node = base.ToXml(doc);
            XmlAttribute attr = doc.CreateAttribute("value");
            attr.Value = Value.ToString();
            node.Attributes.Append(attr);
            return node;
        }
    }
}
