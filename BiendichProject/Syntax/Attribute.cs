﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Biendich.Automation;

namespace Biendich.Syntax
{
    public class Attribute : Statement
    {
        public Attribute(int id, IInstruction node)
            : base("attribute", id, node)
        {
        }

        //public override XmlNode ToXml(XmlDocument doc)
        //{
        //    return doc.CreateElement("");
        //}
    }
}
