﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Biendich.Lexical;
using Biendich.Automation;

namespace Biendich.Syntax
{
    public class Error : Expression
    {
        public Error(int id, IInstruction node)
            :base("error", id, node)
        {
        }
    }
}
