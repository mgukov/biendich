using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Language;

namespace Biendich.Syntax
{
	public class Parser : Automaton
	{
        private class StateMove : AState
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                foreach (Jump j in Jumps)
                {
                    if (j.Accept(chain.Current, ref param))
                    {
                        param["PREV"] = this;
                        chain.Move(j.Shift);
                        return j.State;
                    }
                }
                StateError err = new StateError(1000, 0);
                return err;
            }
            public override object GetObject(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("���������� �������� ������ StateMove.GetObject(...)");
            }

            public override bool Motion()
            {
                return true;
            }
        }

        
        private class StateOperand : StateMove
        {
            private int id;
            public StateOperand(int id)
            {
                this.id = id;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Token tok = (Token)chain.Deviation(-1).Clone();

                Operand var = new Operand(id, tok);
                param["CURR"] = var;
                return base.Advance(chain, ref param);
            }
        }

        private class StateOperation : StateMove
        {
            private int id;

            public StateOperation(int id)
            {
                this.id = id;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Lang lang = (Lang)param["LN"];

                Token tok = (Token)chain.Deviation(-1).Clone();

                Operator op = new Operator(id, tok,
                    Convert.ToInt32(lang.InstructionAttribute("operators", id, "priority")),
                    Convert.ToChar(lang.InstructionAttribute("operators", id, "assoc")),
                    Convert.ToInt32(lang.InstructionAttribute("operators", id, "operands")));

                param["CURR"] = op;

                return base.Advance(chain, ref param);
            }
        }

        private class StateStatement : StateMove
        {
            private int id;
            private int sh;
            public StateStatement(int id, int sh = -1)
            {
                this.id = id;
                this.sh = sh;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Lang lang = (Lang)param["LN"];
                Token tok = (Token)chain.Deviation(sh);

                Statement statement = new Statement(id, tok.Clone());
                param["CURR"] = statement;

                return base.Advance(chain, ref param);
            }
        }

        private class StateAttribute : StateMove
        {
            private int id;
            private int sh;
            public StateAttribute(int id, int sh = -1)
            {
                this.id = id;
                this.sh = sh;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {

                Queue<Attribute> attributes = (Queue<Attribute>)param["AT"];
                Token tok = (Token)chain.Deviation(sh).Clone();

                Attribute attr = new Attribute(id, tok);
                attributes.Enqueue(attr);
                param["CURR"] = attr;
                return base.Advance(chain, ref param);
            }

        }

        
        private class StateInsertAttributes : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<Statement> tree = (Stack<Statement>)param["TR"];
                Queue<Attribute> attributes = (Queue<Attribute>)param["AT"];

                if (tree.Count == 0) throw new Exception("������ ���������� ����");
                //if (attributes.Count == 0) throw new Exception("������ ���������� ����");

                Statement root = tree.Peek();
                while (attributes.Count > 0)
                {
                    Attribute attr = attributes.Dequeue();
                    root.AddStatement(attr);
                }

                return base.Advance(chain, ref param);
            }
        }

        private class StateDeep : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<Statement> tree = (Stack<Statement>)param["TR"];

                Statement statement = (Statement)param["CURR"];
                if (tree.Count > 0)
                {
                    Statement root = tree.Peek();
                    root.AddStatement(statement);
                }
                tree.Push(statement);

                return base.Advance(chain, ref param);
            }
        }

        private class StateRise : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<Statement> stack = (Stack<Statement>)param["TR"];
                param["CURR"] = stack.Pop();
                return base.Advance(chain, ref param);
            }
        }

        private class StateInsert : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<Statement> tree = (Stack<Statement>)param["TR"];


                Statement statement = (Statement)param["CURR"];

                    //throw new Exception("������ ���������� ����");
                if (tree.Count == 0) tree.Push(statement);
                else
                {
                    Statement root = tree.Peek();
                    root.AddStatement(statement);
                }

                return base.Advance(chain, ref param);
            }
        }

        private class StateExpress : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                MathParser math = (MathParser)param["MA"];

                Dictionary<string, Object> mparam = new Dictionary<string, Object>();
                LinkedList<Expression> expression = new LinkedList<Expression>();
                Stack<Operator> stack = new Stack<Operator>();
                Stack<AState> states = new Stack<AState>();

                mparam.Add("TH", math);
                mparam.Add("PR", param["TH"]);
                mparam.Add("ER", param["ER"]);
                mparam.Add("LN", param["LN"]);
                mparam.Add("EXP", expression);
                mparam.Add("ST", stack);
                mparam.Add("RET", states);

                param["CURR"] = math.Run(chain, ref mparam);

                return base.Advance(chain, ref param);
            }
        }

        private class StateRec : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<Statement> stack = (Stack<Statement>)param["TR"];

                Parser auto = (Parser)param["TH"];

                param["CURR"] = auto.Run(chain);
                return base.Advance(chain, ref param);
            }
        }


        private class StatePush : StateMove
        {
            private string name;
            public StatePush(string name = "DEF")
            {
                this.name = name;
            }

            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Stack<Statement>> stacks = 
                    (Dictionary<string, Stack<Statement>>)param["ST"];

                Stack<Statement> stack;
                if (stacks.ContainsKey(name)) stack = (Stack<Statement>)stacks[name];
                else
                {
                    stack = new Stack<Statement>();
                    stacks[name] = stack;
                }

                stack.Push((Statement)param["CURR"]);
                return base.Advance(chain, ref param);
            }
        }

        private class StatePop : StateMove
        {
            private string name;
            public StatePop(string name = "DEF")
            {
                this.name = name;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Stack<Statement>> stacks =
                    (Dictionary<string, Stack<Statement>>)param["ST"];

                Stack<Statement> stack;
                if (stacks.ContainsKey(name)) stack = (Stack<Statement>)stacks[name];
                else throw new Exception(
                    "���� � ������ " + name + " �� �����");

                param["CURR"] = stack.Pop();
                return base.Advance(chain, ref param);
            }
        }

        private class StateEnque : StateMove
        {
            private string name;
            public StateEnque(string name = "DEF")
            {
                this.name = name;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Queue<Statement>> queues =
                    (Dictionary<string, Queue<Statement>>)param["QU"];

                Queue<Statement> queue;
                if (queues.ContainsKey(name)) queue = (Queue<Statement>)queues[name];
                else
                {
                    queue = new Queue<Statement>();
                    queues[name] = queue;
                }

                queue.Enqueue((Statement)param["CURR"]);
                return base.Advance(chain, ref param);
            }
        }

        private class StateDeque : StateMove
        {
            private string name;
            public StateDeque(string name = "DEF")
            {
                this.name = name;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Queue<Statement>> queues =
                    (Dictionary<string, Queue<Statement>>)param["QU"];

                Queue<Statement> queue;
                if (queues.ContainsKey(name)) queue = (Queue<Statement>)queues[name];
                else throw new Exception("������� � ������ " + name + " �� ������");

                param["CURR"] = queue.Dequeue();
                return base.Advance(chain, ref param);
            }
        }


        private class StateCreateCounter : StateMove
        {
            private string name;
            private int val;
            public StateCreateCounter(string name, int val = 0)
            {
                this.name = name;
                this.val = val;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, int> counters =
                    (Dictionary<string, int>)param["CN"];

                if (counters.ContainsKey(name))
                    throw new Exception("������� � ������ " + name + " ��� ������ �����");

                counters.Add(name, val);

                return base.Advance(chain, ref param);
            }
        }

        private class StateCount : StateMove
        {
            private string name;
            public StateCount(string name)
            {
                this.name = name;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, int> counters = 
                    (Dictionary<string, int>)param["CN"];

                if (!counters.ContainsKey(name)) 
                    throw new Exception("������� � ������ " + name + " �� ������");
                    // counters.Add(name, 0);

                counters[name] = counters[name] + 1;

                return base.Advance(chain, ref param);
            }
        }

        private class StateRepeat : StateMove
        {
            private string name;
            public StateRepeat(string name)
            {
                this.name = name;
            }
            public override void AddJump(Jump j)
            {
                if (JumpsCount == 2) 
                    throw new Exception("������ �������� ����� 2 ��������� ���������");
                base.AddJump(j);
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, int> counters =
                    (Dictionary<string, int>)param["CN"];

                if (!counters.ContainsKey(name)) 
                    throw new Exception("������� � ������ " + name + " �� ������");

                int count = counters[name];
                if (count > 0)
                {
                    counters[name] = count - 1;
                    return Jumps[0].State;
                }
                return Jumps[1].State;
            }
        }


        private class StateSave : StateMove
        {
            private string name;
            public StateSave(string name)
            {
                this.name = name;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Statement> saves = 
                    (Dictionary<string, Statement>)param["SV"];

                saves[name] = (Statement)param["CURR"];
                return base.Advance(chain, ref param);
            }
        }

        private class StateResume : StateMove
        {
            private string name;
            public StateResume(string name)
            {
                this.name = name;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Statement> saves =
                    (Dictionary<string, Statement>)param["SV"];

                if (!saves.ContainsKey(name))
                    throw new Exception("��������� � ������ " + name + " �� �������");

                param["CURR"] = saves[name];
                return base.Advance(chain, ref param);
            }
        }

        private class StatePoint : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<int> stack = (Stack<int>)param["PN"];
                stack.Push(chain.Cursor);
                return base.Advance(chain, ref param);
            }
        }

        private class StateBackoff : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<int> stack = (Stack<int>)param["PN"];
                if (stack.Count == 0)
                    throw new Exception("���� �������� ����");

                chain.Cursor = stack.Pop();
                return base.Advance(chain, ref param);
            }
        }


        private class StateSetFlag : StateMove
        {
            private string[] names;
            public StateSetFlag(string name)
                : this(new string[] { name })
            {
            }
            public StateSetFlag(string[] names)
            {
                this.names = names;
            }

            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                HashSet<string> gflags = (HashSet<string>)param["GF"];
                HashSet<string> lflags = (HashSet<string>)param["LF"];
                foreach (string name in names)
                {
                    if (!gflags.Contains(name))
                    {
                        gflags.Add(name);
                        lflags.Add(name);
                    }
                }
                return base.Advance(chain, ref param);
            }
        }

        private class StateUnsetFlag : StateMove
        {
            private string[] names;
            public StateUnsetFlag(string name)
                : this(new string[] { name })
            {
            }
            public StateUnsetFlag(string[] names)
            {
                this.names = names;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                HashSet<string> gflags = (HashSet<string>)param["GF"];
                HashSet<string> lflags = (HashSet<string>)param["LF"];
                foreach (string name in names)
                {
                    if (lflags.Contains(name))
                    {
                        gflags.Remove(name);
                        lflags.Remove(name);
                    }
                }
                return base.Advance(chain, ref param);
            }
        }

        private class StateDumpFlag : StateMove
        {
            private string[] names;
            public StateDumpFlag(string name)
                : this(new string[] { name })
            {
            }
            public StateDumpFlag(string[] names)
            {
                this.names = names;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                HashSet<string> gflags = (HashSet<string>)param["GF"];
                HashSet<string> lflags = (HashSet<string>)param["LF"];
                foreach (string name in names)
                {
                    if (gflags.Contains(name)) gflags.Remove(name);
                    if (lflags.Contains(name)) lflags.Remove(name);
                }
                return base.Advance(chain, ref param);
            }
        }

        private class StateFlagAll : StateMove
        {
            private string[] names;
            public StateFlagAll(string[] names)
            {
                this.names = names;
            }
            public override void AddJump(Jump j)
            {
                if (JumpsCount == 2)
                    throw new Exception("������ �������� ����� 2 ��������� ���������");
                base.AddJump(j);
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                if (JumpsCount < 2)
                    throw new Exception("������ ���� 2 ��������� ���������");

                HashSet<string> gflags = (HashSet<string>)param["GF"];

                foreach (string name in names)
                {
                    if (!gflags.Contains(name)) return Jumps[1].State;
                }
                return Jumps[0].State;
            }
        }

        private class StateFlagOne : StateMove
        {
            private string[] names;
            public StateFlagOne(string[] names)
            {
                this.names = names;
            }
            public override void AddJump(Jump j)
            {
                if (JumpsCount == 2)
                    throw new Exception("������ �������� ����� 2 ��������� ���������");
                base.AddJump(j);
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                if (JumpsCount < 2)
                    throw new Exception("������ ���� 2 ��������� ���������");

                HashSet<string> gflags = (HashSet<string>)param["GF"];

                foreach (string name in names)
                {
                    if (gflags.Contains(name)) return Jumps[0].State;
                }
                return Jumps[1].State;
            }
        }

        private class StateSubprogram : StateMove
        {
            private HashSet<string> pflags;

            public StateSubprogram(string[] pflags)
            {
                if (pflags != null) this.pflags = new HashSet<string>(pflags);
                else this.pflags = new HashSet<string>();
            }

            public StateSubprogram()
                : this(new string[] { "GF" })
            {
            }

            public override void AddJump(Jump j)
            {
                if (JumpsCount == 2)
                    throw new Exception("������ �������� ����� 2 ��������� ���������");
                base.AddJump(j);
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                if (JumpsCount < 2)
                    throw new Exception("������ ���� 2 ��������� ���������");

                Dictionary<string, Object> subparam = new Dictionary<string, Object>();
                
                Queue<Attribute> attr = new Queue<Attribute>();
                HashSet<string> lflags = new HashSet<string>();

                subparam.Add("PRM", param);
                subparam.Add("TH", param["TH"]);
                subparam.Add("LN", param["LN"]);
                subparam.Add("MA", param["MA"]);
                subparam.Add("ER", param["ER"]);

                subparam.Add("AT", attr);
                subparam.Add("LF", lflags);

                if (!pflags.Contains("TR"))
                {
                    Stack<Statement> tree = new Stack<Statement>();
                    subparam.Add("TR", tree);
                } else subparam.Add("TR", param["TR"]);

                if (!pflags.Contains("PN"))
                {
                    Stack<int> points = new Stack<int>();
                    subparam.Add("PN", points);
                } else subparam.Add("PN", param["PN"]);

                if (!pflags.Contains("ST"))
                {
                    Dictionary<string, Stack<Statement>> stacks = 
                        new Dictionary<string, Stack<Statement>>();
                    subparam.Add("ST", stacks);
                } else subparam.Add("ST", param["ST"]);

                if (!pflags.Contains("QU"))
                {
                    Dictionary<string, Queue<Statement>> queues = 
                        new Dictionary<string, Queue<Statement>>();
                    subparam.Add("QU", queues);
                } else subparam.Add("QU", param["QU"]);

                if (!pflags.Contains("GF"))
                {
                    HashSet<string> gflags = new HashSet<string>();
                    subparam.Add("GF", gflags);
                } else subparam.Add("GF", param["GF"]);

                if (!pflags.Contains("SV"))
                {
                    Dictionary<string, Statement> saves = new Dictionary<string, Statement>();
                    subparam.Add("SV", saves);
                } else subparam.Add("SV", param["SV"]);

                if (!pflags.Contains("CN"))
                {
                    Dictionary<string, int> counters = new Dictionary<string, int>();
                    subparam.Add("CN", counters);
                } else subparam.Add("CN", param["CN"]);

                param["RET"] = Jumps[1].State;
                param = subparam;
                return Jumps[0].State;
            }
        }

        private class StateReturn : StateMove
        {
            //public StateReturn()
            //{
            //    jumps.Add(new Jump(null, new ConditTrue(), 0));
            //}
            public override void AddJump(Jump j)
            {
                throw new Exception("������� ��������� ��������� � StateReturn");
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                if (!param.ContainsKey("PRM")) throw new Exception("���� �������� ����");

                Stack<Statement> subtree = (Stack<Statement>)param["TR"];
                param = (Dictionary<string, Object>)param["PRM"];

                while (subtree.Count > 1)
                    subtree.Pop();

                param["CURR"] = subtree.Peek();
                AState ret = (AState)param["RET"];
                return ret.Advance(chain, ref param);
            }
        }

        private class StateFinal : AState
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("�������� �� ��������� ���������");
            }

            public override object GetObject(IChain chain, ref Dictionary<string, Object> param)
            {
                Stack<Statement> tree = (Stack<Statement>)param["TR"];
                while (tree.Count > 1) tree.Pop();
                return tree.Peek();
            }

            public override bool Motion()
            {
                return false;
            }
        }

        private class StateError : StateMove
        {
            private int id;
            private int sh;
            public StateError(int id, int sh)
            {
                this.id = id;
                this.sh = sh;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Error err = new Error(id, chain.Deviation(sh).Clone());

                List<Error> errors = (List<Error>)param["ER"];
                Stack<Statement> tree = (Stack<Statement>)param["TR"];

                errors.Add(err);

                param["CURR"] = err;

                if (tree.Count > 0) tree.Peek().AddStatement(err);
                else tree.Push(err);

                return new StateFinal();//base.Advance(chain, ref param);
            }
        }


        private Lang Lang;
        private List<Error> errors;

        public Parser(Lang lang, AState first)
            : base(first)
        {
            this.Lang = lang;
        }

        public Parser(Lang lang, string path)
            : this(lang, build(path))
        {
        }

        public Object Run(IChain chain)
        {
            Dictionary<string, Object> param = new Dictionary<string, Object>();
            
            Stack<Statement> tree = new Stack<Statement>();
            Queue<Attribute> attr = new Queue<Attribute>();
            Stack<int> points = new Stack<int>();

            HashSet<string> gflags = new HashSet<string>();
            HashSet<string> lflags = new HashSet<string>();

            Dictionary<string, Statement> saves = new Dictionary<string, Statement>();
            Dictionary<string, int> counters = new Dictionary<string, int>();

            Dictionary<string, Stack<Statement>> stacks = new Dictionary<string, Stack<Statement>>();
            Dictionary<string, Queue<Statement>> queues = new Dictionary<string, Queue<Statement>>();

            MathParser m = new MathParser(Lang, "xml/math.xml");

            errors = new List<Error>();

            param.Add("TH", this);
            param.Add("TR", tree);
            param.Add("AT", attr);
            param.Add("PN", points);
            param.Add("ST", stacks);
            param.Add("QU", queues);

            param.Add("GF", gflags);
            param.Add("LF", lflags);
            param.Add("SV", saves);
            param.Add("CN", counters);

            param.Add("LN", this.Lang);
            param.Add("MA", m);
            param.Add("ER", errors);

            return Run(chain, ref param);
        }

        private static AState build(string path)
        {
            Dictionary<int, AState> states = new Dictionary<int, AState>();

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(path);

            foreach (XmlNode node in xmldoc["generation"]["states"].ChildNodes)
            {
                string[] param = node.Attributes["params"].Value.Split(
                    new char[]{ ';' }, StringSplitOptions.RemoveEmptyEntries );

                for (int i = 0; i < param.Length; ++i)
                {
                    param[i] = param[i].Trim();
                }

                switch (node.Attributes["type"].Value)
                {
                    case "oprt":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateOperation(Convert.ToInt32(param[0])));
                        break;
                    case "oprn":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateOperand(Convert.ToInt32(param[0])));
                        break;
                    case "move":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateMove());
                        break;
                    case "attr":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateAttribute(Convert.ToInt32(param[0]), Convert.ToInt32(param[1])));
                        break;
                    case "deep":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateDeep());
                        break;
                    case "rise":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateRise());
                        break;
                    case "stmt":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateStatement(Convert.ToInt32(param[0]), Convert.ToInt32(param[1])));
                        break;
                    case "expr":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateExpress());
                        break;
                    case "insr":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateInsert());
                        break;
                    case "inattr":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateInsertAttributes());
                        break;
                    case "push":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StatePush(param[0]));
                        break;
                    case "pop":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StatePop(param[0]));
                        break;
                    case "enq":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateEnque(param[0]));
                        break;
                    case "deq":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateDeque(param[0]));
                        break;

                    case "crcn":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateCreateCounter(param[0],
                                Convert.ToInt32(param[1])));
                        break;
                    case "cntr":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateCount(param[0]));
                        break;
                    case "rpt":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateRepeat(param[0]));
                        break;

                    case "save":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateSave(param[0]));
                        break;
                    case "resm":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateResume(param[0]));
                        break;
                    case "point":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StatePoint());
                        break;
                    case "back":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateBackoff());
                        break;

                    case "setf":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateSetFlag(param[0].Split(',')));
                        break;
                    case "unsf":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateUnsetFlag(param[0].Split(',')));
                        break;
                    case "dumf":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateDumpFlag(param[0].Split(',')));
                        break;
                    case "onef":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateFlagOne(param[0].Split(',')));
                        break;
                    case "allf":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateFlagAll(param[0].Split(',')));
                        break;

                    case "fin":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateFinal());
                        break;
                    case "subp":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateSubprogram(param[0].Split(',')));
                        break;
                    case "ret":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateReturn());
                        break;
                    case "err":
                        states.Add(Convert.ToInt32(node.Attributes["id"].Value),
                            new StateError(Convert.ToInt32(param[0]), Convert.ToInt32(param[1])));
                        break;
                    default:
                        throw new Exception("����������� ���������");
                }
            }
            foreach (XmlNode node in xmldoc["generation"]["jumps"].ChildNodes)
            {
                AState s1, s2;
                if (!states.TryGetValue(
                    Convert.ToInt32(node.Attributes["s"].Value), out s1))
                    throw new Exception("��������� id = " + node.Attributes["s"].Value + " �� �������");
                if (!states.TryGetValue(
                    Convert.ToInt32(node.Attributes["f"].Value), out s2))
                    throw new Exception("��������� id = " + node.Attributes["f"].Value + " �� �������");
                IInstruction inst = new Token(Convert.ToInt32(node.Attributes["i"].Value));
                ICondition cond;
                if (node.Attributes["c"].Value == "=")
                    cond = new ConditEquals();
                else if (node.Attributes["c"].Value == "+")
                    cond = new ConditTrue();
                else cond = new ConditFalse();
                Jump jump = new Jump(inst, cond,
                    Convert.ToInt32(node.Attributes["sh"].Value));
                Jump.Join(s1, s2, jump);
            }
            AState first;
            states.TryGetValue(0, out first);
            return first;
        }

        public Error[] Errors()
        {
            return errors.ToArray();
        }
	}
}
