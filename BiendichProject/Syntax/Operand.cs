﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Lexical;
using Biendich.Automation;

namespace Biendich.Syntax
{
    public class Operand : Expression
    {
        public Operand(int id, IInstruction node)
            : base("operand", id, node)
        {
        }

        public override XmlNode ToXml(XmlDocument doc)
        {
            XmlNode node = base.ToXml(doc);
            XmlAttribute attr = doc.CreateAttribute("value");
            attr.Value = Value.ToString();
            node.Attributes.Append(attr);
            return node;
        }
    }
}
