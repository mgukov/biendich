using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Utility;
using Biendich.Language;

namespace Biendich.Syntax
{
    public class MathParser : Automaton
    {
		private class StateMove : AState
		{
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                //if (chain.EOF()) throw new Exception("����������� ���������� id = ");
                foreach (Jump j in Jumps)
                {
                    if (j.Accept(chain.Current, ref param))
                    {
                        param["pres"] = this;
                        chain.Move(j.Shift);
                        return j.State;
                    }
                }
                StateError err = new StateError(1000);
                StateFinish finish = new StateFinish();
                Jump.Join(err, finish, new Jump(new Token(0), new ConditTrue(), 0));

                return err;
            }
            public override Object GetObject(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("���������� �������� ������ StateMove.GetObject(...)");
            }
            public override bool Motion()
            {
                return true;
            }
						
		}

		private class StateOperand : StateMove
        {
            int id;

            public StateOperand(int id)
            {
                this.id = id;
            }

            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
			{
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];

                Token tok = (Token)chain.Deviation(-1).Clone();

                Operand var = new Operand(id, tok);
                expression.AddLast(var);

                return base.Advance(chain, ref param);
			}
        }

		private class StateOperation : StateMove
        {
            private int id;

            public StateOperation(int id)
            {
                this.id = id;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
			{
                Lang lang = (Lang)param["LN"];
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];

                Token tok = (Token)chain.Deviation(-1).Clone();

                Operator op = new Operator(id, tok,
                    Convert.ToInt32(lang.InstructionAttribute("operators", id, "priority")),
                    Convert.ToChar(lang.InstructionAttribute("operators", id, "assoc")),
                    Convert.ToInt32(lang.InstructionAttribute("operators", id, "operands")));

                if (stack.Count > 0)
                {
                    Operator top = stack.Peek();
                    int prl = op.Priority, prr = top.Priority;

                    while (prl <= prr)
                    {
                        if (op.Associativity == 'r' && prl == prr) 
                            break;
                        expression.AddLast(stack.Pop());
                        if (stack.Count == 0) break;
                        top = stack.Peek();
                        prr = top.Priority;
                    }
                }
                stack.Push(op);
                return base.Advance(chain, ref param);
			}
       }

        private class StateAddParametr : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Lang lang = (Lang)param["LN"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];

                Operator func = stack.Peek();
                func.AddParametr();

                return base.Advance(chain, ref param);
            }
        }

        private class StateStackPop : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];
                expression.AddLast(stack.Pop());
                return base.Advance(chain, ref param);
            }
        }

        private class StateFillOperator : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];
                Expression exp = stack.Pop();
                expression.AddLast(exp);
                exp.Fill(expression.Last);
                return base.Advance(chain, ref param);
            }
        }

        private class StateFillAll : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];

                while (stack.Count > 0)
                {
                    expression.AddLast(stack.Pop());
                }

                Queue<int> q = new Queue<int>();

                LinkedListNode<Expression> e = expression.First;
                while (expression.Count > 1)
                {
                    if (!e.Value.Filled) e.Value.Fill(e);
                    e = e.Next;
                }
                return base.Advance(chain, ref param);
            }
        }

		private class StateCreate : AState
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
			{
				throw new Exception("�������� �� ��������� ���������");
			}
            public override Object GetObject(IChain chain, ref Dictionary<string, Object> param)
			{
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];

				while (stack.Count > 0)
				{
					expression.AddLast(stack.Pop());
				}

                Queue<int> q = new Queue<int>();

				LinkedListNode<Expression> e = expression.First;
                while (expression.Count > 1)
				{
                    if (!e.Value.Filled) e.Value.Fill(e);
					e = e.Next;
				}
				return expression.First.Value;
			}
			
			public override bool Motion()
			{
				return false;
			}
        }
        
        private class StateSubexpression : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                MathParser mparser = (MathParser)param["TH"];

                Dictionary<string, Object> mparam = new Dictionary<string, Object>();
                LinkedList<Expression> expression = new LinkedList<Expression>();
                Stack<Operator> stack = new Stack<Operator>();
                Stack<AState> states = new Stack<AState>();

                mparam.Add("TH", mparser);
                mparam.Add("PR", param["PR"]);
                mparam.Add("ER", param["ER"]);
                mparam.Add("LN", param["LN"]);
                mparam.Add("EXP", expression);
                mparam.Add("ST", stack);
                mparam.Add("RET", states);

                expression = (LinkedList<Expression>)param["EXP"];

                Expression exp = (Expression)mparser.Run(chain, ref mparam);
                expression.AddLast(exp);

                return base.Advance(chain, ref param);
            }
        }

        private class StatePoint : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];

                param["cur"] = chain.Cursor;
                param["expc"] = expression.Count;
                param["stac"] = stack.Count;
                return base.Advance(chain, ref param);
            }
        }

        private class StateBackoff : StateMove
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];
                Stack<Operator> stack = (Stack<Operator>)param["ST"];

                int expc = (int)param["expc"];
                int tsac = (int)param["stac"];
                int sh = -(chain.Cursor - (int)param["cur"]);

                chain.Move(sh);

                while (expression.Count > expc)
                    expression.RemoveLast();

                while (stack.Count > tsac)
                    stack.Pop();

                return base.Advance(chain, ref param);
            }
        }

        private class StateError : StateMove
        {
            private int id;
            public StateError(int id)
            {
                this.id = id;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                List<Error> errors = (List<Error>)param["ER"];
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];

                Error err = new Error(id, chain.Current.Clone());
                errors.Add(err);
                expression.AddLast(err);
                return base.Advance(chain, ref param);
            }

        }

        private class StateSubprogram : StateMove
        {
            public override void AddJump(Jump j)
            {
                if (JumpsCount > 2) throw new Exception(
                    "������� ��������� ����� ������ ��������� � StateSubprogram");
                jumps.Add(j);
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                Dictionary<string, Object> subparam = new Dictionary<string, Object>();
                LinkedList<Expression> expression = new LinkedList<Expression>();
                Stack<Operator> stack = new Stack<Operator>();

                subparam.Add("TH", param["TH"]);
                subparam.Add("LN", param["LN"]);
                subparam.Add("ER", param["ER"]);
                subparam.Add("PR", param["PR"]);
                subparam.Add("EXP", expression);
                subparam.Add("ST", stack);
                subparam.Add("PRM", param);

                AState s = (AState)param["pres"];
                param["RET"] = s.Jumps[s.JumpsCount - 1].State;
                param = subparam;
                return Jumps[0].State;
            }
        }

        private class StateReturn : StateFillAll
        {
            public StateReturn()
            {
                jumps.Add(new Jump(null, new ConditTrue(), 0));
            }
            public override void AddJump(Jump j)
            {
                throw new Exception("������� ��������� ��������� � StateReturn");
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                if (!param.ContainsKey("PRM")) throw new Exception("���� �������� ����");

                base.Advance(chain, ref param);

                LinkedList<Expression> subexpression = (LinkedList<Expression>)param["EXP"];
                param = (Dictionary<string, Object>)param["PRM"];
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];

                expression.AddLast(subexpression.First.Value);
                AState ret = (AState)param["RET"];
                return ret.Advance(chain, ref param);
            }
        }

        private class StateFinish : AState
        {
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("�������� �� ��������� ���������");
            }
            public override Object GetObject(IChain chain, ref Dictionary<string, Object> param)
            {
                LinkedList<Expression> expression = (LinkedList<Expression>)param["EXP"];

                return expression.Last.Value;
            }

            public override bool Motion()
            {
                return false;
            }
        }


        private Lang lang;
        private List<Error> errors;

        public MathParser(Lang lang, AState first)
            : base(first)
        {
            this.lang = lang;
        }

        public MathParser(Lang lang, string path)
            : this(lang, build(path))
        {
        }

        public Object Run(IChain chain)
        {
            Dictionary<string, Object> param = new Dictionary<string, Object>();
            LinkedList<Expression> expression = new LinkedList<Expression>();
            Stack<Operator> stack = new Stack<Operator>();
            Stack<AState> states = new Stack<AState>();

            errors = new List<Error>();

            param.Add("TH", this);
            param.Add("ER", errors);
            param.Add("LN", lang);
			param.Add("EXP", expression);
			param.Add("ST", stack);
            param.Add("RET", states);

            return Run(chain, ref param);
        }
        private static AState build(string path)
        {
            Dictionary<int, AState> states = new Dictionary<int, AState>();

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(path);

            foreach (XmlNode node in xmldoc["generation"]["states"].ChildNodes)
            {
                string[] param = node.Attributes["params"].Value.Split(
                    new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < param.Length; ++i)
                {
                    param[i] = param[i].Trim();
                }

                switch (node.Attributes["type"].Value)
                {
                    case "OPRT":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateOperation(int.Parse(param[0])));
                        break;
                    case "OPRN":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateOperand(int.Parse(param[0])));
                        break;
                    case "MOVE":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateMove());
                        break;
                    case "PRM":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateAddParametr());
                        break;
                    case "FILL":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateFillOperator());
                        break;
                    case "POP":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateStackPop());
                        break;
                    case "CRT":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateCreate());
                        break;
                    case "PNT":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StatePoint());
                        break;
                    case "BACK":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateBackoff());
                        break;
                    case "SUBE":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateSubexpression());
                        break;
                    case "SUBP":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateSubprogram());
                        break;
                    case "RET":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateReturn());
                        break;
                    case "FIN":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateFinish());
                        break;
                    case "ERR":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateError(int.Parse(param[0])));
                        break;
                }
            }
            foreach (XmlNode node in xmldoc["generation"]["jumps"].ChildNodes)
            {
                AState s1, s2;
                if (!states.TryGetValue(
                    Convert.ToInt32(node.Attributes["s"].Value), out s1))
                    throw new Exception("��������� id = " + node.Attributes["s"].Value + " �� �������");
                if (!states.TryGetValue(
                    Convert.ToInt32(node.Attributes["f"].Value), out s2))
                    throw new Exception("��������� id = " + node.Attributes["f"].Value + " �� �������");
                IInstruction inst = new Token(Convert.ToInt32(node.Attributes["i"].Value));
                ICondition cond;
                if (node.Attributes["c"].Value == "=")
                    cond = new ConditEquals();
                else if (node.Attributes["c"].Value == "+")
                    cond = new ConditTrue();
                else cond = new ConditFalse();
                Jump jump = new Jump(inst, cond,
                    Convert.ToInt32(node.Attributes["sh"].Value));
                Jump.Join(s1, s2, jump);
            }
            AState first;
            states.TryGetValue(0, out first);
            return first;
        }

        public Error[] Errors()
        {
            return errors.ToArray();
        }

    }
}
