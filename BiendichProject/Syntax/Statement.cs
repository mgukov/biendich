using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Lexical;
using Biendich.Automation;

namespace Biendich.Syntax
{


    public class Statement : IInstruction
	{
        private string name;

        int id;
        //string value;

		private IInstruction node;
		private List<Statement> statements;

        protected Statement(string name, int id, IInstruction node)
        {
            this.node = node;
            this.name = name;
            this.id = id;
            //this.value = value;
            statements = new List<Statement>();
        }

        public Statement(int id, IInstruction node)
            :this("statement", id, node)
        {
        }

        public Object Value
        {
            get { return node.Value; }
        }

        public int Id
        {
            get { return id; }
        }

        public int Position
        {
            get { return node.Position; }
        }


        public string Name
        {
            get { return name; }
        }

        public IInstruction Instruction
        {
            get { return node; }
        }

        public Statement[] Statements
        {
            get { return statements.ToArray(); }
        }


		public void AddStatement(Statement statement)
		{
            statements.Add(statement);
		}

        public IInstruction Clone()
        {
            throw new NotImplementedException();
        }

        public bool Equals(IInstruction i)
        {
            return (i.Id == this.Id);
        }

        public virtual XmlNode ToXml(XmlDocument doc)
        {
            XmlNode node = doc.CreateElement(name);

            foreach (Statement s in statements)
            {
                node.AppendChild(s.ToXml(doc));
            }

            XmlAttribute attribute;

            attribute = doc.CreateAttribute("id");
            attribute.Value = Id.ToString();
            node.Attributes.Append(attribute);

            return node;
        }
        public XmlDocument ToXml()
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(ToXml(doc));
            return doc;
        }
    }
}
