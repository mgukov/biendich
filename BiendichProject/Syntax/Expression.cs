using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Lexical;
using Biendich.Automation;

namespace Biendich.Syntax {
    public abstract class Expression : Statement {
        private bool filled;

        public Expression(string name, int id, IInstruction node)
            : base(name, id, node) {
        }

        public bool Filled {
            get { return filled; }
        }

        public virtual void Fill(LinkedListNode<Expression> exp) {
            if (filled) throw new Exception("��������� ��� ���������");
            filled = true;
        }
    }
}
