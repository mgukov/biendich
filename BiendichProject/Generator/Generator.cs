﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Biendich.Utility;
using Biendich.Language;
using Biendich.Automation;
using Biendich.Lexical;

namespace Biendich.Generation
{
    public abstract class AGenerator
    {
        public abstract GState Generate(Lang lang);

        public virtual string GenerateXml(Lang lang)
        {
            GState first = Generate(lang);
            HashSet<GState> states = new HashSet<GState>();
            HashSet<GJump> jumps = new HashSet<GJump>();
            Queue<GState> q = new Queue<GState>();
            GState curr;

            states.Add(first);
            q.Enqueue(first);
            while (q.Count > 0)
            {
                curr = q.Dequeue();
                foreach (Jump j in curr.Jumps)
                {
                    jumps.Add((GJump)j);
                    if (states.Add((GState)j.State))
                        q.Enqueue((GState)j.State);
                }
            }
            StringBuilder str = new StringBuilder();
            str.AppendLine("<generation>");
            str.AppendLine("\t<states>");
            foreach (GState s in states)
            {
                str.AppendLine("\t\t" + s.ToXml());
            }
            str.AppendLine("\t</states>");
            str.AppendLine("\t<jumps>");
            foreach (GJump j in jumps)
            {
                str.AppendLine("\t\t" + j.ToXml());
            }
            str.AppendLine("\t</jumps>");
            str.AppendLine("</generation>");
            return str.ToString();
        }
    }
}
