﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Biendich.Utility;
using Biendich.Language;
using Biendich.Automation;

namespace Biendich.Generation
{
    public class GJump : Jump
    {
        private AState start;
        private int id;

        private static int counter = 0;
        private static int getid()
        {
            return counter++;
        }

        public new static Jump[] Create(IInstruction[] instruct, ICondition cond, int sh)
        {
            Jump[] jj = new Jump[instruct.Length];
            for (int i = 0; i < instruct.Length; ++i)
            {
                jj[i] = new GJump(instruct[i], cond, sh);
            }
            return jj;
        }

        public GJump(IInstruction instruct, int sh)
            : base(instruct, new ConditEquals(), sh)
        {
            id = getid();
        }

        public GJump(IInstruction instruct, ICondition condit, int sh)
            : base(instruct, condit, sh)
        {
            id = getid();
        }

        private GJump(IInstruction instruct, ICondition condit, int sh, AState start, AState final)
            : base(instruct, condit, sh, final)
        {
            id = getid();
            this.start = start;
        }

        public int Id
        {
            get { return id; }
        }
        protected override void join(AState start, AState final)
        {
            Jump j = new GJump(Instruction, Condition, Shift, start, final);
            start.AddJump(j);
        }
        public virtual string ToXml()
        {
            return "<jump id='" + id.ToString() +
                "' s='" + ((GState)this.start).Id.ToString() +
                "' f='" + ((GState)this.State).Id.ToString() +
                "' sh='" + Shift.ToString() + "' c='" + Condition.ToString() +
                "' i='" + Instruction.Id.ToString() + "'/>";
        }
    }
}