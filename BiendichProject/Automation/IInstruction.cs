using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation
{
	public interface IInstruction
	{
        int Id { get; }
        int Position { get; }
        Object Value { get; }
		bool Equals(IInstruction i);
        IInstruction Clone();
	}
}
