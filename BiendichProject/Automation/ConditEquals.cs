using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation {
    public class ConditEquals : ICondition {
        public bool Check(IInstruction i1, IInstruction i2) {
            return i1.Equals(i2);
        }
        public override string ToString() {
            return "=";
        }
    }
}
