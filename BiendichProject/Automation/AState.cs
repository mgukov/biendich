using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation {
    public abstract class AState {
        protected List<Jump> jumps;
        public AState() {
            jumps = new List<Jump>();
        }
        public int JumpsCount {
            get { return jumps.Count; }
        }
        public Jump[] Jumps {
            get { return jumps.ToArray(); }
        }
        public virtual void AddJump(Jump j) {
            jumps.Add(j);
        }
        public abstract AState Advance(IChain chain, ref Dictionary<string, Object> param);
        public abstract Object GetObject(IChain chain, ref Dictionary<string, Object> param);
        public abstract bool Motion();
    }
}
