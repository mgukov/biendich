using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation
{
	public class ConditTrue : ICondition
	{
		public bool Check(IInstruction i1, IInstruction i2)
		{
            return true;
		}
        public override string ToString()
        {
            return "+";
        }
    }
}
