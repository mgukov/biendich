using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation
{
	public interface ICondition
	{
		bool Check(IInstruction i1, IInstruction i2);
	}
}
