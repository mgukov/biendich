using System;
using System.Collections.Generic;
using System.Text;


/* 
 * class BienDich.Automation.Jump
 *
 * ��������:
 *      ���������� ������ ������� �� ������ ��������� ��������� �������� � ������.
 *      
 * ��������:
 *      private ICondition condit       - �������(���������) ��������;
 *      private IInstruction instruct   - ���������� ��� ��������;
 *      private int shift               - ����� �������� ��� ������ ����������(�����);
 *      private AState start            - ��������� ���������;
 *      private AState final            - �������� ���������;
 *      
 * ������:
 *      public int Shift();
 *      public AState State();
 *      public ICondition Condition();
 *      public IInstruction Instruction();
 *      public virtual bool Accept(IInstruction instruct);
 *      internal void SetStart(AState s);
 *      internal void SetFinal(AState s);
 *      protected virtual Jamp clone();
 *      
 */

namespace Biendich.Automation
{
	public class Jump
	{
		private ICondition condit;
		private IInstruction instruct;
        private AState state;
		private int shift;

        public Jump(IInstruction instruct, ICondition cond, int sh)
        {
            this.condit = cond;
            this.instruct = instruct;
            this.shift = sh;
        }

        public Jump(IInstruction instruct, int sh)
            : this(instruct, new ConditEquals(), sh)
        {
        }

        protected Jump(IInstruction instruct, ICondition cond, int sh, AState state)
            : this(instruct, cond, sh)
        {
            this.state = state;
        }

        public int Shift
		{
            get { return shift; }
		}

        public AState State
		{
            get { return state; }
		}

        public ICondition Condition
        {
            get { return condit; }
        }

        public IInstruction Instruction
        {
            get { return instruct; }
        }

        public virtual bool Accept(IInstruction instruct, ref Dictionary<string, Object> param)
		{
            return condit.Check(this.instruct, instruct);
		}

        protected virtual void join(AState start, AState final)
        {
            Jump j = new Jump(instruct, condit, shift, final);
            start.AddJump(j);
        }


        public static void Join(AState start, AState final, Jump jump)
        {
            jump.join(start, final);
        }

        public static void Join(AState start, AState final, Jump[] jumps)
        {
            foreach (Jump j in jumps)
            {
                Jump.Join(start, final, j);
            }
        }

        public static void Join(AState start, AState[] final, Jump[] jumps)
        {
            if (final.Length != jumps.Length) throw new Exception("������ �����������");
            for (int i = 0; i < jumps.Length; ++i)
            {
                Jump.Join(start, final[i], jumps[i]);
            }
        }

        public static void Join(AState[] start, AState final, Jump jumps)
        {
            foreach (AState s in start)
            {
                Jump.Join(s, final, jumps);
            }
        }

        public static void JoinFParallelJ(AState[] start, AState[] final, Jump[] jumps)
        {
            foreach (AState s in start)
            {
                Jump.Join(s, final, jumps);
            }
        }

        public static void JoinAll(AState[] start, AState final, Jump[] jumps)
        {
            foreach (AState s in start)
            {
                foreach (Jump j in jumps)
                {
                     Jump.Join(s, final, j);
                }
            }
        }

        public static Jump[] Create(IInstruction[] instruct, ICondition cond, int sh)
        {
            Jump[] jj = new Jump[instruct.Length];
            for (int i = 0; i < instruct.Length; ++i)
            {
                jj[i] = new Jump(instruct[i], cond, sh);
            }
            return jj;
        }
	}
}
