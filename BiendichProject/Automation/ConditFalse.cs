﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation
{
    public class ConditFalse : ICondition
    {
        public bool Check(IInstruction i1, IInstruction i2)
        {
            return false;
        }
        public override string ToString()
        {
            return "-";
        }
    }
}
