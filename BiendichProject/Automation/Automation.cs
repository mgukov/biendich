using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation {
    public class Automaton {
        private AState first;

        public Automaton(AState first) {
            this.first = first;
        }
        public AState First {
            get { return first; }
        }
        public virtual Object Run(IChain chain, ref Dictionary<string, Object> param) {
            AState current = first;
            while (current.Motion()) {
                current = current.Advance(chain, ref param);
                if (current == null) return null;
            }
            return current.GetObject(chain, ref param);
        }
    }
}
