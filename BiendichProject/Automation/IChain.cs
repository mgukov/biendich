﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation {
    public interface IChain {
        int Cursor {
            get;
            set;
        }

        IInstruction Current {
            get;
        }

        int Reserved { get; }

        IInstruction Deviation(int sh);

        void Move(int sh);
        bool EOF();
        bool TryMove(int sh);
        void Close();
        int Reserve(int len);
        int ReserveMore(int sh);
    }
}
