﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Automation
{
    class ConditRange : ICondition
    {
        IInstruction[] range;
        public ConditRange(IInstruction[] range)
        {
            this.range = range;
        }
        public bool Check(IInstruction i1, IInstruction i2)
        {
            return !Array.TrueForAll(range, i => !i.Equals(i2));
        }
    }
}
