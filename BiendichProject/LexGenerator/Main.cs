using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Lexical;
using Biendich.Language;

namespace Biendich.Generation.Lexical
{
    class Program
    {
        public static void Main(string[] args)
        {
            ScannGenerator sg = new ScannGenerator();
            sg.GenerateToFile("xml/lexical.xml",
                new Lang("xml/lang.xml"));
        }
    }
}
