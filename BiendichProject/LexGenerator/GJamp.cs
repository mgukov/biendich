using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Automation;
using Biendich.Utility;
using Biendich.Lexical;

namespace Biendich.Generation.Lexical
{
	public class GJump : Jump
	{
        private AState start;
        private int id;
        private static int counter = 0;

        private static int getid()
        {
            return counter++;
        }
        private static GJump[] create(string instr, ICondition condit, int sh)
        {
            GJump[] jamps = new GJump[instr.Length];
            for (int i = 0; i < instr.Length; ++i)
            {
                jamps[i] = new GJump(new CharInstruction(instr[i], 0), condit, sh);
            }
            return jamps;
        }

        public GJump(CharInstruction instruct, ICondition condit, int sh)
            : base(instruct, condit, sh)
        {
            id = getid();
        }

        private GJump(CharInstruction instruct, ICondition condit, int sh, AState start, AState final)
            : base(instruct, condit, sh, final)
        {
            id = getid();
            this.start = start;
        }

        public int Id
        {
            get { return id; }
        }
        public static GJump Forward(ICondition condit, char instr)
		{
            return new GJump(new CharInstruction(instr, 0), condit, 1);
		}

        public static GJump[] Forward(ICondition condit, string instr)
		{
            return create(instr, condit, 1);
		}
        public static GJump Back(ICondition condit, char instr)
		{
            return new GJump(new CharInstruction(instr, 0), condit, -1);
		}

        public static GJump[] Back(ICondition condit, string instr)
		{
            return create(instr, condit, -1);
		}

        public static GJump Fixed(ICondition condit, char instr)
		{
            return new GJump(new CharInstruction(instr, 0), condit, 0);
		}

        public static GJump[] Fixed(ICondition condit, string instr)
        {
            return create(instr, condit, 0);
        }

        private void join(GState s, GState f, Dictionary<int, HashSet<int>> entries)
        {
            HashSet<int> jampid;
            if (entries.TryGetValue(s.Id, out jampid))
            {
                if (!jampid.Add(id))
                    return;
            }
            else
            {
                jampid = new HashSet<int>();
                jampid.Add(id);
                entries.Add(s.Id, jampid);
            }

            GJump like = s.getjamp(this);
            if (like != null)
            {
                foreach (GJump j in f.Jumps)
                    j.join(((GState)(like.State)), (GState)(j.State), entries);
            }
            else
            {
                GJump j = new GJump((CharInstruction)Instruction, Condition, Shift, s, f);
                s.AddJump(j);
            }
        }

        protected override void join(AState s, AState f)
        {
            Dictionary<int, HashSet<int>>
                entries = new Dictionary<int, HashSet<int>>();
            join((GState)s, (GState)f, entries);
        }
		
		public string ToXml()
		{
            return "<j id='" + id.ToString() +
                "' s='" + ((GState)this.start).Id.ToString() +
                "' f='" + ((GState)this.State).Id.ToString() + 
                "' sh='" + Shift.ToString() + "' c='" + Condition.ToString() + 
                "' i='" + Utils.Encode((char)Instruction.Value) + "'/>";
		}
	}
}
