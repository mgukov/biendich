using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Biendich.Automation;
using Biendich.Utility;
using Biendich.Language;

namespace Biendich.Generation.Lexical
{
	public class ScannGenerator
	{
        private class StateCreate : GState
        {
            private int tokenid;

            public StateCreate(int tokenid)
            {
                this.tokenid = tokenid;
            }
            public override string ToXml()
            {
                return "<c id='" + Id.ToString() + "' tok='" +
                    tokenid.ToString() + "'/>";
            }

            public override bool Motion()
            {
                return false;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("");
            }
            public override object GetObject(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("");
            }
        }

        private class StateMove : GState
        {
            public StateMove()
            {
            }
            public override string ToXml()
            {
                return "<s id='" + Id.ToString() + "'/>";
            }

            public override bool Motion()
            {
                return true;
            }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("");
            }
            public override object GetObject(IChain chain, ref Dictionary<string, Object> param)
            {
                throw new Exception("");
            }
        }

		private GState generate(Lang lang)
		{
            GState first = new StateMove();

            GState[] s = new GState[10];
            GState err;

            GJump[][] jj = new GJump[10][];

            GJump[] j = new GJump[10],
                intnums = GJump.Fixed(new ConditEquals(),
                    Utils.SubString(lang.Alphabet("operators") + lang.Alphabet("separators"), ".")),
                sepop = GJump.Fixed(new ConditEquals(),
                    lang.Alphabet("operators") + lang.Alphabet("separators") + "\'\""),
                all = GJump.Fixed(new ConditEquals(), lang.Alphabet("all"));

            GJump anyfw = GJump.Forward(new ConditTrue(), '\0'),
                anyfx = GJump.Fixed(new ConditTrue(), '\0');

            // Comment //
            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateMove();
            s[3] = new StateCreate(lang.TokenId("comment"));
            j[0] = GJump.Forward(new ConditEquals(), '/');
            j[1] = GJump.Forward(new ConditEquals(), '\r');
            j[2] = GJump.Back(new ConditEquals(), '\n');

            Jump.Join(s[0], s[1], j[0]);
            Jump.Join(s[1], s[2], j[1]);
            Jump.Join(s[1], s[1], anyfw);
            Jump.Join(s[2], s[3], j[2]);
            Jump.Join(first, s[0], j[0]);

            // Comment /**/
            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateMove();
            s[3] = new StateCreate(lang.TokenId("comment"));
            j[0] = GJump.Forward(new ConditEquals(), '/');
            j[1] = GJump.Forward(new ConditEquals(), '*');

            Jump.Join(s[0], s[1], j[1]);
            Jump.Join(s[1], s[2], j[1]);
            Jump.Join(s[1], s[1], anyfw);
            Jump.Join(s[2], s[3], j[0]);
            Jump.Join(s[2], s[1], anyfw);
            Jump.Join(first, s[0], j[0]);

            // HexIntConst
            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateCreate(lang.TokenId("hex_int_const"));
            s[3] = new StateMove();
            s[4] = new StateCreate(lang.TokenId("hex_long_const"));

            err = new StateCreate(4000);
            j[0] = GJump.Forward(new ConditEquals(), '0');
            jj[0] = GJump.Forward(new ConditEquals(), "xX");
            jj[1] = GJump.Forward(new ConditEquals(), lang.Alphabet("hexdigits"));
            jj[2] = GJump.Forward(new ConditEquals(), "lL");

            Jump.Join(s[0], s[1], jj[0]);
            Jump.Join(s[1], s[1], jj[1]);
            Jump.Join(s[1], s[2], intnums);     // int
            Jump.Join(s[1], s[3], jj[2]);
            Jump.Join(s[3], s[4], intnums);     // long
            Jump.Join(s[1], err, anyfw);        // error
            Jump.Join(first, s[0], j[0]);

            // OctIntNum
            s[0] = new StateMove();
            s[1] = new StateCreate(lang.TokenId("oct_int_const"));
            s[2] = new StateMove();
            s[3] = new StateCreate(lang.TokenId("oct_long_const"));

			j[0] = GJump.Forward(new ConditEquals(), '0');
            jj[0] = GJump.Forward(new ConditEquals(), lang.Alphabet("octdigits"));
            jj[1] = GJump.Forward(new ConditEquals(), "lL");

            Jump.Join(s[0], s[0], jj[0]);
            Jump.Join(s[0], s[1], intnums);     // int
            Jump.Join(s[0], s[2], jj[1]);
            Jump.Join(s[2], s[3], intnums);     // long
            Jump.Join(s[0], err, anyfw);        // error
            Jump.Join(first, s[0], j[0]);

            // DecIntNum
            s[0] = new StateMove();
            s[1] = new StateCreate(lang.TokenId("dec_int_const"));
            s[2] = new StateMove();
            s[3] = new StateCreate(lang.TokenId("dec_long_const"));

            jj[0] = GJump.Forward(new ConditEquals(), "123456789");
            jj[1] = GJump.Forward(new ConditEquals(), lang.Alphabet("digits"));
            jj[2] = GJump.Forward(new ConditEquals(), "lL");

            Jump.Join(s[0], s[0], jj[1]);
            Jump.Join(s[0], s[1], intnums);
            Jump.Join(s[0], s[2], jj[2]);
            Jump.Join(s[2], s[3], intnums);     // long
            Jump.Join(s[0], err, anyfw);        // error
            Jump.Join(first, s[0], jj[0]);

            // FloatNum
            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateMove();
            s[3] = new StateMove();
            s[4] = new StateMove();
            s[5] = new StateCreate(lang.TokenId("double_const"));
            s[6] = new StateMove();
            s[7] = new StateCreate(lang.TokenId("float_const"));
            s[8] = new StateCreate(4000);
            j[0] = GJump.Forward(new ConditEquals(), '.');
            jj[0] = GJump.Forward(new ConditEquals(), "eE");
            jj[1] = GJump.Forward(new ConditEquals(), "+-");
            jj[2] = GJump.Forward(new ConditEquals(), lang.Alphabet("digits"));
            jj[3] = GJump.Forward(new ConditEquals(), "fF");

            Jump.Join(s[0], s[0], jj[2]);
            Jump.Join(s[0], s[1], j[0]);
            Jump.Join(s[0], s[3], jj[0]);
            Jump.Join(s[1], s[2], jj[2]);
            Jump.Join(s[1], s[5], sepop);
            Jump.Join(s[1], s[6], jj[3]);
            Jump.Join(s[6], s[7], sepop);   // float
            Jump.Join(s[6], s[8], anyfw);   // error
            Jump.Join(s[1], s[3], jj[0]);
            Jump.Join(s[1], s[8], anyfw);   // error
            Jump.Join(s[2], s[2], jj[2]);
            Jump.Join(s[2], s[3], jj[0]);
            Jump.Join(s[2], s[5], sepop);
            Jump.Join(s[2], s[6], jj[3]);
            Jump.Join(s[6], s[7], sepop);   // float
            Jump.Join(s[2], s[8], anyfw);   // error
            Jump.Join(s[3], s[4], jj[1]);
            Jump.Join(s[3], s[4], jj[2]);
            Jump.Join(s[3], s[8], anyfw);   // error
            Jump.Join(s[4], s[4], jj[2]);
            Jump.Join(s[4], s[5], sepop);
            Jump.Join(s[4], s[6], jj[3]);
            Jump.Join(s[6], s[7], sepop);   // float
            Jump.Join(s[4], s[8], anyfw);   // error
            Jump.Join(first, s[0], jj[2]);

            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateMove();
            s[3] = new StateMove();
            s[4] = new StateCreate(lang.TokenId("double_const"));
            s[5] = new StateMove();
            s[6] = new StateCreate(lang.TokenId("float_const"));
            s[7] = new StateCreate(4000);
            j[0] = GJump.Forward(new ConditEquals(), '.');
            jj[0] = GJump.Forward(new ConditEquals(), "eE");
            jj[1] = GJump.Forward(new ConditEquals(), "+-");
            jj[2] = GJump.Forward(new ConditEquals(), lang.Alphabet("digits"));
            jj[3] = GJump.Forward(new ConditEquals(), "fF");

            Jump.Join(s[0], s[1], jj[2]);
            Jump.Join(s[1], s[1], jj[2]);
            Jump.Join(s[1], s[2], jj[0]);
            Jump.Join(s[1], s[4], sepop);
            Jump.Join(s[1], s[5], jj[3]);
            Jump.Join(s[5], s[6], sepop);   // float
            Jump.Join(s[5], s[7], anyfw);   // error
            Jump.Join(s[2], s[3], jj[1]);
            Jump.Join(s[2], s[3], jj[2]);
            Jump.Join(s[2], s[7], anyfw);   // error
            Jump.Join(s[3], s[3], jj[2]);
            Jump.Join(s[3], s[4], sepop);
            Jump.Join(s[3], s[5], jj[3]);
            Jump.Join(s[5], s[6], sepop);   // float
            Jump.Join(s[3], s[7], anyfw);   // error
            Jump.Join(first, s[0], j[0]);

            // KeyWord
            foreach (string word in lang.Tokens("keyword"))
            {
                s[0] = new StateMove();
                s[1] = s[2] = s[0];
                for (int i = 1; i < word.Length; ++i)
                {
                    s[2] = new StateMove();
                    Jump.Join(s[1], s[2], GJump.Forward(new ConditEquals(), word[i]));
                    s[1] = s[2];
                }
                s[3] = new StateCreate(lang.TokenId(word));
                Jump.Join(s[2], s[3], sepop);
                Jump.Join(first, s[0], GJump.Forward(new ConditEquals(), word[0]));
            }

            // Operator
            foreach (string word in lang.Tokens("operator"))
            {
                s[0] = new StateMove();
                s[1] = s[2] = s[0];
                for (int i = 1; i < word.Length; ++i)
                {
                    s[2] = new StateMove();
                    Jump.Join(s[1], s[2], GJump.Forward(new ConditEquals(), word[i]));
                    s[1] = s[2];
                }
                s[3] = new StateCreate(lang.TokenId(word));
                Jump.Join(s[2], s[3], anyfx);
                Jump.Join(first, s[0], GJump.Forward(new ConditEquals(), word[0]));
            }

            // Identificator
            s[0] = new StateMove();
            s[1] = new StateCreate(lang.TokenId("identif"));
            s[2] = new StateCreate(1070);
            jj[0] = GJump.Forward(new ConditEquals(), 
                lang.Alphabet("letters"));
            jj[1] = GJump.Forward(new ConditEquals(), 
                lang.Alphabet("letters") + lang.Alphabet("digits"));

            Jump.Join(s[0], s[0], jj[1]);
            Jump.Join(s[0], s[1], sepop);
            Jump.Join(s[0], s[2], anyfw);
            Jump.Join(first, s[0], jj[0]);

            // String
            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateCreate(lang.TokenId("string_const"));
            s[3] = new StateCreate(4001);
            j[0] = GJump.Forward(new ConditEquals(), '"');
            j[1] = GJump.Forward(new ConditEquals(), '\\');
            j[2] = GJump.Fixed(new ConditEquals(), '\r');

            Jump.Join(s[0], s[1], j[1]);
            Jump.Join(s[0], s[2], j[0]);
            Jump.Join(s[0], s[3], j[2]);   // error
            Jump.Join(s[0], s[0], anyfw);
            Jump.Join(s[1], s[3], j[2]);   // error
            Jump.Join(s[1], s[0], anyfw);
            Jump.Join(first, s[0], j[0]);

            // Char
            s[0] = new StateMove();
            s[1] = new StateMove();
            s[2] = new StateCreate(lang.TokenId("char_const"));
            s[3] = new StateCreate(4002);
            j[0] = GJump.Forward(new ConditEquals(), '\'');
            j[1] = GJump.Forward(new ConditEquals(), '\\');
            j[2] = GJump.Fixed(new ConditEquals(), '\r');

            Jump.Join(s[0], s[1], j[1]);
            Jump.Join(s[0], s[2], j[0]);
            Jump.Join(s[0], s[3], j[2]);   // error
            Jump.Join(s[0], s[0], anyfw);
            Jump.Join(s[1], s[3], j[2]);   // error
            Jump.Join(s[1], s[0], anyfw);
            Jump.Join(first, s[0], j[0]);

            // Separator
            foreach (char ch in lang.Alphabet("separators"))
            {
                s[0] = new StateCreate(lang.TokenId(ch.ToString()));
                j[0] = GJump.Forward(new ConditEquals(), ch);
                Jump.Join(first, s[0], j[0]);
            }

            ////////////// ������ //////////////////////
            // OctIntNum
            s[0] = new StateMove();
            s[1] = new StateCreate(4000);
            j[0] = GJump.Forward(new ConditEquals(), '0');
            jj[0] = GJump.Forward(new ConditEquals(), lang.Alphabet("digits"));

            Jump.Join(s[0], s[0], jj[0]);
            Jump.Join(s[0], s[1], intnums);
            Jump.Join(first, s[0], j[0]);

            // DecIntNum
            s[0] = new StateMove();
            s[1] = new StateCreate(4000);
            jj[0] = GJump.Forward(new ConditEquals(), lang.Alphabet("digits"));
            jj[1] = GJump.Fixed(new ConditEquals(), lang.Alphabet("letters"));

            Jump.Join(s[0], s[0], jj[0]);
            Jump.Join(s[0], s[1], jj[1]);
            Jump.Join(first, s[0], jj[0]);
            
            return first;
		}
        public void GenerateToFile(
            string path, Lang lang)
		{
            GState first = generate(lang);
            HashSet<GState> states = new HashSet<GState>();
            HashSet<GJump> jumps = new HashSet<GJump>();
            Queue<GState> q = new Queue<GState>();
            GState curr;
            
            states.Add(first);
            q.Enqueue(first);
            while (q.Count > 0)
            {
                curr = q.Dequeue();
                foreach (Jump j in curr.Jumps)
                {
                    jumps.Add((GJump)j);
                    if (states.Add((GState)j.State))
                        q.Enqueue((GState)j.State);
                }
            }
            StringBuilder str = new StringBuilder();
            str.AppendLine("<tree>");
            str.AppendLine("\t<states>");
            foreach (GState s in states)
            {
                str.AppendLine("\t\t" + s.ToXml());
            }
            str.AppendLine("\t</states>");
            str.AppendLine("\t<jumps>");
            foreach (GJump j in jumps)
            {
                str.AppendLine("\t\t" + j.ToXml());
            }
            str.AppendLine("\t</jumps>");
            str.AppendLine("</tree>");
            File.WriteAllText(path, str.ToString());
		}
	}
}
