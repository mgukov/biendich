using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Automation;

namespace Biendich.Generation.Lexical
{
	public abstract class GState : AState
	{

		private static int counter = 0;
		private static int getid()
		{
            return counter++;
		}

		private int id;
        public int Id
        {
            get { return id; }
        }

		public GState()
		{
            id = getid();
		}

        internal GJump getjamp(GJump jump)
		{
            foreach (GJump j in jumps)
            {
                if (j.Condition.Check(jump.Instruction, j.Instruction) &&
                    jump.Condition.Check(jump.Instruction, j.Instruction))
                    return j;
            }
            return null;
		}
        public abstract string ToXml();
    }
}
