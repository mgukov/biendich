using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Utility {
    public class Collection<TK, TV> {
        private Dictionary<TK, TV> coll;

        public Collection() {
            coll = new Dictionary<TK, TV>();
        }

        public void Add(TK key, TV val) {
            coll.Add(key, val);
        }

        public TV Get(TK key) {
            TV val;
            coll.TryGetValue(key, out val);
            return val;
        }
    }
}
