﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Utility {
    public class Utils {
        public static string Encode(char ch) {
            switch (ch) {
                case ' ': return "&#32;";
                case '\r': return "&#13;";
                case '\n': return "&#10;";
                case '\t': return "&#9;";
                case '\'': return "&apos;";
                case '\"': return "&quot;";
                case '&': return "&amp;";
                case '<': return "&lt;";
                case '>': return "&gt;";
                case '\0': return "\\0";
                default: return ch.ToString();
            }
        }
        public static char Decode(string str) {
            switch (str) {
                //case ' ': return "&#32;";
                //case '\r': return "&#13;";
                //case '\n': return "&#10;";
                //case '\t': return "&#9;";
                //case '\'': return "&apos;";
                //case '\"': return "&quot;";
                //case '&': return "&amp;";
                //case '<': return "&lt;";
                //case '>': return "&gt;";
                case "\\0": return '\0';
                default: return str[0];
            }
        }
        public static string SubString(string s1, string s2) {
            StringBuilder str = new StringBuilder();
            foreach (char c in s1) {
                if (s2.IndexOf(c) == -1) str.Append(c);
            }
            return str.ToString();
        }
        public static T[] SubArray<T>(IList<T> array, int first, int last) {
            int len = last - first + 1;
            T[] arr = new T[len];
            for (int i = first, j = 0; i <= last; ++i, ++j) {
                arr[j] = array[i];
            }
            return arr;
        }
        public static T[] SubArray<T>(IList<T> array, int[] indexes) {
            int len = indexes.Length;
            T[] arr = new T[len];
            for (int i = 0; i < len; ++i) {
                arr[i] = array[indexes[i]];
            }
            return arr;
        }

    }
}
