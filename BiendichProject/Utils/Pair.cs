﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biendich.Utility {
    public class Pair {
        protected Pair() { }
        public static Pair<T1, T2> Create<T1, T2>(T1 f, T2 s) {
            return new Pair<T1, T2>(f, s);
        }
    }
    public class Pair<T1, T2> : Pair {
        private T1 first;
        private T2 second;
        public T1 First {
            get { return first; }
        }
        public T2 Second {
            get { return second; }
        }
        public Pair(T1 f, T2 s) {
            this.first = f;
            this.second = s;
        }
        public Pair<T1, T2> Clone() {
            return new Pair<T1, T2>(first, second);
        }
    }
}
