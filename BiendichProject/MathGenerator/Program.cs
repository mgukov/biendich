﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Biendich.Language;

namespace Biendich.Generation.Math
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MathGenerator sg = new MathGenerator();
            string xml = sg.GenerateXml(new Lang("xml/lang.xml"));
            File.WriteAllText("xml/math.xml", xml);
        }
    }
}
