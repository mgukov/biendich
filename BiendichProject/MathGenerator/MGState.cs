﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Biendich.Utility;
using Biendich.Lang;
using Biendich.Automation;

public class MGState : AState
{
    private int id;
    private string type;
    private List<Pair<string, string>> attr;

    private static int counter = 0;
    private static int getid()
    {
        return counter++;
    }

    public MGState(string type)
    {
        this.id = getid();
        this.type = type;
        this.attr = new List<Pair<string, string>>();
    }

    public int Id
    {
        get { return id; }
    }

    public void AddAttr(string name, string value)
    {
        this.attr.Add(Pair.Create(name, value));
    }
    public virtual string ToXml()
    {
        StringBuilder xml = new StringBuilder();
        xml.Append("<state id='");
        xml.Append(id.ToString());
        xml.Append("' type='");
        xml.Append(type);
        xml.Append("'");

        foreach (Pair<string, string> item in this.attr)
        {
            xml.Append(" ");
            xml.Append(item.First);
            xml.Append("='");
            xml.Append(item.Second);
            xml.Append("'");
        }
        xml.Append(" />");
        return xml.ToString();
    }
    public override bool Motion()
    {
        return true;
    }
    public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
    {
        throw new Exception("");
    }
    public override object GetObject(IChain chain, ref Dictionary<string, Object> param)
    {
        throw new Exception("");
    }
}