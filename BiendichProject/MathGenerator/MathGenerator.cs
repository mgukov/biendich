﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Biendich.Utility;
using Biendich.Language;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Generation;

namespace Biendich.Generation.Math
{
    public class MathGenerator : AGenerator
    {
        private class StateMove : GState
        {
            public StateMove()
                :base("MOVE")
            {
            }
        }

        private class StateOperand : GState
        {
            public StateOperand(int id)
                : base("OPRN")
            {
                AddAttribute("iid", id.ToString());
            }
            public static StateOperand[] Create(int[] ids)
            {
                StateOperand[] operands = new StateOperand[ids.Length];
                for (int i = 0; i < ids.Length; ++i)
                {
                    operands[i] = new StateOperand(ids[i]);
                }
                return operands;
            }
        }

        private class StateOperation : GState
        {
            public StateOperation(int id)
                : base("OPRT")
            {
                AddAttribute("iid", id.ToString());
            }
            public static StateOperation[] Create(int[] ids)
            {
                StateOperation[] operators = new StateOperation[ids.Length];
                for (int i = 0; i < ids.Length; ++i)
                {
                    operators[i] = new StateOperation(ids[i]);
                }
                return operators;
            }
        }


        private class StateAddParametr : GState
        {
            public StateAddParametr()
                : base("PRM")
            {
            }
        }

        private class StateSubexpression : GState
        {
            public StateSubexpression()
                :base("SUBE")
            {
            }
        }

        private class StatePoint : GState
        {
            public StatePoint()
                :base("PNT")
            {
            }
        }

        private class StateBackoff : GState
        {
            public StateBackoff()
                :base("BACK")
            {
            }
        }

        private class StateSubprogram : GState
        {
            public StateSubprogram()
                :base("SUBP")
            {
            }
        }


        private class StateFillOperator : GState
        {
            public StateFillOperator()
                : base("FILL")
            {
            }
        }

        private class StatePopOperator : GState
        {
            public StatePopOperator()
                : base("POP")
            {
            }
        }


        private class StateReturn : GState
        {
            public StateReturn()
                :base("RET")
            {
            }
        }

        private class StateCreate : GState
        {
            public StateCreate()
                :base("CRT")
            {
            }
        }

        private class StateError : GState
        {
            public StateError(int id)
                :base("ERR")
            {
                AddAttribute("e", id.ToString());
            }
        }

        private class StateFinish : GState
        {
            public StateFinish()
                : base("FIN")
            {
            }
        }



        public override GState Generate(Lang lang)
        {
            AState[] s = new AState[10];
            Jump[] j = new Jump[10];
            GState first = new StateMove();

            List<Jump>
                j_bin_operat = new List<Jump>(),
                j_left_unoperat = new List<Jump>(),
                j_right_unoperat = new List<Jump>(),
                j_operand_var = new List<Jump>(),
                j_operand_const = new List<Jump>(),
                j_type = new List<Jump>(),
                j_create = new List<Jump>(),

                j_bin_operat_fix = new List<Jump>(),
                j_left_unoperat_fix = new List<Jump>(),
                j_right_unoperat_fix = new List<Jump>(),
                j_operand_var_fix = new List<Jump>(),
                j_operand_const_fix = new List<Jump>(),
                j_type_fix = new List<Jump>();

            j_operand_var.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "this", "super",
            })), new ConditEquals(), 1));

            j_operand_var_fix.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "this", "super",
            })), new ConditEquals(), 0));

            j_operand_const.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "string_const", "char_const", "dec_int_const", "oct_int_const",  "hex_int_const", 
                "dec_long_const", "oct_long_const",  "hex_long_const", "float_const", 
                "double_const", "true", "false" 
            })), new ConditEquals(), 1));

            j_operand_const_fix.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "string_const", "char_const", "dec_int_const", "oct_int_const",  "hex_int_const", 
                "dec_long_const", "oct_long_const",  "hex_long_const", "float_const", 
                "double_const", "true", "false" 
            })), new ConditEquals(), 0));

            j_type.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "int", "short", "byte", "long", "float", "double", "char", "boolean"
            })), new ConditEquals(), 1));

            j_bin_operat.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "=", "-=", "+=", "*=", "/=", "%=", "|=", "^=", "&=", 
                ">>=", "<<=", "+", "-", "/", "*", "%", "^", "|", "||", 
                "==", "!=", "<", ">", ">=", "<=", "&", ">>", "<<", "&&", "instanceof"
            })), new ConditEquals(), 1));

            j_bin_operat_fix.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "=", "-=", "+=", "*=", "/=", "%=", "|=", "^=", "&=", 
                ">>=", "<<=", "+", "-", "/", "*", "%", "^", "|", "||", 
                "==", "!=", "<", ">", ">=", "<=", "&", ">>", "<<", "&&", "instanceof"
            })), new ConditEquals(), 0));

            j_left_unoperat.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "++", "--", "+", "-", "!", "~"
            })), new ConditEquals(), 1));

            j_right_unoperat.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "++", "--"
            })), new ConditEquals(), 1));

            j_right_unoperat_fix.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "++", "--"
            })), new ConditEquals(), 0));

            j_create.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                ";", ",", ")", "]", "}", ":"
            })), new ConditEquals(), 0));


            Jump
                j_open_round_br = new GJump(new Token(lang.TokenId("(")), 1),
                j_open_round_br_fix = new GJump(new Token(lang.TokenId("(")), 0),
                j_close_round_br = new GJump(new Token(lang.TokenId(")")), 1),
                j_comma = new GJump(new Token(lang.TokenId(",")), 1),
                j_comma_fix = new GJump(new Token(lang.TokenId(",")), 0),
                j_operator_dot = new GJump(new Token(lang.TokenId(".")), 1),
                j_operand_ident = new GJump(new Token(lang.TokenId("identif")), 1),
                j_operand_ident_fix = new GJump(new Token(lang.TokenId("identif")), 0),
                j_operator_new = new GJump(new Token(lang.TokenId("new")), 1),
                j_open_brace = new GJump(new Token(lang.TokenId("{")), 1),
                j_close_brace = new GJump(new Token(lang.TokenId("}")), 1),
                j_open_brace_fix = new GJump(new Token(lang.TokenId("{")), 0),
                j_close_brace_fix = new GJump(new Token(lang.TokenId("}")), 0),
                j_open_sq_br = new GJump(new Token(lang.TokenId("[")), 1),
                j_close_sq_br = new GJump(new Token(lang.TokenId("]")), 1),
                j_array = new GJump(new Token(lang.TokenId("[]")), 1),
                j_class = new GJump(new Token(lang.TokenId("class")), 1),
                j_ter = new GJump(new Token(lang.TokenId("?")), 1),
                j_colon = new GJump(new Token(lang.TokenId(":")), 1),

                j_fix = new GJump(new Token(0), new ConditTrue(), 0),
                j_fw = new GJump(new Token(0), new ConditTrue(), 1),
                j_false = new GJump(new Token(0), new ConditFalse(), 0),

                j_operand_this = new GJump(new Token(lang.TokenId("this")), 1),
                j_operand_super = new GJump(new Token(lang.TokenId("super")), 1);


            GState[]
                s_operand_type = StateOperand.Create(
                    new int[] { 1036, 1040, 1044, 1048, 1052, 1056, 1060, 1064 }),
                s_operand_const = StateOperand.Create(
                    new int[] { 1004, 1008, 1012, 1014, 1016, 1018, 1020, 1021, 1022, 
                        1023, 1024, 1026 }),

                s_bin_operator = StateOperation.Create(
                    new int[] { 1170, 1180, 1190, 1200, 1210, 1220, 1230, 1240, 1250, 
                    1260, 1270, 1280, 1290, 1300, 1310, 1320, 1330, 1340, 1350, 1360, 
                    1370, 1380, 1390, 1400, 1410, 1420, 1430, 1440, 1450, 1415}),
                s_left_un_operator = StateOperation.Create(
                    new int[] { 1110, 1120, 1130, 1140, 1150, 1160 }),
                s_right_un_operator = StateOperation.Create(
                    new int[] { 1090, 1100 }),


                s_operand_type_cast = StateOperand.Create(
                    new int[] { 1036, 1040, 1044, 1048, 1052, 1056, 1060, 1064 });


            GState
                s_operand_ident = new StateOperand(1000),
                s_operand_this = new StateOperand(1028),
                s_operand_super = new StateOperand(1032),
                s_operand_class = new StateOperand(1034),

                s_operand_ident_cast = new StateOperand(1000),
                s_bin_operator_dot_cast = new StateOperation(1040),
                s_cast_type = new StateOperation(1080),
                s_cast_ident = new StateOperation(1080),

                s_bin_operator_dot = new StateOperation(1040),
                s_operator_new = new StateOperation(1070),

                s_callmethod = new StateOperation(1050),
                s_params = new StateOperation(1010),
                s_create = new StateCreate(),
                s_initarray = new StateOperation(1030),
                s_array = new StateOperation(1060),
                s_ter_operator = new StateOperation(1460),

                s_point_open_round_br = new StatePoint(),
                s_backoff_open_round_br = new StateBackoff(),
                s_subexpression_round_br = new StateSubexpression(),
                s_subexpression_func = new StateSubexpression(),

                s_move_new = new StateMove(),
                s_subprog_new = new StateSubprogram(),
                s_continue = new StateMove(),

                s_operator_arr,
                s_mv_err_1000 = new StateMove(),
                s_err_1000 = new StateError(1000),
                s_finish = new StateFinish();

            AState[]
                s_move = new StateMove[4],
                s_addparam = new StateAddParametr[4],
                s_fillop = new StateFillOperator[4],
                s_err = new StateError[4],
                s_subexp = new StateSubexpression[4];

            
            
            // >>>>>>>>>>>>>>>>>>>>>>> errors <<<<<<<<<<<<<<<<<<<<<<<
            Jump.Join(s_mv_err_1000, s_finish, j_create.ToArray());
            Jump.Join(s_err_1000, s_mv_err_1000, j_fix);
            Jump.Join(s_mv_err_1000, s_mv_err_1000, j_fw);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            
            // >>>>>>>>>>>>>>>>>>>> s_subprog_new <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_continue, s_create, j_create.ToArray());
            Jump.Join(s_continue, s_bin_operator, j_bin_operat.ToArray());
            Jump.Join(s_continue, s_bin_operator_dot, j_operator_dot);
            Jump.Join(s_continue, s_right_un_operator, j_right_unoperat.ToArray());
            Jump.Join(s_continue, s_ter_operator, j_ter);
            Jump.Join(s_subprog_new, s_operator_new, j_fix);
            Jump.Join(s_move_new, s_subprog_new, j_fix);
            Jump.Join(s_move_new, s_continue, j_false);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>>>>>> first <<<<<<<<<<<<<<<<<<<<<<<
            Jump.Join(first, s_point_open_round_br, j_open_round_br);
            Jump.Join(first, s_operand_type, j_type.ToArray());
            Jump.Join(first, s_operand_ident, j_operand_ident);
            Jump.Join(first, s_operand_const, j_operand_const.ToArray());
            Jump.Join(first, s_left_un_operator, j_left_unoperat.ToArray());
            Jump.Join(first, s_move_new, j_operator_new);
            Jump.Join(first, s_operand_this, j_operand_this);
            Jump.Join(first, s_operand_super, j_operand_super);
            
            Jump.Join(first, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> this, super <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_operand_this, s_create, j_create.ToArray());
            Jump.Join(s_operand_this, s_callmethod, j_open_round_br);
            Jump.Join(s_operand_this, s_bin_operator_dot, j_operator_dot);
            Jump.Join(s_operand_this, s_ter_operator, j_ter);

            Jump.Join(s_operand_super, s_create, j_create.ToArray());
            Jump.Join(s_operand_super, s_callmethod, j_open_round_br);
            Jump.Join(s_operand_super, s_bin_operator_dot, j_operator_dot);
            Jump.Join(s_operand_super, s_ter_operator, j_ter);

            Jump.Join(s_operand_class, s_bin_operator, j_bin_operat.ToArray());
            Jump.Join(s_operand_class, s_create, j_create.ToArray());
            Jump.Join(s_operand_class, s_ter_operator, j_ter);

            Jump.Join(s_operand_this, s_err_1000, j_fix);
            Jump.Join(s_operand_super, s_err_1000, j_fix);
            Jump.Join(s_operand_class, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            

            // >>>>>>>>>>>>>>> s_point_open_round_br <<<<<<<<<<<<<<<<
            Jump.Join(s_point_open_round_br, s_operand_type_cast, j_type.ToArray());
            Jump.Join(s_point_open_round_br, s_operand_ident_cast, j_operand_ident);
            Jump.Join(s_point_open_round_br, s_subexpression_round_br, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>> s_operand_ident_cast <<<<<<<<<<<<<<<<
            s_move[0] = new StateMove();
            s_move[1] = new StateMove();
            s_operator_arr = new StateOperation(1020);
            Jump.Join(s_operand_ident_cast, s_bin_operator_dot_cast, j_operator_dot);
            Jump.Join(s_operand_ident_cast, s_operator_arr, j_array);
            Jump.Join(s_operator_arr, s_operator_arr, j_array);
            Jump.Join(s_operator_arr, s_cast_ident, j_close_round_br);
            Jump.Join(s_operator_arr, s_backoff_open_round_br, j_fix);

            Jump.Join(s_operand_ident_cast, s_move[1], j_close_round_br);
            Jump.Join(s_move[1], s_cast_ident, j_operand_ident_fix);
            Jump.Join(s_move[1], s_cast_ident, j_operand_const_fix.ToArray());
            Jump.Join(s_move[1], s_cast_ident, j_open_round_br_fix);
            Jump.Join(s_move[1], s_backoff_open_round_br, j_fix);
            Jump.Join(s_operand_ident_cast, s_backoff_open_round_br, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>> s_bin_operator_dot_cast <<<<<<<<<<<<<<<
            Jump.Join(s_bin_operator_dot_cast, s_operand_ident_cast, j_operand_ident);
            Jump.Join(s_bin_operator_dot_cast, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_cast_ident <<<<<<<<<<<<<<<<<<<<
            Jump.Join(s_cast_ident, s_operand_ident, j_operand_ident);
            Jump.Join(s_cast_ident, s_operand_const, j_operand_const.ToArray());
            Jump.Join(s_cast_ident, s_left_un_operator, j_left_unoperat.ToArray());
            Jump.Join(s_cast_ident, first, j_open_round_br_fix);
            Jump.Join(s_cast_ident, s_move_new, j_operator_new);

            Jump.Join(s_cast_ident, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>> s_subexpression_round_br <<<<<<<<<<<<<<
            s_move[0] = new StateMove();
            s_move[1] = new StateMove();
            s_subexp[0] = new StateSubexpression();
            Jump.Join(s_subexpression_round_br, s_move[0], j_close_round_br);
            Jump.Join(s_move[0], s_bin_operator, j_bin_operat.ToArray());
            Jump.Join(s_move[0], s_bin_operator_dot, j_operator_dot);
            Jump.Join(s_move[0], s_right_un_operator, j_right_unoperat.ToArray());
            Jump.Join(s_move[0], s_ter_operator, j_ter);
            Jump.Join(s_move[0], s_array, j_open_sq_br);
                Jump.Join(s_array, s_subexp[0], j_fix);
                Jump.Join(s_subexp[0], s_move[1], j_close_sq_br);
                Jump.Join(s_move[1], s_array, j_open_sq_br);
                Jump.Join(s_move[1], s_move[0], j_fix);
            Jump.Join(s_move[0], s_create, j_create.ToArray());

            Jump.Join(s_move[0], s_err_1000, j_fix);
            Jump.Join(s_subexp[0], s_err_1000, j_fix);
            Jump.Join(s_subexpression_round_br, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>> s_operand_type <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_operand_type, s_bin_operator_dot, j_operator_dot);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>> s_operand_const <<<<<<<<<<<<<<<<<<
            Jump.JoinAll(s_operand_const, s_create, j_create.ToArray());
            Jump.JoinFParallelJ(s_operand_const, s_bin_operator, j_bin_operat.ToArray());
            Jump.Join(s_operand_const, s_bin_operator_dot, j_operator_dot);
            Jump.JoinFParallelJ(s_operand_const, s_right_un_operator, j_right_unoperat.ToArray());
            Jump.Join(s_operand_const, s_ter_operator, j_ter);

            Jump.Join(s_operand_const, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_operand_ident <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_operand_ident, s_callmethod, j_open_round_br);
                Jump.Join(s_callmethod, s_params, j_fix);
            Jump.Join(s_operand_ident, s_bin_operator_dot, j_operator_dot);
            Jump.Join(s_operand_ident, s_bin_operator, j_bin_operat.ToArray());
            Jump.Join(s_operand_ident, s_right_un_operator, j_right_unoperat.ToArray());
            Jump.Join(s_operand_ident, s_array, j_open_sq_br);
            Jump.Join(s_operand_ident, s_create, j_create.ToArray());
            Jump.Join(s_operand_ident, s_ter_operator, j_ter);

            Jump.Join(s_operand_ident, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_bin_operator <<<<<<<<<<<<<<<<<<
            Jump.Join(s_bin_operator, s_point_open_round_br, j_open_round_br);
            Jump.Join(s_bin_operator, s_operand_ident, j_operand_ident);
            Jump.JoinFParallelJ(s_bin_operator, s_operand_const, j_operand_const.ToArray());
            Jump.JoinFParallelJ(s_bin_operator, s_operand_type, j_type.ToArray());
            Jump.JoinFParallelJ(s_bin_operator, s_left_un_operator, j_left_unoperat.ToArray());
            Jump.Join(s_bin_operator, s_move_new, j_operator_new);

            Jump.Join(s_bin_operator, s_operand_this, j_operand_this);
            Jump.Join(s_bin_operator, s_operand_super, j_operand_super);

            Jump.Join(s_bin_operator, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_ter_operator <<<<<<<<<<<<<<<<<<
            s_subexp[0] = new StateSubexpression();
            s_subexp[1] = new StateSubexpression();
            Jump.Join(s_ter_operator, s_subexp[0], j_fix);
            Jump.Join(s_subexp[0], s_subexp[1], j_colon);
            Jump.Join(s_subexp[1], s_create, j_create.ToArray());

            Jump.Join(s_subexp[1], s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>> s_left_un_operator <<<<<<<<<<<<<<<<
            Jump.Join(s_left_un_operator, s_point_open_round_br, j_open_round_br);
            Jump.Join(s_left_un_operator, s_operand_ident, j_operand_ident);
            Jump.JoinFParallelJ(s_left_un_operator, s_operand_const, j_operand_const.ToArray());
            Jump.JoinFParallelJ(s_left_un_operator, s_left_un_operator, j_left_unoperat.ToArray());
            Jump.Join(s_left_un_operator, s_move_new, j_operator_new);

            Jump.Join(s_left_un_operator, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>> s_right_un_operator <<<<<<<<<<<<<<<<
            Jump.JoinFParallelJ(s_right_un_operator, s_bin_operator, j_bin_operat.ToArray());
            Jump.JoinAll(s_right_un_operator, s_create, j_create.ToArray());
            //Jump.Join(s_right_un_operator, s_operand_const, j_operand_const.ToArray());
            Jump.Join(s_right_un_operator, s_ter_operator, j_ter);

            Jump.Join(s_right_un_operator, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>> s_cast_operand_type <<<<<<<<<<<<<<<<
            s_operator_arr = new StateOperation(1020);
            s_move[0] = new StateMove();
            Jump.Join(s_operand_type_cast, s_operator_arr, j_array);
            Jump.Join(s_operator_arr, s_operator_arr, j_array);
            Jump.Join(s_operator_arr, s_cast_type, j_close_round_br);
            Jump.Join(s_operator_arr, s_backoff_open_round_br, j_fix);

            Jump.Join(s_operand_type_cast, s_cast_type, j_close_round_br);
            Jump.Join(s_operand_type_cast, s_backoff_open_round_br, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>>> s_cast_type <<<<<<<<<<<<<<<<<<<<
            Jump.Join(s_cast_type, s_operand_ident, j_operand_ident);
            Jump.Join(s_cast_type, s_operand_const, j_operand_const.ToArray());
            Jump.Join(s_cast_type, s_left_un_operator, j_left_unoperat.ToArray());
            Jump.Join(s_cast_type, first, j_open_round_br_fix);
            Jump.Join(s_cast_type, s_move_new, j_operator_new);

            Jump.Join(s_cast_type, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>> s_backoff_open_round_br <<<<<<<<<<<<<<
            Jump.Join(s_backoff_open_round_br, s_subexpression_round_br, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>> s_call_function <<<<<<<<<<<<<<<<<<
            s_move[1] = new StateMove();
            s_fillop[0] = new StateFillOperator();
            Jump.Join(s_params, s_fillop[0], j_close_round_br);
            Jump.Join(s_fillop[0], s_move[1], j_fix);
            Jump.Join(s_move[1], s_bin_operator, j_bin_operat.ToArray());
            Jump.Join(s_move[1], s_bin_operator_dot, j_operator_dot);
            Jump.Join(s_move[1], s_right_un_operator, j_right_unoperat.ToArray());
            Jump.Join(s_move[1], s_create, j_create.ToArray());
            Jump.Join(s_params, s_subexpression_func, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>> s_subexpression_func <<<<<<<<<<<<<<<
            s_addparam[0] = new StateAddParametr();
            Jump.Join(s_subexpression_func, s_addparam[0], j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_parametrs <<<<<<<<<<<<<<<<<<<<<
            Jump.Join(s_addparam[0], s_fillop[0], j_close_round_br);
            Jump.Join(s_move[1], s_ter_operator, j_ter);
            //Jump.Join(s_move[1], s_bin_operator, j_bin_operat.ToArray());
            //Jump.Join(s_move[1], s_bin_operator_dot, j_operator_dot);
            //Jump.Join(s_move[1], s_right_un_operator, j_right_unoperat.ToArray());
            //Jump.Join(s_move[1], s_create, j_create.ToArray());
            Jump.Join(s_addparam[0], s_subexpression_func, j_comma);

            Jump.Join(s_move[1], s_err_1000, j_fix);
            Jump.Join(s_addparam[0], s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>> s_bin_operator_dot <<<<<<<<<<<<<<<<
            Jump.Join(s_bin_operator_dot, s_operand_ident, j_operand_ident);
            Jump.Join(s_bin_operator_dot, s_operand_class, j_class);
            Jump.Join(s_bin_operator_dot, s_operand_super, j_operand_super);
            Jump.Join(s_bin_operator_dot, s_operand_this, j_operand_this);

            Jump.Join(s_bin_operator_dot, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            
            // >>>>>>>>>>>>>>>>>>>> object creation <<<<<<<<<<<<<<<<<<<
            GState[]
                s_operand_type_new = StateOperand.Create(
                    new int[] { 1036, 1040, 1044, 1048, 1052, 1056, 1060, 1064 });
            GState
                s_subprog_arr, s_return,
                s_operator_arr_fill = new StateOperation(1020),
                s_operator_arr_empty = new StateOperation(1020),
                s_operator_arr_fe = new StateOperation(1020),
                s_operator_dimension = new StateOperation(1000),
                s_operand_ident_new = new StateOperand(1000),
                s_operand_dot_ident_new = new StateOperand(1000),
                s_bin_operator_dot_new = new StateOperation(1040),
                s_pop;

            

            s_return = new StateReturn();
            s_subprog_arr = new StateSubprogram();
            Jump.Join(s_subprog_arr, s_initarray, j_fix);

            // >>>>>>>>>>>>>>>>>>>> s_operator_new <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_operator_new, s_operand_type_new, j_type.ToArray());
            Jump.Join(s_operator_new, s_operand_ident_new, j_operand_ident);
            
            Jump.Join(s_operator_new, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_operand_ident_new <<<<<<<<<<<<<<<<<<<
            s_params = new StateOperation(1010);
            s_pop = new StatePopOperator();
            Jump.Join(s_operand_ident_new, s_params, j_open_round_br);
            Jump.Join(s_operand_ident_new, s_operator_arr_fill, j_open_sq_br);
            Jump.Join(s_operand_ident_new, s_operator_arr_empty, j_array);
            Jump.Join(s_operand_ident_new, s_bin_operator_dot_new, j_operator_dot);
            
            Jump.Join(s_operand_ident_new, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_bin_operator_dot_new <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_bin_operator_dot_new, s_operand_dot_ident_new, j_operand_ident);

            Jump.Join(s_bin_operator_dot_new, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_operand_dot_ident_new <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_operand_dot_ident_new, s_bin_operator_dot_new, j_operator_dot);
            Jump.Join(s_operand_dot_ident_new, s_operator_arr_fill, j_open_sq_br);
            Jump.Join(s_operand_dot_ident_new, s_operator_arr_empty, j_array);
            Jump.Join(s_operand_dot_ident_new, s_pop, j_open_round_br);
            Jump.Join(s_pop, s_params, j_fix);

            Jump.Join(s_operand_dot_ident_new, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_operand_type_new <<<<<<<<<<<<<<<<<<<
            Jump.Join(s_operand_type_new, s_params, j_open_round_br);
            Jump.Join(s_operand_type_new, s_operator_arr_fill, j_open_sq_br);
            Jump.Join(s_operand_type_new, s_operator_arr_empty, j_array);

            Jump.Join(s_operand_type_new, s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // >>>>>>>>>>>>>>>>>>>> s_params <<<<<<<<<<<<<<<<<<<
            s_subexp[0] = new StateSubexpression();
            s_addparam[0] = new StateAddParametr();
            s_addparam[1] = new StateAddParametr();
            s_fillop[0] = new StateFillOperator();
            Jump.Join(s_params, s_fillop[0], j_close_round_br);
                Jump.Join(s_fillop[0], s_addparam[1], j_fix); //
            Jump.Join(s_params, s_subexp[0], j_fix);            
                Jump.Join(s_subexp[0], s_addparam[0], j_fix);
                Jump.Join(s_addparam[0], s_subexp[0], j_comma);
                Jump.Join(s_addparam[0], s_fillop[0], j_close_round_br);
                Jump.Join(s_addparam[1], s_return, j_fix);


            s_move[0] = new StateMove();
            s_addparam[0] = new StateAddParametr();
            Jump.Join(s_move[0], s_subprog_arr, j_open_brace);
            //>>>>
            Jump.Join(s_move[0], s_addparam[0], j_false);
            Jump.Join(s_addparam[0], s_return, j_fix);
            //<<<<


            // >>>>>>>>>>>>>>>>>>>> s_operator_arr <<<<<<<<<<<<<<<<<<<
            s_pop = new StatePopOperator();
            Jump.Join(s_operator_arr_empty, s_operator_arr_empty, j_array);
            Jump.Join(s_operator_arr_empty, s_move[0], j_fix);
            Jump.Join(s_operator_arr_fill, s_operator_dimension, j_fix);


            // >>>>>>>>>>>>>>>>>>>> s_operator_dimension <<<<<<<<<<<<<<<<<<<
            s_subexp[0] = new StateSubexpression();
            s_addparam[0] = new StateAddParametr();
            s_addparam[1] = new StateAddParametr();
            s_fillop[0] = new StateFillOperator();
            Jump.Join(s_operator_dimension, s_subexp[0], j_fix);
                Jump.Join(s_subexp[0], s_addparam[0], j_fix);
                Jump.Join(s_addparam[0], s_fillop[0], j_close_sq_br);
                Jump.Join(s_fillop[0], s_addparam[1], j_fix);
                Jump.Join(s_addparam[1], s_operator_arr_fill, j_open_sq_br);
                Jump.Join(s_addparam[1], s_operator_arr_fe, j_array);
                Jump.Join(s_addparam[1], s_return, j_fix);

            s_move[0] = new StateMove();
            Jump.Join(s_operator_arr_fe, s_operator_arr_fe, j_array);
            Jump.Join(s_operator_arr_fe, s_move[0], j_fix);
            Jump.Join(s_move[0], s_return, j_fix);

            Jump.Join(s_addparam[0], s_err_1000, j_fix);
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>




            // >>>>>>>>>>>>>>>>>>>> subprogram {} <<<<<<<<<<<<<<<<<<<
            s_fillop[0] = new StateFillOperator();
            Jump.Join(s_initarray, s_subprog_arr, j_open_brace);
            Jump.Join(s_initarray, s_fillop[0], j_close_brace);
            Jump.Join(s_fillop[0], s_return, j_fix);
            s_subexp[0] = new StateSubexpression();
            Jump.Join(s_initarray, s_subexp[0], j_fix);
            //>>>>
            s_move[0] = new StateMove();
            s_addparam[0] = new StateAddParametr();
            Jump.Join(s_move[0], s_addparam[0], j_fix);
            Jump.Join(s_initarray, s_move[0], j_false);
            //<<<<

            Jump.Join(s_subexp[0], s_addparam[0], j_fix);
            s_move[1] = new StateMove();
            Jump.Join(s_addparam[0], s_move[1], j_comma);
            Jump.Join(s_addparam[0], s_fillop[0], j_close_brace);
            Jump.Join(s_fillop[0], s_return, j_fix);
            Jump.Join(s_move[1], s_subprog_arr, j_open_brace);
            Jump.Join(s_move[1], s_subexp[0], j_fix);
            //>>>>
            s_move[0] = new StateMove();
            Jump.Join(s_move[0], s_addparam[0], j_fix);
            Jump.Join(s_move[1], s_move[0], j_false);
            //<<<<

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            
            return first;
        }
    }
}
