using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Automation;
using Biendich.Utility;
using Biendich.Errors;
using Biendich.Language;

namespace Biendich.Lexical
{
	public class Scanner : Automaton
	{
		private class StateCreate : AState
		{
	        private int id;
	        public StateCreate(int id)
	        {
	            this.id = id;
	        }
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
			{
	            throw new Exception("");
			}

            public override Object GetObject(IChain chain, ref Dictionary<string, Object> param)
			{
	            StringBuilder str = new StringBuilder();
	            IInstruction[] sub = ((ByteChain)chain).SubChain(
	                (int)param["c"], chain.Cursor);
	
	            foreach (IInstruction i in sub)
		        {
	                str.Append(i.ToString());
		        }
                Token tok = new Token(id, (int)param["c"], str.ToString());
	            return tok;
			}
	        public override bool Motion()
	        {
	            return false;
	        }
	    }
		private class StateMove : AState
		{
            public override AState Advance(IChain chain, ref Dictionary<string, Object> param)
			{
	            if (chain.EOF()) return null;
	            foreach (Jump j in jumps)
		        {
                    if (j.Accept(chain.Current, ref param))
	                {
	                    chain.Move(j.Shift);
	                    return j.State;
	                }
		        }
	            throw new Exception("");
	            //return null;
			}

            public override Object GetObject(IChain chain, ref Dictionary<string, Object> param)
			{
	            throw new Exception("");
			}
	        public override bool Motion()
	        {
	            return true;
	        }
	    }
		
        private Lang lang;
        private int[] tokignor;
		private List<Error> errs;

        public Error[] Errors()
		{
            return errs.ToArray();
		}

        private static AState init(string path)
        {
            Dictionary<int, AState> states = new Dictionary<int, AState>();
            //Dictionary<int, Jump> jumps = new Dictionary<int, Jump>();
            
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(path);

            foreach (XmlNode node in xmldoc["tree"]["states"].ChildNodes)
            {
                switch (node.Name)
                {
                    case "s":
                        states.Add(int.Parse(node.Attributes["id"].Value), 
                            new StateMove());
                        break;
                    case "c":
                        states.Add(int.Parse(node.Attributes["id"].Value),
                            new StateCreate(
                            int.Parse(node.Attributes["tok"].Value)));
                        break;
                }
            }
            foreach (XmlNode node in xmldoc["tree"]["jumps"].ChildNodes)
            {
                AState s1, s2;
                if (!states.TryGetValue(
                    int.Parse(node.Attributes["s"].Value), out s1))
                    throw new Exception("");
                if (!states.TryGetValue(
                    int.Parse(node.Attributes["f"].Value), out s2))
                    throw new Exception("");
                IInstruction inst = new CharInstruction(
                    Utils.Decode(node.Attributes["i"].Value), 0);
                ICondition cond;
                if (node.Attributes["c"].Value == "=")
                    cond = new ConditEquals();
                else
                    cond = new ConditTrue();
                Jump jump = new Jump(inst, cond, 
                    int.Parse(node.Attributes["sh"].Value));
                Jump.Join(s1, s2, jump);
            }
            AState first;
            states.TryGetValue(0, out first);
            return first;
        }

		public Scanner(AState first)
            :base(first)
		{
            errs = new List<Error>();
		}
        public Scanner(string auto, Lang lang, int[] tokignor)
            : this(init(auto))
        {
            this.lang = lang;
            this.tokignor = tokignor;
        }
        public Token Run(IChain chain)
        {
            Token tok;
            Dictionary<string, Object> param;
            do {
                param = new Dictionary<string, Object>();
                param.Add("c", chain.Cursor);
                tok = (Token)base.Run(chain, ref param);
                if (tok == null) break;
                if (lang.TokenClass(tok.Id) == "error")
                    errs.Add(new Error());
            } while (Array.Exists(tokignor, 
                m => (tok.Id == m)));
            return tok;
        }
	}
}
