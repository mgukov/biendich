using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Automation;
using Biendich.Utility;

namespace Biendich.Lexical
{
	public class Token : IInstruction
	{
        public static IInstruction[] Create(int[] id)
        {
            IInstruction[] toks = new IInstruction[id.Length];
            for (int i = 0; i < id.Length; ++i)
            {
                toks[i] = new Token(id[i]);
            }
            return toks;
        }
		private int id;
		private int pos;
		private string value;

		public int Position
		{
            get { return pos; }
		}
        public int Id
		{
            get { return id; }
		}
		public Object Value
		{
            get { return value; }
		}
        public Token(int id)
        {
            this.id = id;
        }
        public Token(int id, int pos, string value)
		{
            this.id = id;
            this.pos = pos;
            this.value = value;
        }
        public virtual bool Equals(IInstruction i)
        {
            if (i == null) return false;
            return i.Id == this.Id;
        }
        public virtual IInstruction Clone()
        {
            Token t = new Token(id, pos, value);
            return t;
        }
    }
}
