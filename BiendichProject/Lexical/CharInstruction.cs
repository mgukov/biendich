using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Automation;

namespace Biendich.Lexical
{
    public class CharInstruction : IInstruction
    {

        private char ch;
        private int position;

        public int Id
        {
            get { return Convert.ToInt32(this.ch); }
        }

        public int Position
        {
            get { return position; }
        }

        public Object Value
        {
            get { return ch; }
        }

        public CharInstruction(char ch, int position)
        {
            this.ch = ch;
            this.position = position;
        }

        public bool Equals(IInstruction i)
        {
            return (ch == ((CharInstruction)i).ch);
        }

        public override string ToString()
        {
            return ch.ToString();
        }

        public IInstruction Clone()
        {
            return new CharInstruction(ch, position);
        }


        //public static implicit operator CharInstruction(char ch)
        //{
        //    return new CharInstruction(ch, 0);
        //}
    }
}
