using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Biendich.Automation;

namespace Biendich.Lexical
{
	public class ByteChain : IChain
	{
		private int cursor;
		private List<CharInstruction> instructions;
        private StreamReader reader;

        public ByteChain(string path)
        {
            cursor = 0;
            instructions = new List<CharInstruction>();
            reader = new StreamReader(path);
            Reserve(1);
        }

		public int Cursor
		{
            get { return cursor; }
            set { this.Move(value - cursor); }
		}

		public IInstruction Current
		{
            get { return !EOF() ? instructions[cursor] : null; }
		}

        public int Reserved
        {
            get { return instructions.Count; }
        }

        public IInstruction Deviation(int sh)
        {
            if (sh > 0) Reserve(sh + cursor);
            return (cursor + sh >= 0 &&
                cursor + sh < Reserved) ? instructions[cursor + sh] : null;
        }
		public bool EOF()
		{
            return (cursor >= Reserved && ReserveMore(1) == 0);
		}

		public void Move(int sh)
		{
            Reserve(sh + cursor);
            cursor += sh;
		}

        public bool TryMove(int sh)
        {
            if (sh + cursor == Reserve(sh + cursor))
            {
                cursor += sh;
                return true;
            }
            return false;
        }

        public int ReserveMore(int sh)
        {
            int i = 0;
            while (!reader.EndOfStream && i < sh)
            {
                instructions.Add(new CharInstruction((char)reader.Read(), i));
                ++i;
            }
            return i;
        }
        public int Reserve(int len)
        {
            ReserveMore(len - Reserved);
            return Reserved;
        }
		public IInstruction[] SubChain(int bp, int p)
		{
            int size = p - bp;
            CharInstruction[] sub = new CharInstruction[size];
            for (int i = 0; i < size; i++)
			{
			    sub[i] = instructions[bp + i];
			}
            return sub;
		}
        public void Close()
        {
            reader.Close();
        }

        ~ByteChain() 
        {
            reader.Close();
        }
    }
}
