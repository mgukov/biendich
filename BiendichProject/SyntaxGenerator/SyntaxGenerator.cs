﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using Biendich.Utility;
using Biendich.Language;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Generation;

namespace Biendich.Generation.Syntax
{
    public class SyntaxGenerator : AGenerator
    {

        private class StateMove : GState
        {
            public StateMove()
                :base("move")
            {
            }
        }

        private class StateOperand : GState
        {
            public StateOperand(string id)
                : base("oprn")
            {
                AddAttribute("iid", id);
            }
            public static StateOperand[] Create(string[] ids)
            {
                StateOperand[] operands = new StateOperand[ids.Length];
                for (int i = 0; i < ids.Length; ++i)
                {
                    operands[i] = new StateOperand(ids[i]);
                }
                return operands;
            }
        }

        private class StateOperation : GState
        {
            public StateOperation(string id)
                :base("oprt")
            {
                AddAttribute("iid", id);
            }
        }

        private class StateStatement : GState
        {
            public StateStatement(string id, string sh = "-1")
                :base("stmt")
            {
                AddAttribute("iid", id);
                AddAttribute("sh", sh);
            }

        }

        private class StateAttribute : GState
        {
            public StateAttribute(string id, string sh = "-1")
                :base("attr")
            {
                AddAttribute("iid", id);
                AddAttribute("sh", sh);
            }

        }

        private class StateInsert : GState
        {
            public StateInsert()
                :base("insr")
            {
            }

        }

        private class StateDeep : GState
        {
            public StateDeep()
                :base("deep")
            {
            }

        }

        private class StateRise : GState
        {
            public StateRise()
                :base("rise")
            {
            }

        }

        private class StateExpress : GState
        {
            public StateExpress()
                :base("expr")
            {
            }

        }

        private class StateRec : GState
        {
            public StateRec()
                :base("rec")
            {
            }

        }

        private class StateInsertAttributes : GState
        {
            public StateInsertAttributes()
                :base("inattr")
            {
            }

        }

        private class StateSubprogram : GState
        {
            public StateSubprogram(string pflags = "GF")
                : base("subp")
            {
                AddAttribute("flags", pflags);
            }
        }

        private class StateReturn : GState
        {
            public StateReturn()
                :base("ret")
            {
            }

        }

        private class StateFinal : GState
        {
            public StateFinal()
                :base("fin")
            {
            }

        }


        private class StatePush : GState
        {
            public StatePush(string name = "S")
                : base("push")
            {
                AddAttribute("name", name);
            }
        }

        private class StatePop : GState
        {
            public StatePop(string name = "S")
                : base("pop")
            {
                AddAttribute("name", name);
            }
        }

        private class StateEnque : GState
        {
            public StateEnque(string name = "E")
                : base("enq")
            {
                AddAttribute("name", name);
            }
        }

        private class StateDeque : GState
        {
            public StateDeque(string name = "E")
                : base("deq")
            {
                AddAttribute("name", name);
            }

        }


        private class StateCreateCounter : GState
        {
            public StateCreateCounter(string name, string val = "0")
                : base("crcn")
            {
                AddAttribute("name", name);
                AddAttribute("val", val);
            }

        }

        private class StateCounter : GState
        {
            public StateCounter(string name)
                : base("cntr")
            {
                AddAttribute("name", name);
            }

        }

        private class StateRepeat : GState
        {
            public StateRepeat(string name)
                : base("rpt")
            {
                AddAttribute("name", name);
            }

        }


        private class StateSave : GState
        {
            public StateSave(string name)
                : base("save")
            {
                AddAttribute("name", name);
            }

        }

        private class StateResume : GState
        {
            public StateResume(string name)
                : base("resm")
            {
                AddAttribute("name", name);
            }

        }

        private class StatePoint : GState
        {
            public StatePoint()
                :base("point")
            {
            }
        }

        private class StateBackoff : GState
        {
            public StateBackoff()
                :base("back")
            {
            }
        }


        private class StateSetFlag : GState
        {
            public StateSetFlag(string names)
                : base("setf")
            {
                AddAttribute("names", names.Replace(", ", ","));
            }
        }

        private class StateUnsetFlag : GState
        {
            public StateUnsetFlag(string names)
                : base("unsf")
            {
                AddAttribute("names", names.Replace(", ", ","));
            }
        }

        private class StateDumpFlag : GState
        {
            public StateDumpFlag(string names)
                : base("dumf")
            {
                AddAttribute("names", names.Replace(", ", ","));
            }
        }

        private class StateFlagOne : GState
        {
            public StateFlagOne(string names)
                : base("onef")
            {
                AddAttribute("names", names.Replace(", ", ","));
            }
        }

        private class StateFlagAll : GState
        {
            public StateFlagAll(string names)
                : base("allf")
            {
                AddAttribute("names", names.Replace(", ", ","));
            }
        }

        private class StateError : GState
        {
            public StateError(string id, string sh = "0")
                : base("err")
            {
                AddAttribute("id", id);
                AddAttribute("sh", sh);
            }
        }


        public override GState Generate(Lang lang)
        {
            Jump[] j = new Jump[10];
            AState[] s = new AState[10];

            
            // >>>>>>>>>>>>>>>>>>>>>>>>>> JUMPS <<<<<<<<<<<<<<<<<<<<<<<<<<<
            Jump
                jopen_round_br = new GJump(new Token(lang.TokenId("(")), 1),
                jclose_round_br = new GJump(new Token(lang.TokenId(")")), 1),
                jopen_brace = new GJump(new Token(lang.TokenId("{")), 1),
                jclose_brace = new GJump(new Token(lang.TokenId("}")), 1),
                jcomma = new GJump(new Token(lang.TokenId(",")), 1),
                jdot = new GJump(new Token(lang.TokenId(".")), 1),
                jmult = new GJump(new Token(lang.TokenId("*")), 1),
                jscolon = new GJump(new Token(lang.TokenId(";")), 1),
                jcolon = new GJump(new Token(lang.TokenId(":")), 1),

                jiden = new GJump(new Token(lang.TokenId("identif")), 1),
                jiden_fix = new GJump(new Token(lang.TokenId("identif")), 0),

                jpackage = new GJump(new Token(lang.TokenId("package")), 1),
                jimport = new GJump(new Token(lang.TokenId("import")), 1),
                jclass = new GJump(new Token(lang.TokenId("class")), 1),
                jinterface = new GJump(new Token(lang.TokenId("interface")), 1),
                jclass_back = new GJump(new Token(lang.TokenId("class")), -1),
                jinterface_back = new GJump(new Token(lang.TokenId("interface")), -1),
                jclass_fix = new GJump(new Token(lang.TokenId("class")), 0),
                jinterface_fix = new GJump(new Token(lang.TokenId("interface")), 0),

                jprivate = new GJump(new Token(lang.TokenId("private")), 1),
                jprotected = new GJump(new Token(lang.TokenId("protected")), 1),
                jpublic = new GJump(new Token(lang.TokenId("public")), 1),
                jprivate_fix = new GJump(new Token(lang.TokenId("private")), 0),
                jprotected_fix = new GJump(new Token(lang.TokenId("protected")), 0),
                jpublic_fix = new GJump(new Token(lang.TokenId("public")), 0),


                jfinal = new GJump(new Token(lang.TokenId("final")), 1),
                jabstract = new GJump(new Token(lang.TokenId("abstract")), 1),
                jextends = new GJump(new Token(lang.TokenId("extends")), 1),
                jimplements = new GJump(new Token(lang.TokenId("implements")), 1),

                jif = new GJump(new Token(lang.TokenId("if")), 1),
                jelse = new GJump(new Token(lang.TokenId("else")), 1),

                jswitch = new GJump(new Token(lang.TokenId("switch")), 1),
                jcase = new GJump(new Token(lang.TokenId("case")), 1),
                jdefault = new GJump(new Token(lang.TokenId("default")), 1),

                jfor = new GJump(new Token(lang.TokenId("for")), 1),
                jwhile = new GJump(new Token(lang.TokenId("while")), 1),
                jdo = new GJump(new Token(lang.TokenId("do")), 1),

                jtry = new GJump(new Token(lang.TokenId("try")), 1),
                jcatch = new GJump(new Token(lang.TokenId("catch")), 1),
                jreturn = new GJump(new Token(lang.TokenId("return")), 1),
                jbreak = new GJump(new Token(lang.TokenId("break")), 1),
                jcontinue = new GJump(new Token(lang.TokenId("continue")), 1),
                jthrow = new GJump(new Token(lang.TokenId("throw")), 1),
                jthrows = new GJump(new Token(lang.TokenId("throws")), 1),
                jnative = new GJump(new Token(lang.TokenId("native")), 1),
                jstatic = new GJump(new Token(lang.TokenId("static")), 1),
                jvoid = new GJump(new Token(lang.TokenId("void")), 1),
                jvolatile = new GJump(new Token(lang.TokenId("volatile")), 1),

                jarray = new GJump(new Token(lang.TokenId("[]")), 1),
                jarray_fix = new GJump(new Token(lang.TokenId("[]")), 0),


                jfalse = new GJump(new Token(0), new ConditFalse(), 0),
                jtrue = new GJump(new Token(0), new ConditTrue(), 0),
                jret = new GJump(new Token(0), new ConditFalse(), 0),
                jfix = new GJump(new Token(0), new ConditTrue(), 0),
                jfw = new GJump(new Token(0), new ConditTrue(), 1),
                jback = new GJump(new Token(0), new ConditTrue(), -1),

                jequal = new GJump(new Token(lang.TokenId("=")), 1),
                jfinish = new GJump(new Token(-1), 0);



            List<Jump> jtype = new List<Jump>();
            jtype.AddRange(
                GJump.Create(Token.Create(lang.TokenId(new string[]
            {
                "int", "short", "byte", "long", "float", "double", "char", "boolean"
            })), new ConditEquals(), 1));

            Jump jsynchronized = new GJump(new Token(1159), new ConditEquals(), 1);
            Jump jtransient = new GJump(new Token(1173), new ConditEquals(), 1);
            Jump jfinally = new GJump(new Token(1084), new ConditEquals(), 1);
            Jump jint = new GJump(new Token(1102), new ConditEquals(), 1);
            Jump jlong = new GJump(new Token(1110), new ConditEquals(), 1);
            Jump jbyte = new GJump(new Token(1024), new ConditEquals(), 1);
            Jump jshort = new GJump(new Token(1150), new ConditEquals(), 1);
            Jump jchar = new GJump(new Token(1034), new ConditEquals(), 1);
            Jump jfloat = new GJump(new Token(1086), new ConditEquals(), 1);
            Jump jdouble = new GJump(new Token(1054), new ConditEquals(), 1);
            Jump jboolean = new GJump(new Token(1020), new ConditEquals(), 1);
            //Jump jvoid = new GJump(new Token(1180), new ConditEquals(), 1);
            Jump jint_fix = new GJump(new Token(1102), new ConditEquals(), 0);
            Jump jlong_fix = new GJump(new Token(1110), new ConditEquals(), 0);
            Jump jbyte_fix = new GJump(new Token(1024), new ConditEquals(), 0);
            Jump jshort_fix = new GJump(new Token(1150), new ConditEquals(), 0);
            Jump jchar_fix = new GJump(new Token(1034), new ConditEquals(), 0);
            Jump jfloat_fix = new GJump(new Token(1086), new ConditEquals(), 0);
            Jump jdouble_fix = new GJump(new Token(1054), new ConditEquals(), 0);
            Jump jboolean_fix = new GJump(new Token(1020), new ConditEquals(), 0);
            Jump jvoid_fix = new GJump(new Token(1180), new ConditEquals(), 0);
            Jump jcatch_fix = new GJump(new Token(1032), new ConditEquals(), 0);
            Jump jfinally_fix = new GJump(new Token(1084), new ConditEquals(), 0);
            Jump jfinal_fix = new GJump(new Token(1082), new ConditEquals(), 0);

            StateMove first = new StateMove();

            StateMove first_syn = new StateMove();
            StateMove first_stmt = new StateMove();
            StateMove first_init = new StateMove();
            StateMove first_while = new StateMove();
            StateMove first_if = new StateMove();
            StateMove first_for = new StateMove();
            StateMove first_param = new StateMove();
            StateMove first_try = new StateMove();
            StateMove first_case = new StateMove();
            StateMove first_do = new StateMove();
            StateMove first_type = new StateMove();
            StateMove first_iden_type = new StateMove();
            StateMove first_method = new StateMove();
            StateMove first_construct = new StateMove();
            StateMove first_static = new StateMove();
            StateMove first_class = new StateMove();
            StateMove first_field = new StateMove();
            StateMove first_interface = new StateMove();

            StateSubprogram call = new StateSubprogram();

            StateDeep s_deep = new StateDeep();
            StateFinal final = new StateFinal();


            //StateOperand op = new StateOperand("1000");
            //StateInsert ins = new StateInsert();

            //Jump.Join(call, first_method, jfix);
            //Jump.Join(call, s_deep, jfalse);

            //Jump.Join(first, call, jfix);
            //Jump.Join(s_deep, final, jfw);
            //Jump.Join(op, ins, jfw);
            //Jump.Join(ins, final, jfw);

            Jump.Join(first, first_syn, jfix);

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //StateSubprogram call_stmt;



            // >>>>>>>>>>>>>>>>>>>>>> SYNTAX <<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_package = new StateStatement("1160");
            StateDeep deep_package = new StateDeep();
            StateInsert in_pack_name = new StateInsert();
            StateStatement stmt_import = new StateStatement("1120");
            StateDeep deep_import = new StateDeep();
            StateOperand oprn_imp_iden1 = new StateOperand("1000");
            StateSave save_imp_iden1 = new StateSave("IMP");
            StateInsert in_imp_type = new StateInsert();
            StateRise rise_import = new StateRise();
            StateOperation oprt_imp_dot = new StateOperation("1040");
            StateMove first_imp_type = new StateMove();
            StateDeep deep_imp_type = new StateDeep();
            StateReturn return_imp_type = new StateReturn();
            StateDeep deep_imp_dot = new StateDeep();
            StateOperation oprt_imp_mult = new StateOperation("1310");
            StateResume rsm_imp_iden1 = new StateResume("IMP");
            StateInsert in_imp_iden1 = new StateInsert();
            StateSubprogram call_imp_type = new StateSubprogram();
            StateInsert in_imp_iden2 = new StateInsert();
            StateSubprogram call_syn_imp_type = new StateSubprogram();
            StatePoint point_syn = new StatePoint();
            StateMove mv_syn = new StateMove();
            StateSubprogram call_syn_class = new StateSubprogram();
            StateInsert in_syn_class = new StateInsert();
            StateBackoff back_syn_class = new StateBackoff();
            StateBackoff back_syn_import = new StateBackoff();
            StateSubprogram call_syn_interface = new StateSubprogram();
            StateInsert in_syn_interface = new StateInsert();
            StateFinal final_syn = new StateFinal();
            StateAttribute attr_pack_name = new StateAttribute("1000");
            StateDeep deep_pack_name = new StateDeep();
            StateRise rise_pack_name = new StateRise();
            StateSubprogram call_iden_type_pack = new StateSubprogram("GF");

            Jump.Join(first_syn, stmt_package, jpackage);
            Jump.Join(stmt_package, deep_package, jfix);
            Jump.Join(stmt_import, deep_import, jfix);
            Jump.Join(in_imp_type, rise_import, jfix);
            Jump.Join(oprn_imp_iden1, save_imp_iden1, jdot);
            Jump.Join(save_imp_iden1, oprt_imp_dot, jfix);
            Jump.Join(first_imp_type, oprn_imp_iden1, jiden);
            Jump.Join(oprn_imp_iden1, deep_imp_type, jscolon);
            Jump.Join(deep_imp_type, return_imp_type, jfix);
            Jump.Join(oprt_imp_dot, deep_imp_dot, jfix);
            Jump.Join(first_imp_type, oprt_imp_mult, jmult);
            Jump.Join(oprt_imp_mult, deep_imp_type, jscolon);
            Jump.Join(deep_imp_dot, rsm_imp_iden1, jfix);
            Jump.Join(rsm_imp_iden1, in_imp_iden1, jfix);
            Jump.Join(in_imp_iden1, call_imp_type, jfix);
            Jump.Join(call_imp_type, first_imp_type, jfix);
            Jump.Join(call_imp_type, in_imp_iden2, jfix);
            Jump.Join(in_imp_iden2, return_imp_type, jfix);
            Jump.Join(deep_import, call_syn_imp_type, jiden_fix);
            Jump.Join(call_syn_imp_type, first_imp_type, jfix);
            Jump.Join(call_syn_imp_type, in_imp_type, jret);
            Jump.Join(rise_import, stmt_import, jimport);
            Jump.Join(rise_import, point_syn, jfix);
            Jump.Join(point_syn, final_syn, jfinish);
            Jump.Join(point_syn, mv_syn, jfix);
            Jump.Join(mv_syn, mv_syn, jpublic);
            Jump.Join(mv_syn, mv_syn, jabstract);
            Jump.Join(mv_syn, mv_syn, jfinal);
            Jump.Join(mv_syn, back_syn_class, jclass);
            Jump.Join(back_syn_class, call_syn_class, jfix);
            Jump.Join(call_syn_class, first_class, jfix);
            Jump.Join(call_syn_class, in_syn_class, jret);
            Jump.Join(in_syn_class, point_syn, jfix);
            Jump.Join(mv_syn, back_syn_import, jinterface);
            Jump.Join(back_syn_import, call_syn_interface, jfix);
            Jump.Join(call_syn_interface, first_interface, jfix);
            Jump.Join(call_syn_interface, in_syn_interface, jret);
            Jump.Join(in_syn_interface, point_syn, jfix);
            Jump.Join(deep_package, attr_pack_name, jfix);
            Jump.Join(attr_pack_name, deep_pack_name, jfix);
            Jump.Join(in_pack_name, rise_pack_name, jscolon);
            Jump.Join(deep_pack_name, call_iden_type_pack, jiden_fix);
            Jump.Join(call_iden_type_pack, first_iden_type, jfix);
            Jump.Join(call_iden_type_pack, in_pack_name, jfix);
            Jump.Join(rise_pack_name, stmt_import, jimport);
            Jump.Join(rise_pack_name, point_syn, jfix);
            Jump.Join(first_syn, point_syn, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>> INIT EXP <<<<<<<<<<<<<<<<<<<<<<
            StatePoint point_init = new StatePoint();
            StateBackoff back_exp_init = new StateBackoff();
            StateStatement stmt_init = new StateStatement("1130");
            StateOperand oprn_name_init = new StateOperand("1000");
            StateInsert in_type_init = new StateInsert();
            StateDeep dp_name_init = new StateDeep();
            StateDeep dp_init = new StateDeep();
            StateReturn return_init = new StateReturn();
            StateExpress exp_init = new StateExpress();
            StateDeep dp_exp_init = new StateDeep();
            StateExpress exp_val_init = new StateExpress();
            StateInsert in_val_init = new StateInsert();
            StateRise rise_name_init = new StateRise();
            StateSubprogram call_type_init = new StateSubprogram("GF");
            StateMove mv_iden_init = new StateMove();
            StateMove mv_dot_init = new StateMove();
            StateBackoff back_type_init = new StateBackoff();
            StateMove mv_type_init = new StateMove();
            StateAttribute attr_fin_init = new StateAttribute("1040", " -1");
            StateInsert in_fin_init = new StateInsert();

            Jump.Join(stmt_init, dp_init, jfix);
            Jump.Join(dp_name_init, rise_name_init, jcomma);
            Jump.Join(dp_name_init, exp_val_init, jequal);
            Jump.Join(dp_name_init, return_init, jfix);
            Jump.Join(back_exp_init, exp_init, jfix);
            Jump.Join(exp_init, dp_exp_init, jfix);
            Jump.Join(dp_exp_init, return_init, jfix);
            Jump.Join(exp_val_init, in_val_init, jfix);
            Jump.Join(in_val_init, rise_name_init, jcomma);
            Jump.Join(in_val_init, return_init, jfix);
            Jump.Join(in_type_init, oprn_name_init, jiden);
            Jump.Join(oprn_name_init, dp_name_init, jfix);
            Jump.Join(rise_name_init, oprn_name_init, jiden);
            Jump.Join(point_init, mv_type_init, jint);
            Jump.Join(point_init, mv_type_init, jlong);
            Jump.Join(point_init, mv_type_init, jbyte);
            Jump.Join(point_init, mv_type_init, jshort);
            Jump.Join(point_init, mv_type_init, jchar);
            Jump.Join(point_init, mv_type_init, jfloat);
            Jump.Join(point_init, mv_type_init, jdouble);
            Jump.Join(point_init, mv_type_init, jboolean);
            Jump.Join(call_type_init, first_type, jfix);
            Jump.Join(point_init, mv_iden_init, jiden);
            Jump.Join(point_init, exp_init, jfix);
            Jump.Join(mv_iden_init, mv_dot_init, jdot);
            Jump.Join(mv_dot_init, mv_iden_init, jiden);
            Jump.Join(mv_iden_init, back_type_init, jiden_fix);
            Jump.Join(mv_type_init, back_type_init, jiden_fix);
            Jump.Join(mv_type_init, mv_type_init, jarray);
            Jump.Join(mv_iden_init, mv_iden_init, jarray);
            Jump.Join(mv_iden_init, back_exp_init, jfix);
            Jump.Join(mv_type_init, back_exp_init, jfix);
            Jump.Join(attr_fin_init, in_fin_init, jfix);
            Jump.Join(back_type_init, stmt_init, jfix);
            Jump.Join(dp_init, attr_fin_init, jfinal);
            Jump.Join(dp_init, call_type_init, jfix);
            Jump.Join(in_fin_init, call_type_init, jfix);
            Jump.Join(call_type_init, in_type_init, jret);
            Jump.Join(first_init, stmt_init, jfinal_fix);
            Jump.Join(first_init, point_init, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>>>> IF <<<<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_if = new StateStatement("1110");
            StateStatement stmt_if_cond = new StateStatement("1050");
            StateExpress exp_if = new StateExpress();
            StateDeep dp_if = new StateDeep();
            StateDeep dp_if_cond = new StateDeep();
            StateInsert in_exp_if = new StateInsert();
            StateSubprogram call_stmt_if = new StateSubprogram();
            StateInsert in_body_true = new StateInsert();
            StateInsert in_body_false = new StateInsert();
            StateReturn return_if = new StateReturn();
            StateSubprogram call_stmt_else = new StateSubprogram();
            StateRise rise_cond_if = new StateRise();

            Jump.Join(first_if, stmt_if, jfix);
            Jump.Join(stmt_if, dp_if, jfix);
            Jump.Join(dp_if, stmt_if_cond, jopen_round_br);
            Jump.Join(stmt_if_cond, dp_if_cond, jfix);
            Jump.Join(dp_if_cond, exp_if, jfix);
            Jump.Join(exp_if, in_exp_if, jclose_round_br);
            Jump.Join(call_stmt_if, first_stmt, jfix);
            Jump.Join(in_body_false, return_if, jfix);
            Jump.Join(call_stmt_if, in_body_true, jfalse);
            Jump.Join(in_body_true, call_stmt_else, jelse);
            Jump.Join(call_stmt_else, first_stmt, jfix);
            Jump.Join(call_stmt_else, in_body_false, jfalse);
            Jump.Join(in_body_true, return_if, jfix);
            Jump.Join(in_exp_if, rise_cond_if, jfix);
            Jump.Join(rise_cond_if, call_stmt_if, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> FOR <<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_for = new StateStatement("1100");
            StateDeep dp_for = new StateDeep();
            StateStatement stmt_cond_for = new StateStatement("1050");
            StateDeep dp_cond_for = new StateDeep();
            StateRise rise_cond_for = new StateRise();
            StateSubprogram call_stmt_for = new StateSubprogram();
            StateInsert in_stmt_for = new StateInsert();
            StateReturn return_for = new StateReturn();
            StateSetFlag sflag_for_break = new StateSetFlag("BREAK");
            StateUnsetFlag uflag_for = new StateUnsetFlag("BREAK, CONTINUE");
            StateStatement stmt_cond1 = new StateStatement("1050");
            StateDeep deep_cond1 = new StateDeep();
            StateRise rise_cond1 = new StateRise();
            StateSubprogram call_init_cond1 = new StateSubprogram();
            StateExpress exp_cond1 = new StateExpress();
            StateStatement stmt_cond2 = new StateStatement("1050");
            StateDeep deep_cond2 = new StateDeep();
            StateRise rise_cond2 = new StateRise();
            StateInsert in_cond1_exp = new StateInsert();
            StateExpress exp_cond2 = new StateExpress();
            StateInsert in_cond2_exp = new StateInsert();
            StateStatement stmt_cond3 = new StateStatement("1050");
            StateDeep deep_cond3 = new StateDeep();
            StateRise rise_cond3 = new StateRise();
            StateSubprogram call_init_cond3 = new StateSubprogram();
            StateInsert in_cond3_exp = new StateInsert();
            StateExpress exp_cond3 = new StateExpress();
            StateSetFlag sflag_for_continue = new StateSetFlag("CONTINUE");

            Jump.Join(stmt_for, dp_for, jfix);
            Jump.Join(dp_for, stmt_cond_for, jopen_round_br);
            Jump.Join(stmt_cond_for, dp_cond_for, jfix);
            Jump.Join(call_stmt_for, first_stmt, jfix);
            Jump.Join(call_stmt_for, in_stmt_for, jfalse);
            Jump.Join(in_stmt_for, uflag_for, jfix);
            Jump.Join(dp_cond_for, stmt_cond1, jfix);
            Jump.Join(stmt_cond1, deep_cond1, jfix);
            Jump.Join(deep_cond1, rise_cond1, jscolon);
            Jump.Join(deep_cond1, call_init_cond1, jfix);
            Jump.Join(stmt_cond2, deep_cond2, jfix);
            Jump.Join(deep_cond2, rise_cond2, jscolon);
            Jump.Join(call_init_cond1, first_init, jfix);
            Jump.Join(call_init_cond1, in_cond1_exp, jret);
            Jump.Join(in_cond1_exp, rise_cond1, jscolon);
            Jump.Join(in_cond1_exp, exp_cond1, jcomma);
            Jump.Join(exp_cond1, in_cond1_exp, jfix);
            Jump.Join(rise_cond1, stmt_cond2, jfix);
            Jump.Join(deep_cond2, exp_cond2, jfix);
            Jump.Join(exp_cond2, in_cond2_exp, jfix);
            Jump.Join(in_cond2_exp, rise_cond2, jscolon);
            Jump.Join(stmt_cond3, deep_cond3, jfix);
            Jump.Join(deep_cond3, rise_cond3, jclose_round_br);
            Jump.Join(call_init_cond3, first_init, jfix);
            Jump.Join(rise_cond2, stmt_cond3, jfix);
            Jump.Join(deep_cond3, call_init_cond3, jfix);
            Jump.Join(call_init_cond3, in_cond3_exp, jret);
            Jump.Join(in_cond3_exp, exp_cond3, jcomma);
            Jump.Join(exp_cond3, in_cond3_exp, jfix);
            Jump.Join(in_cond3_exp, rise_cond3, jclose_round_br);
            Jump.Join(rise_cond3, rise_cond_for, jfix);
            Jump.Join(first_for, sflag_for_break, jfix);
            Jump.Join(sflag_for_break, sflag_for_continue, jfix);
            Jump.Join(sflag_for_continue, stmt_for, jfix);
            Jump.Join(rise_cond_for, call_stmt_for, jfix);
            Jump.Join(uflag_for, return_for, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> WHILE <<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_while = new StateStatement("1230");
            StateDeep deep_while = new StateDeep();
            StateStatement stmt_cond_while = new StateStatement("1050");
            StateDeep deep_cond_while = new StateDeep();
            StateExpress exp_while = new StateExpress();
            StateInsert in_exp_while = new StateInsert();
            StateSubprogram call_stmt_while = new StateSubprogram();
            StateRise rise_cond_while = new StateRise();
            StateInsert in_stmt_while = new StateInsert();
            StateReturn return_while = new StateReturn();
            StateSetFlag sflag_while = new StateSetFlag("BREAK, CONTINUE");
            StateUnsetFlag uflag_while = new StateUnsetFlag("BREAK, CONTINUE");

            Jump.Join(stmt_while, deep_while, jfix);
            Jump.Join(deep_while, stmt_cond_while, jopen_round_br);
            Jump.Join(stmt_cond_while, deep_cond_while, jfix);
            Jump.Join(deep_cond_while, exp_while, jfix);
            Jump.Join(exp_while, in_exp_while, jfix);
            Jump.Join(call_stmt_while, first_stmt, jfix);
            Jump.Join(in_exp_while, rise_cond_while, jclose_round_br);
            Jump.Join(call_stmt_while, in_stmt_while, jfalse);
            Jump.Join(in_stmt_while, uflag_while, jfix);
            Jump.Join(first_while, sflag_while, jfix);
            Jump.Join(rise_cond_while, call_stmt_while, jfix);
            Jump.Join(sflag_while, stmt_while, jfix);
            Jump.Join(uflag_while, return_while, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> DO <<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_do = new StateStatement("1070");
            StateDeep deep_do = new StateDeep();
            StateSubprogram call_stmt_do = new StateSubprogram();
            StateInsert in_stmt_do = new StateInsert();
            StateStatement stmt_cond_do = new StateStatement("1050");
            StateExpress exp_do = new StateExpress();
            StateReturn return_do = new StateReturn();
            StateUnsetFlag uflag_do = new StateUnsetFlag("BREAK, CONTINUE");
            StateInsert in_exp_do = new StateInsert();
            StateDeep deep_cond_do = new StateDeep();
            StateSetFlag sflag_do = new StateSetFlag("BREAK, CONTINUE");

            Jump.Join(stmt_do, deep_do, jfix);
            Jump.Join(call_stmt_do, first_stmt, jfix);
            Jump.Join(call_stmt_do, in_stmt_do, jfalse);
            Jump.Join(in_stmt_do, stmt_cond_do, jwhile);
            Jump.Join(exp_do, in_exp_do, jclose_round_br);
            Jump.Join(in_exp_do, uflag_do, jfix);
            Jump.Join(stmt_cond_do, deep_cond_do, jopen_round_br);
            Jump.Join(deep_cond_do, exp_do, jfix);
            Jump.Join(deep_do, call_stmt_do, jfix);
            Jump.Join(first_do, sflag_do, jfix);
            Jump.Join(sflag_do, stmt_do, jfix);
            Jump.Join(uflag_do, return_do, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> STMT <<<<<<<<<<<<<<<<<<<<<<<
            StateSubprogram call_if = new StateSubprogram();
            StateSubprogram call_stmt = new StateSubprogram();
            StateStatement stmt_empty = new StateStatement("1080");
            StateDeep dp_stmt = new StateDeep();
            StateReturn return_stmt = new StateReturn();
            StateStatement stmt_body = new StateStatement("1000");
            StateDeep dp_body_stmt = new StateDeep();
            StateInsert in_stmt = new StateInsert();
            StateDeep dp_empty = new StateDeep();
            StateSubprogram call_for = new StateSubprogram();
            StateDeep dp_for_stmt = new StateDeep();
            StateSubprogram call_while = new StateSubprogram();
            StateDeep dp_while_stmt = new StateDeep();
            StateSubprogram call_init = new StateSubprogram();
            StateDeep dp_init_stmt = new StateDeep();
            StateSubprogram call_case = new StateSubprogram();
            StateStatement stmt_return = new StateStatement("1190");
            StateSubprogram call_try = new StateSubprogram();
            StateDeep deep_try_stmt = new StateDeep();
            StateSubprogram call_do = new StateSubprogram();
            StateDeep deep_do_stmt = new StateDeep();
            StateDeep deep_return_stmt = new StateDeep();
            StateExpress exp_return = new StateExpress();
            StateInsert in_exp_ret = new StateInsert();
            StateDeep deep_case_stmt = new StateDeep();
            StateStatement stmt_break = new StateStatement("1010");
            StateFlagOne onef_break_stmt = new StateFlagOne("BREAK");
            StateMove err_break = new StateMove();
            StateDeep deep_break = new StateDeep();
            StateMove err_return = new StateMove();
            StateFlagOne onef_continue_stmt = new StateFlagOne("CONTINUE");
            StateStatement stmt_continue = new StateStatement("1051");
            StateDeep deep_continue = new StateDeep();
            StateMove err_continue = new StateMove();
            StateStatement stmt_throw = new StateStatement("1210");
            StateDeep deep_throw = new StateDeep();
            StateExpress exp_throw = new StateExpress();
            StateInsert in_throw = new StateInsert();
            StateFlagOne onef_void = new StateFlagOne("VOID");
            StateSubprogram call_class = new StateSubprogram();
            StateDeep deep_class_stmt = new StateDeep();
            StateSubprogram call_interface = new StateSubprogram();
            StateDeep deep_interface_stmt = new StateDeep();
            StateMove mv_abstract = new StateMove();
            StateMove mv_final = new StateMove();

            Jump.Join(first_stmt, call_if, jif);
            Jump.Join(first_stmt, stmt_empty, jscolon);
            Jump.Join(call_if, first_if, jfix);
            Jump.Join(first_stmt, stmt_body, jopen_brace);
            Jump.Join(stmt_body, dp_body_stmt, jfix);
            Jump.Join(dp_body_stmt, return_stmt, jclose_brace);
            Jump.Join(dp_body_stmt, call_stmt, jfix);
            Jump.Join(call_stmt, first_stmt, jfix);
            Jump.Join(call_stmt, in_stmt, jfalse);
            Jump.Join(in_stmt, return_stmt, jclose_brace);
            Jump.Join(in_stmt, call_stmt, jfix);
            Jump.Join(call_if, dp_stmt, jfalse);
            Jump.Join(dp_stmt, return_stmt, jfix);
            Jump.Join(stmt_empty, dp_empty, jfix);
            Jump.Join(dp_empty, return_stmt, jfix);
            Jump.Join(first_stmt, call_for, jfor);
            Jump.Join(call_for, first_for, jfix);
            Jump.Join(call_for, dp_for_stmt, jfalse);
            Jump.Join(dp_for_stmt, return_stmt, jfix);
            Jump.Join(first_stmt, call_while, jwhile);
            Jump.Join(call_while, first_while, jfix);
            Jump.Join(call_while, dp_while_stmt, jfalse);
            Jump.Join(dp_while_stmt, return_stmt, jfix);
            Jump.Join(call_init, first_init, jfix);
            Jump.Join(call_init, dp_init_stmt, jfalse);
            Jump.Join(dp_init_stmt, return_stmt, jscolon);
            Jump.Join(first_stmt, call_case, jswitch);
            Jump.Join(first_stmt, call_try, jtry);
            Jump.Join(call_try, first_try, jfix);
            Jump.Join(call_try, deep_try_stmt, jret);
            Jump.Join(deep_try_stmt, return_stmt, jfix);
            Jump.Join(first_stmt, call_do, jdo);
            Jump.Join(call_do, first_do, jfix);
            Jump.Join(call_do, deep_do_stmt, jret);
            Jump.Join(deep_do_stmt, return_stmt, jfix);
            Jump.Join(stmt_return, deep_return_stmt, jfix);
            Jump.Join(deep_return_stmt, return_stmt, jscolon);
            Jump.Join(exp_return, in_exp_ret, jfix);
            Jump.Join(in_exp_ret, return_stmt, jscolon);
            Jump.Join(call_case, first_case, jfix);
            Jump.Join(call_case, deep_case_stmt, jret);
            Jump.Join(deep_case_stmt, return_stmt, jfix);
            Jump.Join(first_stmt, onef_break_stmt, jbreak);
            Jump.Join(onef_break_stmt, stmt_break, jtrue);
            Jump.Join(onef_break_stmt, err_break, jfalse);
            Jump.Join(stmt_break, deep_break, jfix);
            Jump.Join(deep_break, return_stmt, jscolon);
            Jump.Join(first_stmt, onef_continue_stmt, jcontinue);
            Jump.Join(onef_continue_stmt, stmt_continue, jtrue);
            Jump.Join(stmt_continue, deep_continue, jfix);
            Jump.Join(deep_continue, return_stmt, jscolon);
            Jump.Join(onef_continue_stmt, err_continue, jfalse);
            Jump.Join(first_stmt, stmt_return, jreturn);
            Jump.Join(stmt_throw, deep_throw, jfix);
            Jump.Join(deep_throw, exp_throw, jfix);
            Jump.Join(exp_throw, in_throw, jfix);
            Jump.Join(in_throw, return_stmt, jscolon);
            Jump.Join(onef_void, err_return, jtrue);
            Jump.Join(onef_void, exp_return, jfalse);
            Jump.Join(deep_return_stmt, onef_void, jfix);
            Jump.Join(first_stmt, call_class, jclass);
            Jump.Join(call_class, first_class, jfix);
            Jump.Join(call_class, deep_class_stmt, jret);
            Jump.Join(deep_class_stmt, return_stmt, jfix);
            Jump.Join(first_stmt, call_interface, jinterface);
            Jump.Join(call_interface, first_interface, jfix);
            Jump.Join(call_interface, deep_interface_stmt, jret);
            Jump.Join(deep_interface_stmt, return_stmt, jfix);
            Jump.Join(first_stmt, mv_abstract, jabstract);
            Jump.Join(mv_abstract, call_class, jclass_back);
            Jump.Join(mv_abstract, call_interface, jinterface_back);
            Jump.Join(first_stmt, mv_final, jfinal);
            Jump.Join(mv_final, call_class, jclass_back);
            Jump.Join(mv_final, call_init, jback);
            Jump.Join(first_stmt, stmt_throw, jthrow);
            Jump.Join(first_stmt, call_init, jfix);

            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> PARAM <<<<<<<<<<<<<<<<<<<<<<<
            StateAttribute attr_param = new StateAttribute("1080");
            StateDeep deep_param = new StateDeep();
            StateSubprogram call_type_param = new StateSubprogram();
            StateOperand oprn_name_param = new StateOperand("1000");
            StateInsert in_type_param = new StateInsert();
            StateInsert in_name_param = new StateInsert();
            StateReturn return_param = new StateReturn();
            StateAttribute attr_final_param = new StateAttribute("1000", " -1");
            StateInsert in_final_param = new StateInsert();

            Jump.Join(in_type_param, oprn_name_param, jiden);
            Jump.Join(oprn_name_param, in_name_param, jfix);
            Jump.Join(in_name_param, return_param, jfix);
            Jump.Join(first_param, attr_param, jfix);
            Jump.Join(call_type_param, first_type, jfix);
            Jump.Join(call_type_param, in_type_param, jret);
            Jump.Join(attr_param, deep_param, jfix);
            Jump.Join(deep_param, attr_final_param, jfinal);
            Jump.Join(attr_final_param, in_final_param, jfix);
            Jump.Join(in_final_param, call_type_param, jfix);
            Jump.Join(deep_param, call_type_param, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> TYPE <<<<<<<<<<<<<<<<<<<<<<<
            StateMove mv_oprn_type = new StateMove();
            StateEnque enq_type = new StateEnque("E");
            StateOperation oprt_arr_type = new StateOperation("1020");
            StateDeep deep_arr_type = new StateDeep();
            StateDeque deq_arr_iden_type = new StateDeque("E");
            StateInsert in_arr_iden_type = new StateInsert();
            StateRise rise_iden_type = new StateRise();
            StateEnque enq_arr_iden_type = new StateEnque("E");
            StateReturn return_type = new StateReturn();
            StateDeque deq_type = new StateDeque("E");
            StateDeep deep_type = new StateDeep();
            StateSubprogram call_iden_type_type = new StateSubprogram("GF");
            StateOperand oprn_type_long = new StateOperand("1048");
            StateOperand oprn_type_byte = new StateOperand("1044");
            StateOperand oprn_type_short = new StateOperand("1040");
            StateOperand oprn_type_char = new StateOperand("1060");
            StateOperand oprn_type_float = new StateOperand("1052");
            StateOperand oprn_type_double = new StateOperand("1056");
            StateOperand oprn_type_bool = new StateOperand("1064");
            StateOperand oprn_type_int = new StateOperand("1036");

            Jump.Join(mv_oprn_type, enq_type, jarray_fix);
            Jump.Join(enq_type, oprt_arr_type, jarray);
            Jump.Join(oprt_arr_type, deep_arr_type, jfix);
            Jump.Join(deep_arr_type, deq_arr_iden_type, jfix);
            Jump.Join(deq_arr_iden_type, in_arr_iden_type, jfix);
            Jump.Join(in_arr_iden_type, rise_iden_type, jfix);
            Jump.Join(rise_iden_type, enq_arr_iden_type, jfix);
            Jump.Join(enq_arr_iden_type, oprt_arr_type, jarray);
            Jump.Join(enq_type, deq_type, jiden_fix);
            Jump.Join(enq_arr_iden_type, deq_type, jfix);
            Jump.Join(deq_type, deep_type, jfix);
            Jump.Join(deep_type, return_type, jfix);
            Jump.Join(first_type, oprn_type_int, jint);
            Jump.Join(first_type, oprn_type_long, jlong);
            Jump.Join(first_type, oprn_type_byte, jbyte);
            Jump.Join(first_type, oprn_type_short, jshort);
            Jump.Join(first_type, oprn_type_char, jchar);
            Jump.Join(first_type, oprn_type_float, jfloat);
            Jump.Join(first_type, oprn_type_double, jdouble);
            Jump.Join(first_type, oprn_type_bool, jboolean);
            Jump.Join(first_type, call_iden_type_type, jiden_fix);
            Jump.Join(call_iden_type_type, first_iden_type, jfix);
            Jump.Join(call_iden_type_type, enq_type, jret);
            Jump.Join(mv_oprn_type, deep_type, jiden_fix);
            Jump.Join(oprn_type_float, mv_oprn_type, jfix);
            Jump.Join(oprn_type_byte, mv_oprn_type, jfix);
            Jump.Join(oprn_type_long, mv_oprn_type, jfix);
            Jump.Join(oprn_type_bool, mv_oprn_type, jfix);
            Jump.Join(oprn_type_short, mv_oprn_type, jfix);
            Jump.Join(oprn_type_char, mv_oprn_type, jfix);
            Jump.Join(oprn_type_int, mv_oprn_type, jfix);
            Jump.Join(oprn_type_double, mv_oprn_type, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> IDEN_TYPE <<<<<<<<<<<<<<<<<<<<<<<
            StateOperand oprn_iden_itp = new StateOperand("1000");
            StateEnque enq_iden_itp = new StateEnque("E");
            StateSubprogram call_iden_type = new StateSubprogram("GF");
            StateOperation oprt_dot_itp = new StateOperation("1040");
            StateDeep deep_dot_itp = new StateDeep();
            StateDeque deq_iden_itp = new StateDeque("E");
            StateInsert in_iden1_itp = new StateInsert();
            StateInsert in_ide2_itp = new StateInsert();
            StateReturn return_iden_type = new StateReturn();
            StateDeep deep_iden_ity = new StateDeep();

            Jump.Join(first_iden_type, oprn_iden_itp, jiden);
            Jump.Join(oprn_iden_itp, enq_iden_itp, jdot);
            Jump.Join(enq_iden_itp, oprt_dot_itp, jfix);
            Jump.Join(oprt_dot_itp, deep_dot_itp, jfix);
            Jump.Join(deep_dot_itp, deq_iden_itp, jfix);
            Jump.Join(deq_iden_itp, in_iden1_itp, jfix);
            Jump.Join(in_iden1_itp, call_iden_type, jiden_fix);
            Jump.Join(call_iden_type, first_iden_type, jfix);
            Jump.Join(call_iden_type, in_ide2_itp, jret);
            Jump.Join(oprn_iden_itp, deep_iden_ity, jfix);
            Jump.Join(deep_iden_ity, return_iden_type, jfix);
            Jump.Join(in_ide2_itp, return_iden_type, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> TRY <<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_try = new StateStatement("1220");
            StateDeep deep_try = new StateDeep();
            StateStatement stmt_try_body = new StateStatement("1000");
            StateDeep deep_try_body = new StateDeep();
            StateSubprogram call_stmt_try = new StateSubprogram();
            StateInsert in_stmt_try = new StateInsert();
            StateRise rise_try_body = new StateRise();
            StateStatement stmt_catch = new StateStatement("1030");
            StateDeep deep_catch = new StateDeep();
            StateSubprogram call_param_catch = new StateSubprogram();
            StateInsert in_param_catch = new StateInsert();
            StateStatement stmt_catch_body = new StateStatement("1000");
            StateDeep deep_catch_body = new StateDeep();
            StateSubprogram call_stmt_catch = new StateSubprogram();
            StateMove mv_br_try = new StateMove();
            StateInsert in_catch_stmt = new StateInsert();
            StateReturn return_try = new StateReturn();
            StateStatement stmt_finally = new StateStatement("1270", " -1");
            StateDeep deep_finally = new StateDeep();
            StateStatement stmt_finally_body = new StateStatement("1000", " -1");
            StateDeep deep_finally_body = new StateDeep();
            StateSubprogram call_stmt_finally = new StateSubprogram("GF");
            StateInsert in_stmt_finally = new StateInsert();
            StateRise rise_catch_body = new StateRise();
            StateRise rise_catch = new StateRise();
            StateMove mv_finally = new StateMove();

            Jump.Join(first_try, stmt_try, jfix);
            Jump.Join(stmt_try, deep_try, jfix);
            Jump.Join(deep_try, stmt_try_body, jopen_brace);
            Jump.Join(stmt_try_body, deep_try_body, jfix);
            Jump.Join(deep_try_body, rise_try_body, jclose_brace);
            Jump.Join(deep_try_body, call_stmt_try, jfix);
            Jump.Join(call_stmt_try, first_stmt, jfix);
            Jump.Join(call_stmt_try, in_stmt_try, jret);
            Jump.Join(in_stmt_try, rise_try_body, jclose_brace);
            Jump.Join(in_stmt_try, call_stmt_try, jfix);
            Jump.Join(rise_try_body, stmt_catch, jcatch);
            Jump.Join(stmt_catch, deep_catch, jfix);
            Jump.Join(deep_catch, call_param_catch, jopen_round_br);
            Jump.Join(call_param_catch, first_param, jfix);
            Jump.Join(call_param_catch, in_param_catch, jret);
            Jump.Join(in_param_catch, mv_br_try, jclose_round_br);
            Jump.Join(mv_br_try, stmt_catch_body, jopen_brace);
            Jump.Join(stmt_catch_body, deep_catch_body, jfix);
            Jump.Join(call_stmt_catch, first_stmt, jfix);
            Jump.Join(call_stmt_catch, in_catch_stmt, jret);
            Jump.Join(in_catch_stmt, mv_finally, jclose_brace);
            Jump.Join(rise_try_body, stmt_finally, jfinally);
            Jump.Join(stmt_finally, deep_finally, jfix);
            Jump.Join(deep_finally, stmt_finally_body, jopen_brace);
            Jump.Join(stmt_finally_body, deep_finally_body, jfix);
            Jump.Join(deep_finally_body, return_try, jclose_brace);
            Jump.Join(deep_finally_body, call_stmt_finally, jfix);
            Jump.Join(call_stmt_finally, first_stmt, jfix);
            Jump.Join(call_stmt_finally, in_stmt_finally, jret);
            Jump.Join(in_stmt_finally, return_try, jclose_brace);
            Jump.Join(in_stmt_finally, call_stmt_finally, jfix);
            Jump.Join(rise_catch_body, rise_catch, jfix);
            Jump.Join(deep_catch_body, mv_finally, jclose_brace);
            Jump.Join(deep_catch_body, call_stmt_catch, jfix);
            Jump.Join(in_catch_stmt, call_stmt_catch, jfix);
            Jump.Join(mv_finally, rise_catch_body, jcatch_fix);
            Jump.Join(mv_finally, rise_catch_body, jfinally_fix);
            Jump.Join(mv_finally, return_try, jfix);
            Jump.Join(rise_catch, stmt_catch, jcatch);
            Jump.Join(rise_catch, stmt_finally, jfinally);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> CASE <<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_switch = new StateStatement("1200");
            StateDeep dp_switch = new StateDeep();
            StateExpress exp_cond_case = new StateExpress();
            StateInsert in_cond_case = new StateInsert();
            StateStatement stmt_body_case = new StateStatement("1000");
            StateDeep dp_body_case = new StateDeep();
            StateStatement stmt_cond_case = new StateStatement("1050");
            StateDeep dp_cond_case = new StateDeep();
            StateRise rise_cond_case = new StateRise();
            StateStatement stmt_case = new StateStatement("1020");
            StateDeep dp_case = new StateDeep();
            StateExpress exp_case = new StateExpress();
            StateInsert in_exp_case = new StateInsert();
            StateStatement stmt_case_body = new StateStatement("1000");
            StateDeep dp_case_body = new StateDeep();
            StateInsert in_body_stmt_case = new StateInsert();
            StateRise rise_case_body = new StateRise();
            StateRise rise_case_body_def = new StateRise();
            StateSubprogram call_stmt_case = new StateSubprogram();
            StateReturn return_switch = new StateReturn();
            StateSetFlag sflag_case = new StateSetFlag("BREAK");
            StateUnsetFlag uflag_case = new StateUnsetFlag("BREAK");
            StateRise rise_case = new StateRise();
            StateRise rise_case_def = new StateRise();
            StateStatement stmt_default = new StateStatement("1060");
            StateDeep dp_default = new StateDeep();
            StateStatement stmt_def_body = new StateStatement("1000");
            StateDeep dp_def_body = new StateDeep();
            StateSubprogram call_stmt_def = new StateSubprogram();
            StateInsert in_def_body = new StateInsert();

            Jump.Join(first_case, stmt_switch, jfix);
            Jump.Join(stmt_switch, dp_switch, jfix);
            Jump.Join(exp_cond_case, in_cond_case, jclose_round_br);
            Jump.Join(dp_switch, stmt_cond_case, jopen_round_br);
            Jump.Join(stmt_cond_case, dp_cond_case, jfix);
            Jump.Join(dp_cond_case, exp_cond_case, jfix);
            Jump.Join(in_cond_case, rise_cond_case, jfix);
            Jump.Join(rise_cond_case, stmt_body_case, jopen_brace);
            Jump.Join(stmt_body_case, dp_body_case, jfix);
            Jump.Join(stmt_case, dp_case, jfix);
            Jump.Join(dp_case, exp_case, jfix);
            Jump.Join(exp_case, in_exp_case, jfix);
            Jump.Join(in_exp_case, stmt_case_body, jcolon);
            Jump.Join(stmt_case_body, dp_case_body, jfix);
            Jump.Join(in_body_stmt_case, rise_case_body, jcase);
            Jump.Join(dp_case_body, call_stmt_case, jfix);
            Jump.Join(call_stmt_case, first_stmt, jfix);
            Jump.Join(call_stmt_case, in_body_stmt_case, jfalse);
            Jump.Join(dp_body_case, sflag_case, jcase);
            Jump.Join(sflag_case, stmt_case, jfix);
            Jump.Join(in_body_stmt_case, rise_case_body_def, jdefault);
            Jump.Join(in_body_stmt_case, uflag_case, jclose_brace);
            Jump.Join(in_body_stmt_case, call_stmt_case, jfix);
            Jump.Join(uflag_case, return_switch, jfix);
            Jump.Join(rise_case_body, rise_case, jfix);
            Jump.Join(rise_case, stmt_case, jfix);
            Jump.Join(rise_case_body_def, rise_case_def, jfix);
            Jump.Join(rise_case_def, stmt_default, jfix);
            Jump.Join(stmt_default, dp_default, jfix);
            Jump.Join(dp_default, stmt_def_body, jcolon);
            Jump.Join(stmt_def_body, dp_def_body, jfix);
            Jump.Join(call_stmt_def, first_stmt, jfix);
            Jump.Join(call_stmt_def, in_def_body, jret);
            Jump.Join(dp_def_body, call_stmt_def, jfix);
            Jump.Join(in_def_body, uflag_case, jclose_brace);
            Jump.Join(in_def_body, call_stmt_def, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> METHOD <<<<<<<<<<<<<<<<<<<<<<<
            StateAttribute attr_type_m = new StateAttribute("1060", "0");
            StateDeep deep_attr_type_m = new StateDeep();
            StateSubprogram call_type_m = new StateSubprogram();
            StateInsert in_attr_type_m = new StateInsert();
            StateRise rise_attr_type_m = new StateRise();
            StateAttribute attr_name_m = new StateAttribute("1000");
            StateOperand oprn_name_m = new StateOperand("1000");
            StateDeep deep_attr_name_m = new StateDeep();
            StateInsert in_name_m = new StateInsert();
            StateAttribute attr_params_m = new StateAttribute("1070");
            StateRise rise_attr_name_m = new StateRise();
            StateDeep deep_attr_params_m = new StateDeep();
            StateSubprogram call_param_m = new StateSubprogram();
            StateInsert in_param_m = new StateInsert();
            StateInsertAttributes in_attr_method = new StateInsertAttributes();
            StateStatement stmt_method_body = new StateStatement("1000");
            StateDeep deep_method_body = new StateDeep();
            StateSubprogram call_stmt_method = new StateSubprogram();
            StateInsert in_stmt_body = new StateInsert();
            StateUnsetFlag uflags_method = new StateUnsetFlag("THROW,M_PRV,M_PUB,M_PRT,M_STC,M_ABS,M_FIN,M_NAT,VOID,METHOD");
            StateStatement stmt_method = new StateStatement("1150");
            StateDeep deep_method = new StateDeep();
            StateRise rise_attr_params_m = new StateRise();
            StateReturn return_method = new StateReturn();
            StateStatement attr_throws_m = new StateStatement("1051");
            StateSetFlag sflag_throw_m = new StateSetFlag("THROW");
            StateDeep deep_attr_throws_m = new StateDeep();
            StateSubprogram call_iden_type_throws = new StateSubprogram();
            StateInsert in_throws_type = new StateInsert();
            StateRise rise_attr_throws = new StateRise();
            StateFlagOne onef_nat_m = new StateFlagOne("M_NAT");
            StateFlagOne onef_nat_abs_m = new StateFlagOne("M_ABS");
            StateError err_nat_dup_m = new StateError("1010");
            StateSetFlag sflag_nat_m = new StateSetFlag("M_NAT");
            StateAttribute attr_nat_m = new StateAttribute("1041");
            StateError err_nat_abs_m = new StateError("1020");
            StateSetFlag sflag_pub_m = new StateSetFlag("M_PUB");
            StateAttribute attr_pub_m = new StateAttribute("1030");
            StateSetFlag sflag_prt_m = new StateSetFlag("M_PRT");
            StateAttribute attr_prt_m = new StateAttribute("1020");
            StateFlagOne onef_prv_abs_m = new StateFlagOne("M_ABS");
            StateSetFlag sflag_prv_m = new StateSetFlag("M_PRV");
            StateAttribute attr_prv_m = new StateAttribute("1010");
            StateFlagOne onef_stc_m = new StateFlagOne("M_STC");
            StateFlagOne onef_stc_abs_m = new StateFlagOne("M_ABS");
            StateError err_stc_dup_m = new StateError("1010");
            StateError err_static2 = new StateError("1020");
            StateSetFlag sflag_stc_m = new StateSetFlag("M_STC");
            StateAttribute attr_stc_m = new StateAttribute("1035");
            StateFlagOne onef_fin_m = new StateFlagOne("M_FIN");
            StateFlagOne onef_fin_abs_m = new StateFlagOne("M_ABS");
            StateError err_fin_dup_m = new StateError("1010");
            StateError err_fin_abs_m = new StateError("1020");
            StateSetFlag sflag_fin_m = new StateSetFlag("M_FIN");
            StateAttribute attr_fin_m = new StateAttribute("1040");
            StateFlagOne onef_abs_m = new StateFlagOne("M_ABS");
            StateFlagOne onef_abs_stc_m = new StateFlagOne("M_STC");
            StateFlagOne onef_abs_fin_m = new StateFlagOne("M_FIN");
            StateFlagOne onef_abs_nat_m = new StateFlagOne("M_NAT");
            StateFlagOne onef_abs_prv_m = new StateFlagOne("M_PRV");
            StateError err_abstract1 = new StateError("1000");
            StateError err_abstract5 = new StateError("1000");
            StateSetFlag sflag_abs_m = new StateSetFlag("M_ABS");
            StateAttribute attr_abs_m = new StateAttribute("1050");
            StateFlagOne onef_abs_body_m = new StateFlagOne("M_ABS");
            StateFlagOne onef_nat_body_m = new StateFlagOne("M_NAT");
            StateMove mv_abs_body_m = new StateMove();
            StateMove mv_body_m = new StateMove();
            StateOperand oprn_void_m = new StateOperand("1068");
            StateSetFlag sflag_void_m = new StateSetFlag("VOID");
            StateFlagOne onef_acs_m = new StateFlagOne("M_PUB,M_PRT,M_PRV");
            StateError err_acs_dup_m = new StateError("1020");
            StateMove mv_acs_m = new StateMove();
            StateError err_prv_abs_m = new StateError("1020");
            StateFlagOne onef_syn_m = new StateFlagOne("M_SYN");
            StateError err_syn_m = new StateError("1010", "-1");
            StateSetFlag sflag_syn_m = new StateSetFlag("M_SYN");
            StateAttribute attr_syn_m = new StateAttribute("1043", "-1");
            StateSetFlag sflag_method = new StateSetFlag("METHOD");

            Jump.Join(attr_type_m, deep_attr_type_m, jfix);
            Jump.Join(deep_attr_type_m, oprn_void_m, jvoid);
            Jump.Join(deep_attr_type_m, call_type_m, jfix);
            Jump.Join(call_type_m, first_type, jfix);
            Jump.Join(call_type_m, in_attr_type_m, jret);
            Jump.Join(in_attr_type_m, rise_attr_type_m, jfix);
            Jump.Join(rise_attr_type_m, attr_name_m, jiden);
            Jump.Join(attr_name_m, deep_attr_name_m, jfix);
            Jump.Join(deep_attr_name_m, oprn_name_m, jfix);
            Jump.Join(oprn_name_m, in_name_m, jfix);
            Jump.Join(in_name_m, rise_attr_name_m, jfix);
            Jump.Join(rise_attr_name_m, attr_params_m, jopen_round_br);
            Jump.Join(attr_params_m, deep_attr_params_m, jfix);
            Jump.Join(deep_attr_params_m, rise_attr_params_m, jclose_round_br);
            Jump.Join(deep_attr_params_m, call_param_m, jfix);
            Jump.Join(call_param_m, first_param, jfix);
            Jump.Join(call_param_m, in_param_m, jret);
            Jump.Join(in_param_m, call_param_m, jcomma);
            Jump.Join(stmt_method_body, deep_method_body, jfix);
            Jump.Join(deep_method_body, uflags_method, jclose_brace);
            Jump.Join(deep_method_body, call_stmt_method, jfix);
            Jump.Join(call_stmt_method, first_stmt, jfix);
            Jump.Join(call_stmt_method, in_stmt_body, jret);
            Jump.Join(in_stmt_body, uflags_method, jclose_brace);
            Jump.Join(in_stmt_body, call_stmt_method, jfix);
            Jump.Join(stmt_method, deep_method, jfix);
            Jump.Join(deep_method, in_attr_method, jfix);
            Jump.Join(in_param_m, rise_attr_params_m, jclose_round_br);
            Jump.Join(rise_attr_params_m, sflag_throw_m, jthrows);
            Jump.Join(rise_attr_params_m, stmt_method, jfix);
            Jump.Join(sflag_throw_m, attr_throws_m, jfix);
            Jump.Join(attr_throws_m, deep_attr_throws_m, jfix);
            Jump.Join(deep_attr_throws_m, call_iden_type_throws, jfix);
            Jump.Join(call_iden_type_throws, first_iden_type, jfix);
            Jump.Join(call_iden_type_throws, in_throws_type, jret);
            Jump.Join(rise_attr_throws, stmt_method, jfix);
            Jump.Join(first_method, onef_syn_m, jsynchronized);
            Jump.Join(first_method, onef_nat_m, jnative);
            Jump.Join(first_method, onef_stc_m, jstatic);
            Jump.Join(first_method, onef_fin_m, jfinal);
            Jump.Join(first_method, onef_abs_m, jabstract);
            Jump.Join(first_method, onef_acs_m, jpublic_fix);
            Jump.Join(first_method, onef_acs_m, jprotected_fix);
            Jump.Join(first_method, onef_acs_m, jprivate_fix);
            Jump.Join(first_method, sflag_method, jfix);
            Jump.Join(onef_nat_m, err_nat_dup_m, jtrue);
            Jump.Join(onef_nat_m, onef_nat_abs_m, jfalse);
            Jump.Join(onef_nat_abs_m, err_nat_abs_m, jtrue);
            Jump.Join(onef_nat_abs_m, sflag_nat_m, jfalse);
            Jump.Join(sflag_nat_m, attr_nat_m, jfix);
            Jump.Join(sflag_pub_m, attr_pub_m, jfix);
            Jump.Join(attr_nat_m, first_method, jfix);
            Jump.Join(attr_pub_m, first_method, jfix);
            Jump.Join(sflag_prt_m, attr_prt_m, jfix);
            Jump.Join(attr_prt_m, first_method, jfix);
            Jump.Join(sflag_prv_m, attr_prv_m, jfix);
            Jump.Join(attr_prv_m, first_method, jfix);
            Jump.Join(onef_stc_m, err_stc_dup_m, jtrue);
            Jump.Join(onef_stc_m, onef_stc_abs_m, jfalse);
            Jump.Join(onef_stc_abs_m, err_static2, jtrue);
            Jump.Join(onef_stc_abs_m, sflag_stc_m, jfalse);
            Jump.Join(sflag_stc_m, attr_stc_m, jfix);
            Jump.Join(attr_stc_m, first_method, jfix);
            Jump.Join(onef_fin_m, err_fin_dup_m, jtrue);
            Jump.Join(onef_fin_m, onef_fin_abs_m, jfalse);
            Jump.Join(onef_fin_abs_m, err_fin_abs_m, jtrue);
            Jump.Join(onef_fin_abs_m, sflag_fin_m, jfalse);
            Jump.Join(sflag_fin_m, attr_fin_m, jfix);
            Jump.Join(attr_fin_m, first_method, jfix);
            Jump.Join(onef_abs_m, err_abstract1, jtrue);
            Jump.Join(onef_abs_m, onef_abs_stc_m, jfalse);
            Jump.Join(onef_abs_prv_m, err_abstract5, jtrue);
            Jump.Join(onef_abs_prv_m, sflag_abs_m, jfalse);
            Jump.Join(sflag_abs_m, attr_abs_m, jfix);
            Jump.Join(attr_abs_m, first_method, jfix);
            Jump.Join(in_attr_method, onef_abs_body_m, jfix);
            Jump.Join(onef_abs_body_m, mv_abs_body_m, jtrue);
            Jump.Join(onef_abs_body_m, onef_nat_body_m, jfalse);
            Jump.Join(onef_nat_body_m, mv_abs_body_m, jtrue);
            Jump.Join(onef_nat_body_m, mv_body_m, jfalse);
            Jump.Join(mv_body_m, stmt_method_body, jopen_brace);
            Jump.Join(mv_abs_body_m, uflags_method, jscolon);
            Jump.Join(uflags_method, return_method, jfix);
            Jump.Join(oprn_void_m, sflag_void_m, jfix);
            Jump.Join(sflag_void_m, in_attr_type_m, jfix);
            Jump.Join(onef_acs_m, err_acs_dup_m, jtrue);
            Jump.Join(onef_acs_m, mv_acs_m, jfalse);
            Jump.Join(mv_acs_m, sflag_pub_m, jpublic);
            Jump.Join(mv_acs_m, sflag_prt_m, jprotected);
            Jump.Join(mv_acs_m, onef_prv_abs_m, jprivate);
            Jump.Join(onef_prv_abs_m, err_prv_abs_m, jtrue);
            Jump.Join(onef_prv_abs_m, sflag_prv_m, jfalse);
            Jump.Join(in_throws_type, call_iden_type_throws, jcomma);
            Jump.Join(in_throws_type, rise_attr_throws, jfix);
            Jump.Join(onef_abs_stc_m, err_abstract5, jtrue);
            Jump.Join(onef_abs_stc_m, onef_abs_fin_m, jfalse);
            Jump.Join(onef_abs_fin_m, err_abstract5, jtrue);
            Jump.Join(onef_abs_fin_m, onef_abs_nat_m, jfalse);
            Jump.Join(onef_abs_nat_m, err_abstract5, jtrue);
            Jump.Join(onef_abs_nat_m, onef_abs_prv_m, jfalse);
            Jump.Join(onef_syn_m, err_syn_m, jtrue);
            Jump.Join(onef_syn_m, sflag_syn_m, jfalse);
            Jump.Join(sflag_syn_m, attr_syn_m, jfix);
            Jump.Join(attr_syn_m, first_method, jfix);
            Jump.Join(sflag_method, attr_type_m, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> CONSTRUCT <<<<<<<<<<<<<<<<<<<<<<<
            StateSetFlag sflag_construct = new StateSetFlag("CONSTRUCT");
            StateFlagOne onef_contr_acs = new StateFlagOne("C_PUB, C_PRV, C_PRT");
            StateError err_constr_acs = new StateError("1010");
            StateStatement stmt_construct = new StateStatement("1260", "0");
            StateDeep deep_construct = new StateDeep();
            StateMove mv_constr_acs = new StateMove();
            StateSetFlag sflag_c_prv = new StateSetFlag("C_PRV");
            StateSetFlag sflag_c_prt = new StateSetFlag("C_PRT");
            StateSetFlag sflag_c_pub = new StateSetFlag("C_PUB");
            StateAttribute attr_c_prv = new StateAttribute("1010");
            StateAttribute attr_c_prt = new StateAttribute("1020");
            StateAttribute attr_c_pub = new StateAttribute("1030");
            StateInsertAttributes in_attr_construct = new StateInsertAttributes();
            StateAttribute attr_c_name = new StateAttribute("1000", "-1");
            StateDeep deep_c_name = new StateDeep();
            StateOperand oprn_c_name = new StateOperand("1000");
            StateInsert in_c_name = new StateInsert();
            StateRise rise_c_name = new StateRise();
            StateAttribute attr_c_param = new StateAttribute("1070", " -1");
            StateDeep deep_c_params = new StateDeep();
            StateSubprogram call_param_c = new StateSubprogram("GF");
            StateRise rise_params_c = new StateRise();
            StateInsert in_param_c = new StateInsert();
            StateStatement stmt_body_c = new StateStatement("1000", "-1");
            StateUnsetFlag uflag_construct = new StateUnsetFlag("C_PUB, C_PRV, C_PRT, CONSTRUCT, THROW");
            StateDeep deep_body_c = new StateDeep();
            StateReturn return_construct = new StateReturn();
            StateSubprogram call_stmt_c = new StateSubprogram("GF");
            StateInsert in_stmt_c = new StateInsert();
            StateStatement attr_throws_c = new StateStatement("1051", "-1");
            StateDeep deep_throws_c = new StateDeep();
            StateSubprogram call_iden_type_c = new StateSubprogram("GF");
            StateInsert in_throws_type_c = new StateInsert();
            StateRise rise_throws_c = new StateRise();
            StateSetFlag sflag_throw_c = new StateSetFlag("THROW");

            Jump.Join(first_construct, onef_contr_acs, jpublic_fix);
            Jump.Join(first_construct, onef_contr_acs, jprotected_fix);
            Jump.Join(first_construct, onef_contr_acs, jprivate_fix);
            Jump.Join(onef_contr_acs, err_constr_acs, jtrue);
            Jump.Join(onef_contr_acs, mv_constr_acs, jfalse);
            Jump.Join(mv_constr_acs, sflag_c_prv, jprivate);
            Jump.Join(mv_constr_acs, sflag_c_prt, jprotected);
            Jump.Join(mv_constr_acs, sflag_c_pub, jpublic);
            Jump.Join(sflag_c_prv, attr_c_prv, jfix);
            Jump.Join(sflag_c_prt, attr_c_prt, jfix);
            Jump.Join(sflag_c_pub, attr_c_pub, jfix);
            Jump.Join(attr_c_prv, first_construct, jfix);
            Jump.Join(attr_c_prt, first_construct, jfix);
            Jump.Join(attr_c_pub, first_construct, jfix);
            Jump.Join(first_construct, sflag_construct, jfix);
            Jump.Join(sflag_construct, stmt_construct, jfix);
            Jump.Join(stmt_construct, deep_construct, jfix);
            Jump.Join(deep_construct, in_attr_construct, jfix);
            Jump.Join(in_attr_construct, attr_c_name, jiden);
            Jump.Join(attr_c_name, deep_c_name, jfix);
            Jump.Join(deep_c_name, oprn_c_name, jfix);
            Jump.Join(oprn_c_name, in_c_name, jfix);
            Jump.Join(in_c_name, rise_c_name, jfix);
            Jump.Join(rise_c_name, attr_c_param, jopen_round_br);
            Jump.Join(attr_c_param, deep_c_params, jfix);
            Jump.Join(deep_c_params, rise_params_c, jclose_round_br);
            Jump.Join(deep_c_params, call_param_c, jfix);
            Jump.Join(call_param_c, first_param, jfix);
            Jump.Join(call_param_c, in_param_c, jret);
            Jump.Join(in_param_c, call_param_c, jcomma);
            Jump.Join(in_param_c, rise_params_c, jclose_round_br);
            Jump.Join(rise_params_c, stmt_body_c, jopen_brace);
            Jump.Join(stmt_body_c, deep_body_c, jfix);
            Jump.Join(deep_body_c, uflag_construct, jclose_brace);
            Jump.Join(uflag_construct, return_construct, jfix);
            Jump.Join(deep_body_c, call_stmt_c, jfix);
            Jump.Join(call_stmt_c, first_stmt, jfix);
            Jump.Join(call_stmt_c, in_stmt_c, jret);
            Jump.Join(in_stmt_c, uflag_construct, jclose_brace);
            Jump.Join(in_stmt_c, call_stmt_c, jfix);
            Jump.Join(attr_throws_c, deep_throws_c, jfix);
            Jump.Join(deep_throws_c, call_iden_type_c, jfix);
            Jump.Join(call_iden_type_c, first_iden_type, jfix);
            Jump.Join(call_iden_type_c, in_throws_type_c, jret);
            Jump.Join(in_throws_type_c, call_iden_type_c, jcomma);
            Jump.Join(in_throws_type_c, rise_throws_c, jfix);
            Jump.Join(rise_throws_c, stmt_body_c, jopen_brace);
            Jump.Join(rise_params_c, sflag_throw_c, jthrows);
            Jump.Join(sflag_throw_c, attr_throws_c, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> STATIC <<<<<<<<<<<<<<<<<<<<<<<
            StateStatement stmt_static = new StateStatement("1290", "-1");
            StateDeep deep_static = new StateDeep();
            StateStatement stmt_static_body = new StateStatement("1000", "-1");
            StateDeep deep_static_body = new StateDeep();
            StateSetFlag sflag_static = new StateSetFlag("STATIC");
            StateUnsetFlag uflag_static = new StateUnsetFlag("STATIC");
            StateReturn return_static = new StateReturn();
            StateSubprogram call_stmt_static = new StateSubprogram("GF");
            StateInsert in_stmt_static = new StateInsert();

            Jump.Join(first_static, sflag_static, jfix);
            Jump.Join(sflag_static, stmt_static, jfix);
            Jump.Join(stmt_static, deep_static, jfix);
            Jump.Join(deep_static, stmt_static_body, jopen_brace);
            Jump.Join(stmt_static_body, deep_static_body, jfix);
            Jump.Join(deep_static_body, uflag_static, jclose_brace);
            Jump.Join(deep_static_body, call_stmt_static, jfix);
            Jump.Join(call_stmt_static, first_stmt, jfix);
            Jump.Join(call_stmt_static, in_stmt_static, jret);
            Jump.Join(uflag_static, return_static, jfix);
            Jump.Join(in_stmt_static, uflag_static, jclose_brace);
            Jump.Join(in_stmt_static, call_stmt_static, jfix);            
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> CLASS <<<<<<<<<<<<<<<<<<<<<<<
            StateFlagOne onef_acs_cl = new StateFlagOne("CL_PUB,CL_PRV,CL_PRT");
            StateError err_acs_cl = new StateError("1010");
            StateSetFlag sflag_cl_pub = new StateSetFlag("CL_PUB");
            StateAttribute attr_cl_pub = new StateAttribute("1030");
            StateSetFlag sflag_cl_prt = new StateSetFlag("CL_PRT");
            StateAttribute attr_cl_prt = new StateAttribute("1020");
            StateSetFlag sflag_cl_prv = new StateSetFlag("CL_PRV");
            StateAttribute attr_cl_prv = new StateAttribute("1010");
            StateFlagOne onef_method_cl = new StateFlagOne("METHOD");
            StateMove mv_acs_cl = new StateMove();
            StateFlagOne onef_abs_cl = new StateFlagOne("CL_ABS");
            StateFlagOne onef_abs_fin_c = new StateFlagOne("CL_FIN");
            StateError err_abs_dup_cl = new StateError("1010");
            StateError err_abs_fin_cl = new StateError("1020");
            StateSetFlag sflag_abs_cl = new StateSetFlag("CL_ABS");
            StateAttribute attr_abs_cl = new StateAttribute("1050");
            StateFlagOne onef_fin_cl = new StateFlagOne("CL_FIN");
            StateFlagOne onef_fin_abs_cl = new StateFlagOne("CL_ABS");
            StateError err_fin_dup = new StateError("1010");
            StateSetFlag sflag_fin_cl = new StateSetFlag("CL_FIN");
            StateAttribute attr_fin_cl = new StateAttribute("1040");
            StateSetFlag sflag_class = new StateSetFlag("CLASS");
            StateStatement stmt_class = new StateStatement("1040");
            StateRise rise_class_name = new StateRise();
            StateStatement stmt_extends_cl = new StateStatement("1240");
            StateDeep deep_extends_cl = new StateDeep();
            StateSubprogram call_iden_type_ext = new StateSubprogram();
            StateInsert in_extends_type_cl = new StateInsert();
            StateDeep deep_class = new StateDeep();
            StateStatement stmt_implements = new StateStatement("1250");
            StateDeep deep_implements = new StateDeep();
            StateRise rise_extends_cl = new StateRise();
            StateSubprogram call_iden_type_imp = new StateSubprogram();
            StateInsert in_impl_type = new StateInsert();
            StateMove mv_impl_type = new StateMove();
            StateStatement stmt_class_body = new StateStatement("1000");
            StateRise rise_implements = new StateRise();
            StateDeep deep_class_body = new StateDeep();
            StateReturn return_class = new StateReturn();
            StateUnsetFlag uflags_class = new StateUnsetFlag("CL_PUB, CL_PRV, CL_PRT, CL_ABS, CL_STC, CL_FIN, CLASS");
            StateAttribute attr_class_name = new StateAttribute("1000");
            StateDeep deep_class_name = new StateDeep();
            StateOperand oprn_class_name = new StateOperand("1000");
            StateInsert in_class_name = new StateInsert();
            StateInsertAttributes in_attr_class = new StateInsertAttributes();
            StateFlagOne onef_stc_cl = new StateFlagOne("CL_STC");
            StateFlagOne onef_class = new StateFlagOne("CLASS");
            StateMove mv_class = new StateMove();
            StateMove mv_method_cl = new StateMove();
            StateMove mv_package_cl = new StateMove();
            StateError err_stc_cl = new StateError("1010");
            StateSetFlag sflag_cl_stc = new StateSetFlag("CL_STC");
            StateAttribute attr_cl_stc = new StateAttribute("1035");
            StateMove mv_cl_body = new StateMove();
            StateSubprogram call_method_cl = new StateSubprogram();
            StateMove mv_type_cl = new StateMove();
            StatePoint point_cl_body = new StatePoint();
            StateBackoff back_method_cl = new StateBackoff();
            StateInsert in_method_cl = new StateInsert();
            StateMove mv_iden_cl = new StateMove();
            StateMove mv_dot_cl = new StateMove();
            StateBackoff back_field_cl = new StateBackoff();
            StateSubprogram call_field_cl = new StateSubprogram();
            StateInsert in_field_cl = new StateInsert();
            StateSubprogram call_class_cl = new StateSubprogram();
            StateInsert in_class_cl = new StateInsert();
            StateSubprogram call_interface_cl = new StateSubprogram();
            StateInsert in_interface_cl = new StateInsert();
            StateBackoff back_class_cl = new StateBackoff();
            StateBackoff back_interface_cl = new StateBackoff();
            StateFlagOne onef_acs_pack_cl = new StateFlagOne("CL_PUB,CL_PRV,CL_PRT");
            StateSubprogram call_construct_cl = new StateSubprogram("GF");
            StateBackoff back_construct = new StateBackoff();
            StateInsert in_construct = new StateInsert();
            StateBackoff back_static_cl = new StateBackoff();
            StateSubprogram call_static_cl = new StateSubprogram("GF");
            StateInsert in_static_cl = new StateInsert();

            Jump.Join(mv_acs_cl, sflag_cl_pub, jpublic);
            Jump.Join(sflag_cl_pub, attr_cl_pub, jfix);
            Jump.Join(sflag_cl_prt, attr_cl_prt, jfix);
            Jump.Join(sflag_cl_prv, attr_cl_prv, jfix);
            Jump.Join(attr_cl_pub, first_class, jfix);
            Jump.Join(attr_cl_prt, first_class, jfix);
            Jump.Join(attr_cl_prv, first_class, jfix);
            Jump.Join(onef_abs_cl, err_abs_dup_cl, jtrue);
            Jump.Join(onef_abs_cl, onef_abs_fin_c, jfalse);
            Jump.Join(onef_abs_fin_c, err_abs_fin_cl, jtrue);
            Jump.Join(onef_abs_fin_c, sflag_abs_cl, jfalse);
            Jump.Join(sflag_abs_cl, attr_abs_cl, jfix);
            Jump.Join(attr_abs_cl, first_class, jfix);
            Jump.Join(onef_fin_cl, err_fin_dup, jtrue);
            Jump.Join(onef_fin_cl, onef_fin_abs_cl, jfalse);
            Jump.Join(onef_fin_abs_cl, err_abs_fin_cl, jtrue);
            Jump.Join(onef_fin_abs_cl, sflag_fin_cl, jfalse);
            Jump.Join(sflag_fin_cl, attr_fin_cl, jfix);
            Jump.Join(attr_fin_cl, first_class, jfix);
            Jump.Join(first_class, sflag_class, jclass);
            Jump.Join(stmt_extends_cl, deep_extends_cl, jfix);
            Jump.Join(rise_class_name, stmt_extends_cl, jextends);
            Jump.Join(deep_extends_cl, call_iden_type_ext, jiden_fix);
            Jump.Join(call_iden_type_ext, first_iden_type, jfix);
            Jump.Join(call_iden_type_ext, in_extends_type_cl, jret);
            Jump.Join(stmt_class, deep_class, jfix);
            Jump.Join(in_extends_type_cl, rise_extends_cl, jfix);
            Jump.Join(rise_extends_cl, stmt_implements, jimplements);
            Jump.Join(stmt_implements, deep_implements, jfix);
            Jump.Join(call_iden_type_imp, first_iden_type, jfix);
            Jump.Join(deep_implements, call_iden_type_imp, jiden_fix);
            Jump.Join(call_iden_type_imp, in_impl_type, jfix);
            Jump.Join(in_impl_type, mv_impl_type, jcomma);
            Jump.Join(mv_impl_type, call_iden_type_imp, jiden_fix);
            Jump.Join(in_impl_type, rise_implements, jopen_brace);
            Jump.Join(stmt_class_body, deep_class_body, jfix);
            Jump.Join(deep_class_body, uflags_class, jclose_brace);
            Jump.Join(uflags_class, return_class, jfix);
            Jump.Join(rise_class_name, stmt_implements, jimplements);
            Jump.Join(attr_class_name, deep_class_name, jfix);
            Jump.Join(deep_class_name, oprn_class_name, jfix);
            Jump.Join(oprn_class_name, in_class_name, jfix);
            Jump.Join(in_class_name, rise_class_name, jfix);
            Jump.Join(rise_implements, stmt_class_body, jfix);
            Jump.Join(rise_class_name, stmt_class_body, jopen_brace);
            Jump.Join(rise_extends_cl, stmt_class_body, jopen_brace);
            Jump.Join(sflag_class, stmt_class, jfix);
            Jump.Join(deep_class, in_attr_class, jfix);
            Jump.Join(in_attr_class, attr_class_name, jiden);
            Jump.Join(onef_class, onef_method_cl, jtrue);
            Jump.Join(onef_method_cl, mv_method_cl, jtrue);
            Jump.Join(onef_method_cl, mv_class, jfalse);
            Jump.Join(onef_class, mv_package_cl, jfalse);
            Jump.Join(mv_class, onef_acs_cl, jprivate_fix);
            Jump.Join(mv_class, onef_acs_cl, jprotected_fix);
            Jump.Join(mv_class, onef_acs_cl, jpublic_fix);
            Jump.Join(first_class, onef_abs_cl, jabstract);
            Jump.Join(first_class, onef_fin_cl, jfinal);
            Jump.Join(mv_class, onef_stc_cl, jstatic);
            Jump.Join(onef_stc_cl, err_stc_cl, jtrue);
            Jump.Join(onef_stc_cl, sflag_cl_stc, jfalse);
            Jump.Join(sflag_cl_stc, attr_cl_stc, jfix);
            Jump.Join(attr_cl_stc, first_class, jfix);
            Jump.Join(first_class, onef_class, jfix);
            Jump.Join(onef_acs_cl, err_acs_cl, jtrue);
            Jump.Join(onef_acs_cl, mv_acs_cl, jfalse);
            Jump.Join(back_method_cl, call_method_cl, jfix);
            Jump.Join(call_method_cl, first_method, jfix);
            Jump.Join(call_method_cl, in_method_cl, jret);
            Jump.Join(mv_cl_body, mv_cl_body, jpublic);
            Jump.Join(mv_cl_body, mv_cl_body, jprotected);
            Jump.Join(mv_cl_body, mv_cl_body, jprivate);
            Jump.Join(mv_cl_body, mv_cl_body, jabstract);
            Jump.Join(mv_cl_body, mv_cl_body, jfinal);
            Jump.Join(mv_cl_body, mv_cl_body, jstatic);
            Jump.Join(mv_cl_body, mv_cl_body, jnative);
            Jump.Join(mv_type_cl, mv_iden_cl, jiden);
            Jump.Join(mv_iden_cl, back_method_cl, jopen_round_br);
            Jump.Join(mv_cl_body, back_method_cl, jvoid);
            Jump.Join(mv_cl_body, mv_type_cl, jint);
            Jump.Join(mv_cl_body, mv_dot_cl, jiden);
            Jump.Join(mv_dot_cl, mv_cl_body, jdot);
            Jump.Join(mv_dot_cl, mv_iden_cl, jiden);
            Jump.Join(mv_iden_cl, back_field_cl, jfix);
            Jump.Join(back_field_cl, call_field_cl, jfix);
            Jump.Join(call_field_cl, first_field, jfix);
            Jump.Join(call_field_cl, in_field_cl, jret);
            Jump.Join(call_class_cl, first_class, jfix);
            Jump.Join(call_class_cl, in_class_cl, jret);
            Jump.Join(call_interface_cl, first_interface, jfix);
            Jump.Join(call_interface_cl, in_interface_cl, jret);
            Jump.Join(mv_cl_body, back_class_cl, jclass);
            Jump.Join(back_class_cl, call_class_cl, jfix);
            Jump.Join(back_interface_cl, call_interface_cl, jfix);
            Jump.Join(deep_class_body, point_cl_body, jfix);
            Jump.Join(in_class_cl, point_cl_body, jfix);
            Jump.Join(in_method_cl, point_cl_body, jfix);
            Jump.Join(in_field_cl, point_cl_body, jfix);
            Jump.Join(in_interface_cl, point_cl_body, jfix);
            Jump.Join(mv_cl_body, back_interface_cl, jinterface);
            Jump.Join(point_cl_body, uflags_class, jclose_brace);
            Jump.Join(point_cl_body, mv_cl_body, jfix);
            Jump.Join(mv_acs_cl, sflag_cl_prt, jprotected);
            Jump.Join(mv_acs_cl, sflag_cl_prv, jprivate);
            Jump.Join(mv_package_cl, onef_acs_pack_cl, jpublic);
            Jump.Join(onef_acs_pack_cl, err_acs_cl, jtrue);
            Jump.Join(onef_acs_pack_cl, sflag_cl_pub, jfalse);
            Jump.Join(mv_cl_body, mv_type_cl, jlong);
            Jump.Join(mv_cl_body, mv_type_cl, jbyte);
            Jump.Join(mv_cl_body, mv_type_cl, jshort);
            Jump.Join(mv_cl_body, mv_type_cl, jchar);
            Jump.Join(mv_cl_body, mv_type_cl, jfloat);
            Jump.Join(mv_cl_body, mv_type_cl, jdouble);
            Jump.Join(mv_cl_body, mv_type_cl, jboolean);
            Jump.Join(mv_type_cl, mv_type_cl, jarray);
            Jump.Join(mv_dot_cl, mv_dot_cl, jarray);
            Jump.Join(mv_cl_body, back_field_cl, jvolatile);
            Jump.Join(mv_dot_cl, back_construct, jopen_round_br);
            Jump.Join(back_construct, call_construct_cl, jfix);
            Jump.Join(call_construct_cl, first_construct, jfix);
            Jump.Join(call_construct_cl, in_construct, jret);
            Jump.Join(in_construct, point_cl_body, jfix);
            Jump.Join(mv_cl_body, back_field_cl, jtransient);
            Jump.Join(mv_cl_body, back_method_cl, jsynchronized);
            Jump.Join(mv_cl_body, back_static_cl, jopen_brace);
            Jump.Join(back_static_cl, call_static_cl, jfw);
            Jump.Join(call_static_cl, first_static, jfix);
            Jump.Join(call_static_cl, in_static_cl, jret);
            Jump.Join(in_static_cl, point_cl_body, jfix);
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>>>>>> FIELD <<<<<<<<<<<<<<<<<<<<<<<
            StateFlagOne onef_f_acs = new StateFlagOne("F_PUB, F_PRT, F_PRV");
            StateFlagOne onef_fin_f = new StateFlagOne("F_FIN");
            StateFlagOne onef_stc_f = new StateFlagOne("F_STC");
            StateFlagOne onef_vol_f = new StateFlagOne("F_VOL");
            StateFlagOne onef_vol_fin_f = new StateFlagOne("F_FIN");
            StateFlagOne onef_f_fin_vol = new StateFlagOne("F_VOL");
            StateError err_asc_f = new StateError("1010");
            StateMove mv_f_acs = new StateMove();
            StateSetFlag sflag_f_prv = new StateSetFlag("F_PRV");
            StateSetFlag sflag_f_prt = new StateSetFlag("F_PRT");
            StateSetFlag sflag_f_pub = new StateSetFlag("F_PUB");
            StateAttribute attr_f_pub = new StateAttribute("1030");
            StateAttribute attr_f_prt = new StateAttribute("1020");
            StateAttribute attr_f_prv = new StateAttribute("1010");
            StateSetFlag sflag_f_stc = new StateSetFlag("F_STC");
            StateAttribute attr_f_stc = new StateAttribute("1035");
            StateError err_stc_f = new StateError("1010");
            StateError err_fin_f = new StateError("1010");
            StateError err_fin_vol_f = new StateError("1020");
            StateSetFlag sflag_f_fin = new StateSetFlag("F_FIN");
            StateAttribute attr_f_fin = new StateAttribute("1040");
            StateError err_vol_f = new StateError("1010");
            StateSetFlag sflag_f_vol = new StateSetFlag("F_VOL");
            StateAttribute attr_f_vol = new StateAttribute("1055");
            StateSubprogram call_type = new StateSubprogram();
            StateAttribute attr_f_type = new StateAttribute("1060", " 0");
            StateInsertAttributes in_attr_f = new StateInsertAttributes();
            StateStatement stmt_field = new StateStatement("1090", " 0");
            StateDeep deep_field = new StateDeep();
            StateDeep deep_f_type = new StateDeep();
            StateInsert in_f_type = new StateInsert();
            StateRise rise_f_type = new StateRise();
            StateAttribute attr_f_name = new StateAttribute("1000");
            StateDeep deep_f_name = new StateDeep();
            StateOperand oprn_f_name = new StateOperand("1000");
            StateDeep deep_f_oprn_name = new StateDeep();
            StateUnsetFlag uflag_field = new StateUnsetFlag("F_PUB, F_PRT, F_PRV, F_FIN, F_STC, F_VOL,F_TRA");
            StateReturn return_field = new StateReturn();
            StateExpress exp_f_val = new StateExpress();
            StateInsert in_f_val = new StateInsert();
            StateFlagOne onef_transient = new StateFlagOne("F_TRA");
            StateError err_dup_tra = new StateError("1010");
            StateSetFlag sflag_transient = new StateSetFlag("F_TRA");
            StateAttribute attr_transient = new StateAttribute("1056", " -1");

            Jump.Join(first_field, onef_f_acs, jprivate_fix);
            Jump.Join(first_field, onef_f_acs, jprotected_fix);
            Jump.Join(first_field, onef_f_acs, jpublic_fix);
            Jump.Join(first_field, onef_fin_f, jfinal);
            Jump.Join(first_field, onef_stc_f, jstatic);
            Jump.Join(first_field, onef_vol_f, jvolatile);
            Jump.Join(onef_f_acs, err_asc_f, jtrue);
            Jump.Join(onef_f_acs, mv_f_acs, jfalse);
            Jump.Join(mv_f_acs, sflag_f_prv, jprivate);
            Jump.Join(mv_f_acs, sflag_f_prt, jprotected);
            Jump.Join(mv_f_acs, sflag_f_pub, jpublic);
            Jump.Join(sflag_f_prv, attr_f_prv, jfix);
            Jump.Join(sflag_f_prt, attr_f_prt, jfix);
            Jump.Join(sflag_f_pub, attr_f_pub, jfix);
            Jump.Join(attr_f_prv, first_field, jfix);
            Jump.Join(attr_f_prt, first_field, jfix);
            Jump.Join(attr_f_pub, first_field, jfix);
            Jump.Join(onef_stc_f, err_stc_f, jtrue);
            Jump.Join(onef_stc_f, sflag_f_stc, jfalse);
            Jump.Join(sflag_f_stc, attr_f_stc, jfix);
            Jump.Join(attr_f_stc, first_field, jfix);
            Jump.Join(onef_fin_f, err_fin_f, jtrue);
            Jump.Join(onef_fin_f, onef_f_fin_vol, jfalse);
            Jump.Join(onef_f_fin_vol, err_fin_vol_f, jtrue);
            Jump.Join(onef_f_fin_vol, sflag_f_fin, jfalse);
            Jump.Join(sflag_f_fin, attr_f_fin, jfix);
            Jump.Join(attr_f_fin, first_field, jfix);
            Jump.Join(onef_vol_f, err_vol_f, jtrue);
            Jump.Join(onef_vol_f, onef_vol_fin_f, jfalse);
            Jump.Join(onef_vol_fin_f, err_fin_vol_f, jtrue);
            Jump.Join(onef_vol_fin_f, sflag_f_vol, jfalse);
            Jump.Join(sflag_f_vol, attr_f_vol, jfix);
            Jump.Join(attr_f_vol, first_field, jfix);
            Jump.Join(call_type, first_type, jfix);
            Jump.Join(first_field, onef_transient, jtransient);
            Jump.Join(first_field, stmt_field, jfix);
            Jump.Join(stmt_field, deep_field, jfix);
            Jump.Join(deep_field, in_attr_f, jfix);
            Jump.Join(in_attr_f, attr_f_type, jfix);
            Jump.Join(attr_f_type, deep_f_type, jfix);
            Jump.Join(deep_f_type, call_type, jfix);
            Jump.Join(call_type, in_f_type, jret);
            Jump.Join(in_f_type, rise_f_type, jfix);
            Jump.Join(rise_f_type, attr_f_name, jfix);
            Jump.Join(attr_f_name, deep_f_name, jfix);
            Jump.Join(deep_f_name, oprn_f_name, jiden);
            Jump.Join(oprn_f_name, deep_f_oprn_name, jfix);
            Jump.Join(uflag_field, return_field, jfix);
            Jump.Join(exp_f_val, in_f_val, jfix);
            Jump.Join(in_f_val, uflag_field, jscolon);
            Jump.Join(onef_transient, err_dup_tra, jtrue);
            Jump.Join(onef_transient, sflag_transient, jfalse);
            Jump.Join(sflag_transient, attr_transient, jfix);
            Jump.Join(attr_transient, first_field, jfix);
            Jump.Join(deep_f_oprn_name, exp_f_val, jequal);
            Jump.Join(deep_f_oprn_name, uflag_field, jscolon);

            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // >>>>>>>>>>>>>>>>>>> INTERFACE <<<<<<<<<<<<<<<<<<<
            StateFlagOne onef_i_acs = new StateFlagOne("I_PUB, I_PRT, I_PRV");
            StateFlagOne onef_i_abs = new StateFlagOne("I_ABS");
            StateFlagOne onef_i_stc = new StateFlagOne("I_STC");
            StateError err_i_acs = new StateError("1010");
            StateMove mv_i_acs = new StateMove();
            StateError err_i_mod_dup = new StateError("1010");
            StateSetFlag sflag_i_abs = new StateSetFlag("I_ABS");
            StateSetFlag sflag_i_stc = new StateSetFlag("I_STC");
            StateSetFlag sflag_i_pub = new StateSetFlag("I_PUB");
            StateSetFlag sflag_i_prt = new StateSetFlag("I_PRT");
            StateSetFlag sflag_i_prv = new StateSetFlag("I_PRV");
            StateAttribute attr_i_pub = new StateAttribute("1030");
            StateAttribute attr_i_prv = new StateAttribute("1010");
            StateAttribute attr_i_prt = new StateAttribute("1020");
            StateAttribute attr_i_abs = new StateAttribute("1050");
            StateAttribute attr_i_stc = new StateAttribute("1035");
            StateStatement stmt_interface = new StateStatement("1140");
            StateDeep deep_interface = new StateDeep();
            StateInsertAttributes in_attr_interface = new StateInsertAttributes();
            StateSetFlag sflag_interface = new StateSetFlag("INTERFACE");
            StateAttribute attr_i_name = new StateAttribute("1000");
            StateDeep deep_i_name = new StateDeep();
            StateOperand oprn_i_name = new StateOperand("1000");
            StateInsert in_i_name = new StateInsert();
            StateRise rise_i_name = new StateRise();
            StateStatement stmt_i_extends = new StateStatement("1240");
            StateDeep deep_i_extends = new StateDeep();
            StateSubprogram call_iden_type_i = new StateSubprogram();
            StateInsert in_i_extends = new StateInsert();
            StateStatement stmt_i_body = new StateStatement("1000");
            StateDeep deep_i_body = new StateDeep();
            StateUnsetFlag uflag_interface = new StateUnsetFlag("I_PUB, I_PRT, I_PRV, INTERFACE, I_ABS, I_STC");
            StateReturn return_interface = new StateReturn();
            StateMove mv_i = new StateMove();
            StateBackoff back_method = new StateBackoff();
            StateMove mv_i_type = new StateMove();
            StateMove mv_i_dot = new StateMove();
            StateSubprogram call_i_method = new StateSubprogram();
            StateInsert in_i_method = new StateInsert();
            StateSubprogram call_i_class = new StateSubprogram();
            StateBackoff back_i_class = new StateBackoff();
            StateInsert in_i_class = new StateInsert();
            StateMove mv_i_iden = new StateMove();
            StatePoint point_i = new StatePoint();
            StateSubprogram call_i_field = new StateSubprogram();
            StateBackoff back_i_field = new StateBackoff();
            StateInsert in_i_field = new StateInsert();
            StateSubprogram call_i_interface = new StateSubprogram();
            StateBackoff back_i_interface = new StateBackoff();
            StateInsert in_i_interface = new StateInsert();
            StateMove mv_i_name = new StateMove();

            Jump.Join(first_interface, onef_i_acs, jpublic_fix);
            Jump.Join(first_interface, onef_i_acs, jprivate_fix);
            Jump.Join(first_interface, onef_i_acs, jprotected_fix);
            Jump.Join(onef_i_acs, err_i_acs, jtrue);
            Jump.Join(first_interface, onef_i_abs, jabstract);
            Jump.Join(first_interface, onef_i_stc, jstatic);
            Jump.Join(onef_i_acs, mv_i_acs, jfalse);
            Jump.Join(onef_i_abs, err_i_mod_dup, jtrue);
            Jump.Join(mv_i_acs, sflag_i_pub, jpublic);
            Jump.Join(sflag_i_pub, attr_i_pub, jfix);
            Jump.Join(mv_i_acs, sflag_i_prt, jprotected);
            Jump.Join(sflag_i_prt, attr_i_prt, jfix);
            Jump.Join(mv_i_acs, sflag_i_prv, jprivate);
            Jump.Join(sflag_i_prv, attr_i_prv, jfix);
            Jump.Join(onef_i_abs, sflag_i_abs, jfalse);
            Jump.Join(sflag_i_abs, attr_i_abs, jfix);
            Jump.Join(onef_i_stc, err_i_mod_dup, jtrue);
            Jump.Join(onef_i_stc, sflag_i_stc, jfalse);
            Jump.Join(sflag_i_stc, attr_i_stc, jfix);
            Jump.Join(attr_i_pub, first_interface, jfix);
            Jump.Join(attr_i_prt, first_interface, jfix);
            Jump.Join(attr_i_prv, first_interface, jfix);
            Jump.Join(attr_i_abs, first_interface, jfix);
            Jump.Join(attr_i_stc, first_interface, jfix);
            Jump.Join(first_interface, sflag_interface, jfix);
            Jump.Join(sflag_interface, stmt_interface, jinterface);
            Jump.Join(stmt_interface, deep_interface, jfix);
            Jump.Join(deep_interface, in_attr_interface, jfix);
            Jump.Join(in_attr_interface, attr_i_name, jiden);
            Jump.Join(attr_i_name, deep_i_name, jfix);
            Jump.Join(deep_i_name, oprn_i_name, jfix);
            Jump.Join(oprn_i_name, in_i_name, jfix);
            Jump.Join(in_i_name, rise_i_name, jfix);
            Jump.Join(stmt_i_extends, deep_i_extends, jfix);
            Jump.Join(deep_i_extends, call_iden_type_i, jfix);
            Jump.Join(call_iden_type_i, first_iden_type, jfix);
            Jump.Join(call_iden_type_i, in_i_extends, jret);
            Jump.Join(rise_i_name, stmt_i_body, jopen_brace);
            Jump.Join(stmt_i_body, deep_i_body, jfix);
            Jump.Join(deep_i_body, uflag_interface, jclose_brace);
            Jump.Join(uflag_interface, return_interface, jfix);
            Jump.Join(rise_i_name, stmt_i_extends, jextends);
            Jump.Join(in_i_extends, stmt_i_body, jopen_brace);
            Jump.Join(in_i_extends, call_iden_type_i, jcomma);
            Jump.Join(mv_i, mv_i, jprivate);
            Jump.Join(mv_i, mv_i, jprotected);
            Jump.Join(mv_i, mv_i, jpublic);
            Jump.Join(mv_i, back_method, jvoid);
            Jump.Join(mv_i, mv_i, jabstract);
            Jump.Join(mv_i, mv_i, jstatic);
            Jump.Join(mv_i, mv_i_type, jint);
            Jump.Join(mv_i, mv_i_dot, jiden);
            Jump.Join(back_method, call_i_method, jfix);
            Jump.Join(call_i_method, first_method, jfix);
            Jump.Join(call_i_method, in_i_method, jret);
            Jump.Join(call_i_class, first_class, jfix);
            Jump.Join(mv_i, back_i_class, jclass);
            Jump.Join(back_i_class, call_i_class, jfix);
            Jump.Join(call_i_class, in_i_class, jret);
            Jump.Join(deep_i_body, point_i, jfix);
            Jump.Join(point_i, uflag_interface, jclose_brace);
            Jump.Join(point_i, mv_i, jfix);
            Jump.Join(in_i_class, point_i, jfix);
            Jump.Join(in_i_method, point_i, jfix);
            Jump.Join(call_i_field, first_field, jfix);
            Jump.Join(back_i_field, call_i_field, jfix);
            Jump.Join(call_i_field, in_i_field, jret);
            Jump.Join(in_i_field, point_i, jfix);
            Jump.Join(mv_i, back_i_interface, jinterface);
            Jump.Join(back_i_interface, call_i_interface, jfix);
            Jump.Join(call_i_interface, first_interface, jfix);
            Jump.Join(call_i_interface, in_i_interface, jret);
            Jump.Join(in_i_interface, point_i, jfix);
            Jump.Join(mv_i_dot, mv_i_iden, jdot);
            Jump.Join(mv_i_iden, mv_i_dot, jiden);
            Jump.Join(mv_i_dot, mv_i_type, jiden);
            Jump.Join(mv_i_dot, mv_i_dot, jarray);
            Jump.Join(mv_i_type, mv_i_type, jarray);
            Jump.Join(mv_i, mv_i_type, jlong);
            Jump.Join(mv_i, mv_i_type, jbyte);
            Jump.Join(mv_i, mv_i_type, jshort);
            Jump.Join(mv_i, mv_i_type, jchar);
            Jump.Join(mv_i, mv_i_type, jfloat);
            Jump.Join(mv_i, mv_i_type, jdouble);
            Jump.Join(mv_i, mv_i_type, jboolean);
            Jump.Join(mv_i_type, mv_i_name, jiden);
            Jump.Join(mv_i_name, back_method, jopen_round_br);
            Jump.Join(mv_i_name, back_i_field, jfix);

            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            return first;
        }
    }
}
