﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Biendich.Language;
using Biendich.Generation;

namespace Biendich.Generation.Syntax
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SyntaxGenerator sg = new SyntaxGenerator();
            string xml = sg.GenerateXml(new Lang("xml/lang.xml"));
            File.WriteAllText("xml/syntax.xml", xml);
        }
    }
}
