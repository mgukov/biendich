using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Biendich.Utility;

namespace Biendich.Language
{
	public class Lang
	{
		private XmlNode root;
        private Dictionary<int, XmlNode> tokens;

        private Dictionary<string, int> tokens_id;
        private Dictionary<int, string> tokens_class;
        private Dictionary<int, string> tokens_value;

        private Dictionary<string, Dictionary<int, XmlNode>> instructions;

        public Lang(string path)
        {
            tokens = new Dictionary<int, XmlNode>();
            tokens_id = new Dictionary<string, int>();
            tokens_class = new Dictionary<int, string>();
            tokens_value = new Dictionary<int, string>();

            instructions = new Dictionary<string, Dictionary<int, XmlNode>>();

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(path);
            root = xmldoc["language"];

			foreach (XmlNode node in root["lexical"]["tokens"].ChildNodes)
			{
				foreach (XmlNode m in node.ChildNodes)
				{
                    tokens.Add(Convert.ToInt32(m.Attributes["id"].Value), m);
                    tokens_id.Add(m.InnerText, Convert.ToInt32(m.Attributes["id"].Value));
                    tokens_class.Add(Convert.ToInt32(m.Attributes["id"].Value), node.Name);
                    tokens_value.Add(Convert.ToInt32(m.Attributes["id"].Value), m.InnerText);
				}
			}
            foreach (XmlNode instruction in root["sintax"].ChildNodes)
            {
                if (instruction is XmlComment) continue;
                Dictionary<int, XmlNode> dict = new Dictionary<int, XmlNode>();
                foreach (XmlNode m in instruction.ChildNodes)
                {
                    dict.Add(Convert.ToInt32(m.Attributes["id"].Value), m);
                }
                instructions.Add(instruction.Name, dict);
            }
        }
		
        public string Alphabet(string charclass)
		{
            return root["lexical"]["alphabet"][charclass].InnerText;
		}
        public string[] Tokens(string tokclass)
		{
			List<string> toks = new List<string>();
            foreach (XmlNode m in root["lexical"]["tokens"][tokclass].ChildNodes)
			{
				toks.Add(m.InnerText);
			}
			return toks.ToArray();
		}
		public int TokenId(string tokname)
		{
            return tokens_id[tokname];
		}
        public int[] TokenId(string[] toknames)
        {
            int[] id = new int[toknames.Length];
            for (int i = 0; i < toknames.Length; ++i)
            {
                id[i] = tokens_id[toknames[i]];
            }
            return id;
        }
        public string TokenValue(int tokid)
        {
            return tokens_value[tokid];
        }
        public string TokenAttribute(int tokid, string attr)
        {
            return tokens[tokid].Attributes[attr].Value;
        }
		public string TokenClass(int tokid)
		{
            return tokens_class[tokid];
		}

        public string InstructionAttribute(string instruction_name, int id, string attr)
        {
            return instructions[instruction_name][id].Attributes[attr].Value;
        }

        public XmlNode Instruction(string instruction_name, int id)
        {
            return instructions[instruction_name][id];
        }

        //public int InstructionId(string op)
        //{
        //    return operators_id.Get(op);
        //}
        //public string StatementAttribute(int id, string attr)
        //{
        //    return statements.Get(id).Attributes[attr].Value;
        //}
        //public int StatementId(string statements)
        //{
        //    return statements_id[statements];
        //}
        //public XmlNode TokenNode(int tokid)
        //{
        //    return tokens.Get(tokid);
        //}

	}
}

