﻿using System;
using System.Collections.Generic;
using System.Text;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Syntax;
using Biendich.Language;

namespace testparser
{
    class Program
    {
        static void Main(string[] args)
        {
            Lang lang = new Lang("C:/test/lang.xml");
            Scanner scann = new Scanner("C:/test/lexical.xml", lang,
                new int[] { 801, 802, 803, 804, 100 });
            Parser parser = new Parser(lang, "C:/test/syntax.xml");
            IChain ch1 = new ByteChain("C:/test/src.java"),
                ch2 = new TokChain(scann, ch1);
            Statement m = (Statement)parser.Run(ch2);
            m.ToXml().Save("C:/test/tree.xml");
        }
    }
}
