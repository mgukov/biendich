using System;
using System.Xml;
using System.IO;
using Biendich.Lexical;
using Biendich.Syntax;
using Biendich.Automation;
using Biendich.Lang;

namespace Biendich.Test
{
	class MainClass
	{
		public static void Main (string[] args)
		{
            Language lang = new Language("C:/test/lang.xml");
            Scanner scann = new Scanner("C:/test/lexical.xml", lang,
                new int[] { 801, 802, 803, 804, 100 });
            MathParser parser = new MathParser(lang, "C:/test/math.xml");
            IChain ch1 = new ByteChain("C:/test/src.java"),
                ch2 = new TokChain(scann, ch1);
            Expression m = (Expression)parser.Run(ch2);
            m.ToXml().Save("C:/test/tree.xml");

		}
	}
}
 