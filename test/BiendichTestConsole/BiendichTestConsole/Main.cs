using System;
using Biendich.Lexical;
using Biendich.Language;
using Biendich.Utility;
using Biendich.Automation;
using Biendich.Generation;
using Biendich.Syntax;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace BiendichTestConsole {
	
	class MainClass {
		
		public static void Main (string[] args) { 
			Console.WriteLine ("Welcome to Biendich");
			Console.WriteLine ("Enter command to continue: ");
			String str = Console.ReadLine ();
			
			TestClass testClass = new TestClass ();
			
		}
	}
	
	class TestClass {
		
		private Scanner scann;
        private Parser parser;
        private Lang lang;
        private Dictionary<object, Statement> nodes;
		
		public TestClass () {
			SyntaxUpdate ();
		}
		
        private void SyntaxUpdate () {
			
			lang = new Lang ("xml/lang.xml");
			scann = new Scanner ("xml/lexical.xml", lang,
                new int[] { 801, 802, 803, 804, 100 });
			parser = new Parser (lang, "xml/syntax.xml");
			nodes = new Dictionary<object, Statement> ();
		}

        private void TreeBuild(Statement tree, Lang lang)
        {
            nodes.Clear();

            Queue<Pair<Statement, object>> queue = new Queue<Pair<Statement, object>>();

            //queue.Enqueue(Pair.Create(null, null));
            nodes.Add(null, tree);

            while (queue.Count > 0)
            {
                Pair<Statement, object> root = queue.Dequeue();
                foreach (Statement s in root.First.Statements)
                {
                    string name = "";
                    if (s is Biendich.Syntax.Attribute) name = "attr : ";
                    if (s is Biendich.Syntax.Error) name = "err : ";

                    name += lang.InstructionAttribute(s.Name + "s", s.Id, "desc");

                    if (s is Operand) name += " : " + s.Value;

                    //queue.Enqueue(Pair.Create(s, null));
                    nodes.Add(null, s);
                }
            }
        }

        private Pair<int, string>[] ShowErrors (Error[] errs) {
			StringBuilder sb = new StringBuilder ();
			List<Pair<int, string>> msg = new List<Pair<int, string>> ();

			foreach (Error err in errs) {
				int row = 0, col = 0;

				sb.Remove (0, sb.Length);

				sb.Append ("Ошибка: ");
				sb.Append (lang.InstructionAttribute ("errors", err.Id, "desc"));
				sb.Append (" '" + err.Value.ToString ());
				sb.Append ("' строка " + row.ToString () + ", колонка " + col.ToString ());
				msg.Add (Pair.Create (err.Position, sb.ToString ()));
			}
			return msg.ToArray ();
		}
		
		public void test () {
			run ();
		}
		
        private void run () {

			File.WriteAllText ("src.txt", " ");

			IChain ch1 = new ByteChain ("src.txt"),
                ch2 = new TokChain (scann, ch1);
            
			Statement m = null;
            try {
				m = (Statement)parser.Run (ch2);
				foreach (Pair<int, string> msg in ShowErrors(parser.Errors())) {
					
				}
				this.TreeBuild (m, lang);
			} catch (Exception exc) {
				Console.WriteLine("Исключение: " + exc.Message);
			}
			ch1.Close ();
		}
	}
}
