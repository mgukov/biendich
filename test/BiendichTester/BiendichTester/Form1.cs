﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Biendich.Language;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Syntax;
using Biendich.Utility;

namespace Biendich.Test
{
    public partial class Form1 : Form
    {

        private Scanner scann;
        private Parser parser;
        private Lang lang;
        private Dictionary<TreeNode, Statement> nodes;


        private void SyntaxUpdate()
        {
            lang = new Lang("xml/lang.xml");
            scann = new Scanner("xml/lexical.xml", lang,
                new int[] { 801, 802, 803, 804, 100 });
            parser = new Parser(lang, "xml/syntax.xml");
            nodes = new Dictionary<TreeNode, Statement>();
        }

        private void TreeBuild(Statement tree, Lang lang)
        {
            treeSyntax.Nodes.Clear();
            nodes.Clear();

            Queue<Pair<Statement, TreeNode>> queue = new Queue<Pair<Statement, TreeNode>>();
            TreeNode tree_root = new TreeNode(
                lang.InstructionAttribute(tree.Name + "s", tree.Id, "desc"));
            treeSyntax.Nodes.Add(tree_root);

            queue.Enqueue(Pair.Create(tree, tree_root));
            nodes.Add(tree_root, tree);

            while (queue.Count > 0)
            {
                Pair<Statement, TreeNode> root = queue.Dequeue();
                foreach (Statement s in root.First.Statements)
                {
                    string name = "";
                    if (s is Biendich.Syntax.Attribute) name = "attr : ";
                    if (s is Biendich.Syntax.Error) name = "err : ";

                    name += lang.InstructionAttribute(s.Name + "s", s.Id, "desc");

                    if (s is Operand) name += " : " + s.Value;

                    TreeNode tree_node = new TreeNode(name);
                    root.Second.Nodes.Add(tree_node);
                    queue.Enqueue(Pair.Create(s, tree_node));
                    nodes.Add(tree_node, s);
                }
            }
        }

        private Pair<int, string>[] ShowErrors(Error[] errs)
        {
            StringBuilder sb = new StringBuilder();
            List<Pair<int, string>> msg = new List<Pair<int, string>>();

            foreach (Error err in errs)
            {
                int row = txtSourseCode.GetLineFromCharIndex(err.Position) + 1,
                    col = err.Position - txtSourseCode.GetFirstCharIndexFromLine(row - 1) + 1;

                sb.Clear();

                sb.Append("Ошибка: ");
                sb.Append(lang.InstructionAttribute("errors", err.Id, "desc"));
                sb.Append(" '" + err.Value.ToString());
                sb.Append("' строка " + row.ToString() + ", колонка " + col.ToString());
                msg.Add(Pair.Create(err.Position, sb.ToString()));
            }
            return msg.ToArray();
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void menuRun_Click(object sender, EventArgs e)
        {
            lstMessage.Items.Clear();

            File.WriteAllText("src.txt", txtSourseCode.Text + " ");

            IChain ch1 = new ByteChain("src.txt"),
                ch2 = new TokChain(scann, ch1);
            
            Statement m = null;
            

            lstMessage.DisplayMember = "Second";
            lstMessage.ValueMember = "First";


            try
            {
                m = (Statement)parser.Run(ch2);
                foreach (Pair<int, string> msg in ShowErrors(parser.Errors()))
                {
                    lstMessage.Items.Add(msg);
                }
                this.TreeBuild(m, lang);
            }
            catch(Exception exc)
            {
                MessageBox.Show("Исключение: " + exc.Message);
            }
            ch1.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SyntaxUpdate();
        }

        private void menuUpdate_Click(object sender, EventArgs e)
        {
            SyntaxUpdate();
        }

        private void lstMessage_DoubleClick(object sender, EventArgs e)
        {
            if (lstMessage.SelectedIndex == -1) return;

            int chr = ((Pair<int, string>)(lstMessage.SelectedItem)).First,
                first_line = txtSourseCode.GetLineFromCharIndex(chr),
                sel_first = txtSourseCode.GetFirstCharIndexFromLine(first_line),
                sel_last = sel_first;

            while (sel_last < txtSourseCode.Text.Length && txtSourseCode.Text[sel_last] != '\r')
                ++sel_last;
            txtSourseCode.SelectionStart = sel_first;
            txtSourseCode.SelectionLength = sel_last - sel_first;
            txtSourseCode.ScrollToCaret();
            txtSourseCode.Focus();
        }

        private void CaretPos()
        {
            int pos = txtSourseCode.SelectionStart,
                row = txtSourseCode.GetLineFromCharIndex(pos) + 1,
                col = pos - txtSourseCode.GetFirstCharIndexFromLine(row - 1) + 1;

            statusStrip1.Items[0].Text = "Строка " + row + "    Колонка " + col;
        }

        private void txtSourseCode_KeyDown(object sender, KeyEventArgs e)
        {
            CaretPos();
        }

        private void txtSourseCode_KeyUp(object sender, KeyEventArgs e)
        {
            CaretPos();
        }

        private void txtSourseCode_MouseClick(object sender, MouseEventArgs e)
        {
            CaretPos();
        }

        private void menuExpandTree_Click(object sender, EventArgs e)
        {
            treeSyntax.ExpandAll();
        }

        private void treeSyntax_DoubleClick(object sender, EventArgs e)
        {
            if (treeSyntax.SelectedNode != null)
            {
                Statement stmt = nodes[treeSyntax.SelectedNode];
                txtSourseCode.SelectionStart = stmt.Instruction.Position;
                txtSourseCode.ScrollToCaret();
                txtSourseCode.Focus();
            }
        }

        private void txtSourseCode_Enter(object sender, EventArgs e)
        {
            CaretPos();
        }

        private void menuOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtSourseCode.Text = File.ReadAllText(openFileDialog1.FileName);
            }
        }

    }
}
