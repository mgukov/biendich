﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Biendich.Automation;
using Biendich.Lexical;
using Biendich.Utility;
using Biendich.Language;

namespace Biendich
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Lang lang = new Lang("C:/test/lang.xml");
            Scanner scann = new Scanner("C:/test/lexical.xml", lang,
                new int[] { 801, 802, 803, 804 });
            IChain ch1 = new ByteChain("C:/test/src.java");
            ScannToFile(scann, ch1, lang);
        }
        public static void ScannToFile(Scanner scanner, IChain chain, Lang lang)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("<title></title>");
            sb.AppendLine("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body bgcolor='yellowgreen'>");
            sb.AppendLine("<table bgcolor='greenyellow' border='1' cellpadding='4'" + 
                "cellspacing='0' align='center' width='55%'>");

            sb.AppendLine("<tr>");
            sb.AppendLine("<th>value</th><th>position</th><th>id</th><th>type</th>");
            Token tok = (Token)(scanner.Run(chain));
            while (!chain.EOF() && tok != null)
            {
                sb.AppendLine("<tr>");
                if (tok.Id != 10 ||
                    !" \t\r\n".Contains(Utils.Decode((string)tok.Value).ToString()))
                {
                    sb.Append("<td>");
                    sb.Append((string)tok.Value);
                    sb.Append("</td>");

                    sb.Append("<td>");
                    sb.Append(tok.Pos);
                    sb.AppendLine("</td>");

                    sb.Append("<td>");
                    sb.Append(lang.TokenAttribute(tok.Id, "id"));
                    sb.AppendLine("</td>");

                    sb.Append("<td>");
                    if (lang.TokenClass(tok.Id) == "error")
                        sb.Append(lang.TokenValue(tok.Id));
                    else
                        sb.Append(lang.TokenClass(tok.Id));
                    sb.AppendLine("</td>");
                }
                sb.AppendLine("</tr>");
                tok = (Token)(scanner.Run(chain));
            }
            sb.AppendLine("</table>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");
            File.WriteAllText("C:/test/out.html", sb.ToString(), Encoding.Unicode);
        }
    }
}
